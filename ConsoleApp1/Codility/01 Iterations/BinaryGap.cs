﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class BinaryGap
    {
        public static int solution(int N)
        {
            //var binary = Convert.ToString(20, 2);
            //int start = 0,gap=0,max=0,tempMax=0;
            //for (int i=0;i<binary.Length;i++)
            //{
            //    if (Convert.ToInt32(binary[i].ToString()) == 1 && start==0 ) start = 0;
            //    else if (Convert.ToInt32(binary[i].ToString()) == 1 && start > 0)
            //    {
            //        if (max < tempMax) max = tempMax;
            //        start = 0; gap = start;
            //    }
            //    else {
            //        start++;
            //        if (tempMax < start) tempMax = start;
            //        gap = start;
            //    }
            //}
            //Console.WriteLine(max);
            //Console.ReadLine();

            var binaryString = Convert.ToString(N, 2);
            int longestBinaryGap = 0;
            ArrayList onesList = new ArrayList();

            for (int i = 0; i < binaryString.Length; i++)
            {
                if (binaryString[i].ToString() == "0") continue;
                onesList.Add(i);
            }

            for (int i = 0; i < onesList.Count - 1; i++)
            {
                //subtract 1 so that don't count 1's next to each other as having any gap
                int indicesDiff = Convert.ToInt32(onesList[i + 1]) - Convert.ToInt32(onesList[i]) - 1;

                longestBinaryGap = Math.Max(longestBinaryGap, indicesDiff);
            }
            return longestBinaryGap;
            //Console.ReadLine();
        }
    }
}
