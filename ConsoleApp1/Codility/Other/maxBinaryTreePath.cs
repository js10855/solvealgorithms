﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ConsoleApp1.Codility.Other
{
    class maxBinaryTreePath
    {
        public int solution(Tree T)
        {
            int result = 0;           
            result = getHeight(T);
            return result;
        }

        public int getHeight(Tree root)
        {
            ArrayList al = new ArrayList();
            if (root == null)
            {
                return -1;
            }
            if (!al.Contains(root.x)) al.Add(root.x);
            getHeight(root.l);
            getHeight(root.r);

            return al.Count + 2;
        }

        public Tree insert(Tree root, int data)
        {
            if (root == null)
            {
                Tree cur = new Tree();
                cur.x = data;
                return cur;
            }
            else
            {
                Tree cur;
                if (data <= root.x)
                {
                    cur = insert(root.l, data);
                    root.l = cur;
                }
                else
                {
                    cur = insert(root.r, data);
                    root.r = cur;
                }
                return root;
            }
        }


    }

    class Tree
    {
        public int x;
        public Tree l;
        public Tree r;
         
    }
}
