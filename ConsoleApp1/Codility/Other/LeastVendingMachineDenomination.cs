﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility.Other
{
    class LeastVendingMachineDenomination
    {
       public static decimal[] getChange(decimal M, decimal P)
        {
            //1c, 5c, 10c, 25c, 50c, and $1. 
            // { 1, 0, 0, 0, 0, 1 };
            //A vending machine has the following denominations: 1c, 5c, 10c, 25c, 50c, and $1.
            //Your task is to write a program that will be used in a vending machine to return change.
            // Assume that the vending machine will always want to return the least number of coins or notes.
            // Devise a function getChange(M, P) where M is how much money was inserted
            //into the machine and P the price of the item selected,
            //that returns an array of integers representing the number of each denomination to return.
            //Example:
            //getChange(5, 0.99) // should return [1,0,0,0,0,4]

            decimal[] result = new decimal[6];
            decimal val = M - P;
            decimal dollar = Math.Truncate(val);
            result[5] = dollar;
            val = val - dollar;
            val = (int)(((decimal)val % 1) * 100);
            //OR
            //val = Convert.ToDecimal(val.ToString().Split('.')[1]);

            while (val > 0)
            {
                if (val >= 50) { result[4] += 1; val = val - 50; }
                else if (val >= 25) { result[3] += 1; val = val - 25; }
                else if (val >= 10) { result[2] += 1; val = val - 10; }
                else if (val >= 5) { result[1] += 1; val = val - 5; }
                else if (val >= 1) { result[0] += 1; val = val - 1; }
            }
            return result;


        }

        //Above problem cannot be solved using double
        static int[] getChange(double MoneyEntered, double cost)
        {
            int[] result = new int[6];
            double temp = MoneyEntered - cost;
            //int dollar = Convert.ToInt32(temp);
            int dollar = Convert.ToInt32(Math.Truncate(temp));
            result[5] = dollar;
            temp = temp - dollar;
            //temp = temp - Math.Truncate(temp);

            int val = (int)(((decimal)temp % 1) * 100);

            while (val > 0)
            {
                if (val >= 50) { val = val - 50; result[4] += 1; }
                else if (val >= 25) { val = val - 25; result[3] += 1; }
                else if (val >= 10) { val = val - 10; result[2] += 1; }
                else if (val >= 5) { val = val - 5; result[1] += 1; }
                else if (val >= 1) { val = val - 1; result[0] += 1; }
            }

            return result;
        }
    }
}
