﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility.Other
{
    class RankSoldier
    {
        public static int solution(int[] ranks)
        {
            int result = 0;
            Array.Sort(ranks);
            int previous = ranks[0];
            int sameNumber = 0;
            for (int i = 0; i < ranks.Length; i++)
            {
                if (ranks[i] == previous) sameNumber++;
                else if (ranks[i] != previous && ranks[i] == previous + 1)
                {
                    result += sameNumber;
                    sameNumber = 1;
                }
                else sameNumber = 1;
                previous = ranks[i];
            }

            return result;
        }
    }
}
