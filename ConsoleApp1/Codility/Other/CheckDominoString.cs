﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility.Other
{
    class CheckDominoString
    {
        public static void solution()
        {
            string domino = "1-1,3-5,7-2,1-3,2-4";
            string[] values = domino.Split(',');
            int counter = 1;
            bool foundDomino = false;

            for (int i = 0; i < values.Length; i++)
            {
                if (i == 0) continue;
                else
                {
                    string[] cVal = values[i].Split('-');
                    string[] pVal = values[i - 1].Split('-');
                    if (cVal[0] == pVal[1]) { counter++; foundDomino = true; }
                }
            }
            if (foundDomino)
                Console.WriteLine(counter);
            else
                Console.WriteLine(0);
            Console.ReadLine();
        }
    }
}
