﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility.Other
{
    class ToptalTestProblem
    {
        public static int solution(string[] T, string[] R)
        {
            int result;
            IDictionary<int, bool> dictFinalGroup = new Dictionary<int, bool>();

            //sorting both arrays
            //Array.Sort(T, R);

            IDictionary<string, string> dict = new Dictionary<string, string>();
            for (int i = 0; i < T.Length; i++)
            {
                dict.Add(T[i], R[i]);
            }

            int cnt = 0;
            int totalGroups = 0;
            for (int i = 1; i < dict.Count; i++)
            {
                Dictionary<string, string> temp = dict.Where(x => x.Key.Contains(i.ToString())).ToDictionary(x => x.Key, x => x.Value);
                cnt = temp.Where(x => !x.Value.Contains("OK")).Count();
                if (cnt <= 0 && temp.Count > 0)
                {
                    dictFinalGroup.Add(i, true);
                }
                if (temp.Count > 0) { totalGroups = i; }
            }

            int numOfGroupsPassed = dictFinalGroup.Where(x => x.Value == true).Count();
            result = (Int32)Math.Round(Convert.ToDouble(numOfGroupsPassed * 100 / totalGroups));
            return result;
        }

        //public int solution(string[] T, string[] R)
        //{
        //    // write your code in C# 6.0 with .NET 4.5 (Mono)
        //    int result = 1;
        //    int previousGroup=0;
        //    int currentGroup=0;
        //    int numOfGroups = 0;
        //    bool moveToNextGroup = false ;
        //    string resultString;
        //    bool[] resultGroupWise;
        //    IDictionary<int, bool> dictFinalGroup = new Dictionary<int, bool>();

        //    //sorting both arrays
        //    Array.Sort(T, R);

        //    IDictionary<string, string> dict = new Dictionary<string, string>();
        //    for (int i = 0; i < T.Length; i++)
        //    {
        //        dict.Add(T[i], R[i]);
        //    }

        //    int cnt = 0;
        //    int totalGroups = 0;
        //    for (int i = 1; i < dict.Count; i++)
        //    {
        //        IDictionary<string, string> temp = dict.Where(x => x.Key.Contains(i.ToString())).ToDictionary(x => x.Key, x => x.Value);
        //        cnt= temp.Where(x => !x.Value.Contains("OK")).Count();
        //        if (cnt <= 0 && temp.Count>0)
        //        {
        //            dictFinalGroup.Add(i, true);
        //        }
        //        if (temp.Count > 0) { totalGroups = i; }
        //    }

        //    int numOfGroupsPassed = dictFinalGroup.Where(x => x.Value == true).Count();
        //    result = (Int32)Math.Round(Convert.ToDouble(numOfGroupsPassed * 100 / totalGroups));

        //    //Find number of groups
        //    //for (int i = 0; i < T.Length; i++)
        //    //{
        //    //    resultString = Regex.Match(T[i], @"\d+").Value;
        //    //    currentGroup = Int32.Parse(resultString);
        //    //    if (currentGroup == previousGroup)
        //    //    {
        //    //        continue;
        //    //    }
        //    //    else
        //    //    {
        //    //        numOfGroups++;
        //    //    }
        //    //    previousGroup = currentGroup;
        //    //}
        //    //resultGroupWise = new bool[numOfGroups];

        //    ////check each group now
        //    //previousGroup = 0;
        //    //currentGroup = 0;
        //    //numOfGroups = 0;
        //    //for (int i=0; i<T.Length; i++)
        //    //{
        //    //    resultString = Regex.Match(T[i], @"\d+").Value;
        //    //    currentGroup = Int32.Parse(resultString);
        //    //    if (currentGroup == previousGroup && moveToNextGroup)
        //    //    {
        //    //        continue;
        //    //    }
        //    //    else {

        //    //        if (R[i] != "OK")
        //    //        {
        //    //            moveToNextGroup = true;
        //    //            if (resultGroupWise.Length < numOfGroups)
        //    //            { 
        //    //                if (resultGroupWise[numOfGroups] == true)
        //    //                {
        //    //                    resultGroupWise[numOfGroups] = false;
        //    //                }
        //    //            }
        //    //        }
        //    //        else
        //    //        {
        //    //            resultGroupWise[numOfGroups] = true;
        //    //            moveToNextGroup = false;
        //    //        }
        //    //        numOfGroups++;
        //    //    }

        //    //    previousGroup = currentGroup;
        //    //}

        //    //int numOfGroupsPassed = resultGroupWise.Where(x => x == true).Count(x=>x);
        //    //result = (Int32) Math.Round(Convert.ToDouble(numOfGroupsPassed * 100 / resultGroupWise.Length));
        //    return result;
        //}

        //public enum ResultCode
        //{
        //    OK,
        //    'Wrong answer',
        //    Time Limit exceeded,
        //    Runtime error
        //}
    }
}
