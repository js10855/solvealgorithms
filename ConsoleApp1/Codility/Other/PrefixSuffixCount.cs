﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility.Other
{
    class PrefixSuffixCount
    {
        public static int solution(string S)
        {
            //int result = 0;
            //string[] prefixArr = new string[S.Length-1];
            //string[] suffixArr = new string[S.Length-1];
            //StringBuilder prefixSb = new StringBuilder();
            //StringBuilder suffixSb = new StringBuilder();
            ////string suffixSb = "";            

            //for (int i=0;i<S.Length-1;i++)
            //{
            //    prefixSb.Append(S[i]);
            //    prefixArr[i] = prefixSb.ToString();                
            //}

            ////for (int i = S.Length-1; i >0; i--)
            ////{               
            ////    suffixSb = S[i] + suffixSb;
            ////    suffixArr[i-1] = suffixSb;
            ////}

            //for (int i = S.Length - 1; i > 0; i--)
            //{
            //    suffixSb.Insert(0, S[i]);               
            //    suffixArr[i - 1] = suffixSb.ToString();
            //}

            //for (int i = 0; i < prefixArr.Length; i++)
            //{
            //    string s = prefixArr[i];
            //    int count = suffixArr.Where(x => x == s).Count();
            //    if (count > 0 && s.Length>result) result=s.Length;
            //}

            //return result;

            int n = S.Length;

            if (n < 2)
                return 0;

            int len = 0;
            int i = n / 2;

            while (i < n)
            {
                if (S[i] == S[len])
                {
                    ++len;
                    ++i;
                }
                else
                {
                    if (len == 0)
                    {

                        // no prefix 
                        ++i;
                    }
                    else
                    {

                        // search for shorter prefixes 
                        --len;
                    }
                }
            }

            return len;


        }
    }
}
