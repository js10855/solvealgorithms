﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class Dominator
    {
        public static int solution(int[] A)
        {
            //int result = -1;
            //if (A.Length == 0) return result;
            //else
            //{            
            //    Array.Sort(A);
            //    int previous = A[0];
            //    int count = 1;

            //    for (int i = 1; i < A.Length; i++)
            //    {
            //        if (A[i] == previous)
            //        {
            //            count++;
            //            if (count > (A.Length / 2)) { result = i; return result; }
            //        }
            //        else { previous = A[i]; count = 1; }
            //    }
            //    return result;
            //}
            int len = A.Length;
            if (len == 0) return -1;

            int depth = 1;
            int value = A[0];

            for (int i = 1; i < len; i++)
            {
                int elem = A[i];
                if (depth == 0)
                {
                    depth += 1;
                    value = elem;
                }
                else if (elem == value)
                {
                    depth += 1;
                }
                else
                {
                    depth -= 1;
                }

            }

            if (depth == 0) return -1;
            else return countElementAndFindIndex(value, A);
        }
        private static int countElementAndFindIndex(int element, int[] arr)
        {
            int count = 0;
            int index = -1;
            for (int i = 0; i < arr.Length; i++)
            {
                if (element == arr[i])
                {
                    count++;
                    index = i;
                }
            }
            if (count * 2 > arr.Length)
                return index;
            else return -1;
        }

        //}
    }
}
