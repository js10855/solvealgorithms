﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class MaxSliceSum
    {
        public static int solution(int[] A)
        {
            //int num = 0;
            //int sum = int.MinValue;
            //for (int i = 0; i < A.Length; i++)
            //{
            //    num = A[i];
            //    for (int j = i+1; j < A.Length; j++)
            //        {
            //            if (num + A[j] > sum) sum = num + A[j];
            //        }
            //}
            //return sum;
            int absoluteMax = A[0];
            int localMax = A[0];
            int nextSum = 0;
            int currentElement = 0;

            for (int i = 1; i < A.Length; i++)
            {
                currentElement = A[i];
                nextSum = localMax + currentElement;
                localMax = Math.Max(A[i], nextSum);
                absoluteMax = Math.Max(absoluteMax, localMax);
            }
            return absoluteMax;

        }
    }
}
