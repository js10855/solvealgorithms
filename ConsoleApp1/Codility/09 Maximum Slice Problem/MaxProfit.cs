﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class MaxProfit
    {
        public static int solution(int[] A)
        {
            //int result = 0;            
            //for (int i = 0; i < A.Length; i++)
            //{                
            //    for (int j = i + 1; j < A.Length; j++)
            //    {
            //        int temp = A[j]- A[i];
            //        if (temp > 0 && temp>result) result = temp;
            //    }
            //}

            //return result;

            if (A.Length < 2) return 0; //for empty array or 1 element array, no profit can be realized

            //convert profit table to delta table so can use max slice technique
            int[] deltaA = new int[A.Length];
            deltaA[0] = 0;
            for (int i = 1; i < A.Length; i++)
            {
                deltaA[i] = A[i] - A[i - 1];
            }

            int absoluteMax = deltaA[0];
            int localMax = deltaA[0];
            int nextSum = 0;
            int currentElement = 0;

            for (int i = 1; i < deltaA.Length; i++)
            {
                currentElement = deltaA[i];
                nextSum = localMax + currentElement;
                localMax = Math.Max(deltaA[i], nextSum);
                absoluteMax = Math.Max(absoluteMax, localMax);
            }
            if (absoluteMax > 0) return absoluteMax;

            return 0;
        }
    }
}
