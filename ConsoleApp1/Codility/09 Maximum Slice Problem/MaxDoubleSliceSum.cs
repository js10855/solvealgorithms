﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility._09_Maximum_Slice_Problem
{
    class MaxDoubleSliceSum
    {
        public static int solution(int[] A)
        {
            int[] slice1LocalMax = new int[A.Length];
            int[] slice2LocalMax = new int[A.Length];

            //start from i=1 because slice can't start at index 0
            for (int i = 1; i < A.Length - 1; i++)
            {
                slice1LocalMax[i] = Math.Max(slice1LocalMax[i - 1] + A[i], 0);
            }
            //start from i=A.Length-2 because slice can't end at index A.Length-1 
            for (int i = A.Length-2; i > 0; i--){
                slice2LocalMax[i] = Math.Max(slice2LocalMax[i + 1] + A[i], 0);
            }

            int MaxDoubleSliceSum = 0;
    
            //compute sums of all slices to find absolute Max
            for(int i = 1; i<A.Length-1; i++) 
            {
              MaxDoubleSliceSum = Math.Max(MaxDoubleSliceSum, slice1LocalMax[i - 1] + slice2LocalMax[i + 1]);
            }
    
            return MaxDoubleSliceSum;

        }
    }
}
