﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class OddOccurrenceInArray
    {
        public static int solution(int[] A)
        {
            //First solution of above problem
            //int val = 0, cnt;
            //A = A.OrderBy(x => x).ToArray();
            //for (int i = 0; i < A.Length; i++)
            //{
            //    cnt = A.Where(x => x == A[i]).Count();
            //    if (cnt % 2 == 0)
            //    {
            //        if (cnt > i) i = cnt - 1;
            //        else if (cnt == i) i = cnt + 1;
            //        continue;
            //    }
            //    else val = A[i]; break;
            //}
            //return val;


            //Second solution for above problem using XOR            
            //int result = A[0];
            //for (int i = 1; i < A.Length; i++)
            //{
            //    result = A[i] ^ result;
            //}
            //return result;

            //Simplest solution for above problem            
            Array.Sort(A);
            int i;
            for (i = 0; i < A.Length - 1 && A[i] == A[i + 1]; i += 2) ;
            return A[i];

        }
    }
}
