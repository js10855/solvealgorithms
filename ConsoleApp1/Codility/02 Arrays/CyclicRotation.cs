﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class CyclicRotation
    {
        public static int[] solution(int[] A, int K)
        {
            int[] result = new int[A.Length];
            for (int i = 0; i < A.Length; i++)
            {
                result[(i + K) % A.Length] = A[i];
            }
            return result;

            // write your code in C# 6.0 with .NET 4.5 (Mono)           
            //int temp;
            //for (int i = 1; i <= K; i++)
            //{
            //    for (int j = 0; j < A.Length; j++)
            //    {
            //        temp = A[A.Length-1];
            //        A[A.Length - 1] = A[j];
            //        A[j] = temp;                    
            //    }
            //}
            //return A;


        }
    }
}
