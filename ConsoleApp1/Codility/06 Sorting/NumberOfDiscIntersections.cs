﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class NumberOfDiscIntersections
    {
        public static int solution(int[] A)
        {
            ArrayList aList = new ArrayList();

            for (int i = 0; i < A.Length; i++)
            {
                //need explicit cast to long on right side for case when A[i] is Integer.MAX_VALUE and adding would cause overflow
                long leftMost = i - (long)A[i];
                long rightMost = i + (long)A[i];

                aList.Add(new Circle(leftMost, rightMost));
            }

            aList.Sort(new CircleComparator());

            Circle[] aOrderedCircles = new Circle[A.Length];
            int index = 0;

            foreach (Circle circle in aList)
            {
                aOrderedCircles[index++] = circle;
            }

            int intersections = 0;

            for (int i = 0; i < aOrderedCircles.Length - 1; i++)
            {
                for (int j = i + 1; j < aOrderedCircles.Length; j++)
                { //check intersection by comparing rightmost x with leftmost x of next element if(aOrderedCircles[i].rightMostX >= aOrderedCircles[j].leftMostX) {
                    intersections++;
                    //as soon as don't find intersection, stop counting cause circles are ordered so all subsequent circles won't intersect
                    if (intersections > 10000000) return -1;
                    else break;
                }      
            }
        //}
    return intersections;
            
        }
    }

    class Circle
    {
        public long leftMostX;
        public long rightMostX;

        public Circle(long pLeftMostX, long pRightMostX)
        {
            leftMostX = pLeftMostX;
            rightMostX = pRightMostX;
        }
    }

    //reference: https://stackoverflow.com/questions/2839137/how-to-use-comparator-in-java-to-sort
    class CircleComparator: IComparer
    {

        // @Override
        public int Compare(Circle pCircle1, Circle pCircle2)
        {
           
            if (pCircle1.leftMostX < pCircle2.leftMostX) return -1; //e.g. circle1 (-4, 6) < circle2 (-2, 9)
            if (pCircle2.leftMostX < pCircle1.leftMostX) return 1;  //e.g. circle2 (-4, 6) < circle1 (-3, 1)
            if (pCircle1.rightMostX < pCircle2.rightMostX) return -1; //e.g. circle1 (-4, 3) < circle2 (-4, 6)
            if (pCircle2.rightMostX < pCircle1.rightMostX) return 1;  //e.g. circle2 (-4, 3) < circle1 (-4, 1)

            return 0;  //e.g. circle1 (-2, 2), circle2 (-2, 2)
        }

        public int Compare(object x, object y)
        {
            throw new NotImplementedException();
        }
    }
}
