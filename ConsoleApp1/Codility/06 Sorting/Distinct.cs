﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class Distinct
    {
        public static int solution(int[] A)
        {
            if (A.Length == 0) return 0;
            else
            {
                Array.Sort(A);
                int previous = A[0];
                int distinctCount = 1;
                for (int i = 0; i < A.Length; i++)
                {
                    if (A[i] != previous) distinctCount++;
                    previous = A[i];
                }
                return distinctCount;
            }
        }
    }
}
