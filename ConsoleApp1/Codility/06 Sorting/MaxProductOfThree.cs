﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class MaxProductOfThree
    {
        public static int solution(int[] A)
        {            
            Array.Sort(A);
            int size = A.Length;

            return Math.Max(
                    A[0] * A[1] * A[size - 1],
                    A[size - 1] * A[size - 2] * A[size - 3]);

            //int result = 0;
            //result = A[A.Length - 1] * A[A.Length - 2] * A[A.Length -3];
            //for (int i = A.Length - 2; i >= 0; i--)
            //{
            //    if (i-2>=0)
            //    result = Math.Max(result, A[i] * A[i - 1] * A[i - 2]);
            //}

            //return result;
        }

    }
}
