﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class Triangle
    {
        public static int solution(int[] A)
        {
            if (A.Length < 3) return 0;

            Array.Sort(A);
            int i = 2;

            while (i < A.Length)
            {
                if (A[i] - A[i - 1] - A[i - 2] < 0) return 1;
                i++;
            }

            return 0;
        }
    }
}
