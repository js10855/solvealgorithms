﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class PermMissingElem
    {
        public static int solution(int[] A, int N)
        {
            int n = A.Length + 1;
            int sumOfAllElements = (n * (1 + n)) / 2;
            int missingElement = sumOfAllElements - A.Sum();
            return missingElement;

            //My solution
            //if (A.Length == 0) return 1;
            //else if (A.Length == 1)
            //{
            //    if (A[0] != 1) return 1;
            //    return A[0] + 1;
            //}
            //else
            //{
            //    Array.Sort(A);
            //    int i;
            //    if (A[0] != 1) return 1;
            //    else { 
            //    for (i = 0; i < A.Length-1 && A[i + 1] - A[i] == 1; i += 1) ;
            //    return A[i] + 1;
            //    }
            //}
        }
    }
}
