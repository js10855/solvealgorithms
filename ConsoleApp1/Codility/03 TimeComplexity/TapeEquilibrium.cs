﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class TapeEquilibrium
    {
        public static int solution(int[] A)
        {
            int minDiff, sum = 0, temp = 0;
            int totalSum = TotalSum(A);
            minDiff = Math.Abs(A[0] - (totalSum - A[0]));
            for (int i = 0; i < A.Length - 1; i++)
            {
                sum += A[i];
                temp = Math.Abs(sum - (totalSum - sum));
                //temp = Math.Abs(2 * sum - totalSum);
                if (temp < minDiff) minDiff = temp;
            }
            return minDiff;


            //long sumAllElements = 0;
            //for (int i = 0; i < A.Length; i++)
            //{
            //    sumAllElements += A[i];
            //}

            //int minDifference = int.MaxValue;
            //int currentDifference = int.MaxValue;
            //long sumFirstPart = 0;
            //long sumSecondPart = 0;

            //for (int p = 0; p < A.Length - 1; p++)
            //{
            //    sumFirstPart += A[p];
            //    sumSecondPart = sumAllElements - sumFirstPart;
            //    currentDifference = (int)Math.Abs(sumFirstPart - sumSecondPart);
            //    minDifference = Math.Min(currentDifference, minDifference);
            //}
            //return minDifference;
        }

        static int TotalSum(int[] A)
        {
            //int res = 0;
            //for (int i = 0; i < A.Length; i++)
            //{
            //    res += A[i];
            //}
            //return res;

            //second solutiun
            return ((1 + A.Length) * A.Length) / 2;
        }
    }
}
