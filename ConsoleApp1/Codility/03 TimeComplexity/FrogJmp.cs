﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class FrogJmp
    {
        public static int solution(int X, int Y, int D)
        {
            //int xy = X, cnt =0;
            //while (xy <Y)
            //{
            //    xy +=  D;
            //    cnt++;
            //}
            //return cnt;
            int dist = Y - X;
            int result = dist / D;
            if (dist % D != 0)
            {
                result += 1;
            }
            return result;
        }
    }
}
