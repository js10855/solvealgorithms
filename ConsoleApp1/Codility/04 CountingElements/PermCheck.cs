﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class PermCheck
    {
        public static int solution(int[] A)
        {
            //First solution- not correct for all cases
            //int max = A.Max();
            //int sum = TotalSum(A);
            //int CorrectSum = GetSum(max);
            //if (sum == CorrectSum) return 1;
            //else return 0;

            //Second solution
            Array.Sort(A);
            int cnt = 1;
            for (int i = 0; i < A.Length; i++)
            {
                if (cnt != A[i]) return 0;
                else cnt++;
            }
            return 1;
        }

        static int GetMax(int[] A)
        {
            int max = 0;
            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] > max) max = A[i];
            }
            return max;
        }

        static int GetSum(int n)
        {
            int sum = 0;
            for (int i = 1; i <= n; i++)
            {
                sum += i;
            }
            return sum;
        }
    }
}
