﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class MaxCounters
    {
        public static int[] solution(int N, int[] A)
        {
            //int[] result = new int[N];
            //int max = 0;

            //foreach (int num in A)
            //{
            //    if (num == N + 1)
            //    {
            //        for (int i = 0; i < result.Length; i++) result[i] =max;
            //    }
            //    else {
            //        result[num-1]++;
            //        if (result[num - 1] > max) max = result[num - 1];
            //    }                
            //}
            //return result;

            //Second solution got 77% only
            //int max = 0;
            //int[] result = new int[N];
            //for (int i = 0; i < A.Length; i++)
            //{
            //    int element = A[i] - 1;
            //    if (element == N)
            //    {
            //        incAll(result, max);
            //    }
            //    else
            //    {
            //        result[element] += 1;
            //        max = Max(max, result[element]);
            //    }
            //}
            //return result;

            //Third solution-below gives wrong result
            //int[] counters = new int[N];

            //int maxCounter = 0; //for the next re-set will know what high score to set all values
            //int lastResetCounter = 0; //for setting values that were never explicitly set - at the end

            //for (int i = 0; i < A.Length; i++)
            //{
            //    if (A[i] <= N)
            //    {
            //        if (counters[A[i] - 1] < lastResetCounter)
            //        {
            //            counters[A[i] - 1] = lastResetCounter; //bring it up to last reset value } counters[A[i]-1]++; //store max counter in case need to set all counters to this value on next reset if(counters[A[i]-1] > maxCounter) {
            //            maxCounter = counters[A[i] - 1];
            //        }

            //    }
            //    else
            //    {
            //        //keep track of last reset counter
            //        lastResetCounter = maxCounter;
            //    }
            //}
            ////set all values to last reset value that was never explicitly changed after last reset
            //for (int i = 0; i < counters.Length; i++)
            //{
            //    if (counters[i] < lastResetCounter)
            //    {
            //        counters[i] = lastResetCounter;
            //    }
            //}

            //return counters;
            int[] result = new int[N];
            int maximum = 0;
            int resetLimit = 0;

            for (int K = 0; K < A.Length; K++)
            {
                if (A[K] < 1 || A[K] > N + 1)
                    throw new InvalidOperationException();

                if (A[K] >= 1 && A[K] <= N)
                {
                    if (result[A[K] - 1] < resetLimit)
                    {
                        result[A[K] - 1] = resetLimit + 1;
                    }
                    else
                    {
                        result[A[K] - 1]++;
                    }

                    if (result[A[K] - 1] > maximum)
                    {
                        maximum = result[A[K] - 1];
                    }
                }
                else
                {
                    // inefficiency here
                    //for (int i = 0; i < result.Length; i++)
                    //    result[i] = maximum;
                    resetLimit = maximum;
                }
            }

            for (int i = 0; i < result.Length; i++)
                result[i] = Math.Max(resetLimit, result[i]);

            return result;



        }
    }
}
