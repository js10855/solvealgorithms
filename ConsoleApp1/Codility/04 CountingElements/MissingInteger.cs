﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class MissingInteger
    {
        public static int solution(int[] A)
        {
            //int res = 1;
            //for (int i = 1; i < A.Length + 1; i++)
            //{
            //    if (A.Contains(i)) res++;
            //    else break;
            //}

            //return res;
            int flag = 1;
            A = A.OrderBy(x => x).ToArray();
            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] < 0) continue;
                else
                {
                    if (A[i] == flag) flag++;
                }
            }
            return flag;

        }
    }
}
