﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class FrogRiverOne
    {
        public static int solution(int X, int[] A)
        {
            bool[] tiles = new bool[X];
            int todo = X;

            for (int i = 0; i < A.Length; i++)
            {
                int internalIndex = A[i] - 1;
                if (internalIndex < X && !tiles[internalIndex])
                {
                    todo--;
                    tiles[internalIndex] = true;
                }

                if (todo == 0)
                    return i;
            }

            return -1;

            //Solution 2
            //int z = -1;

            //long combA = ((long)X) * (((long)X) + 1) / 2;
            //long sumA = 0;

            //int[] countA = new int[X];

            //for (int i = 0; i < A.Length; i++)
            //{
            //    countA[A[i] - 1] += 1;

            //    if (countA[A[i] - 1] > 1)
            //    {
            //        countA[A[i] - 1] = 1;
            //    }
            //    else
            //    {
            //        sumA += A[i];
            //    }


            //    if (sumA == combA)
            //    {
            //        z = i;
            //        break;
            //    }

            //}

            //return z;
        }
    }
}
