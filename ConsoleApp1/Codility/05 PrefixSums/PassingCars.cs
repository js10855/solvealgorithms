﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class PassingCars
    {
        public static int solution(int[] A)
        {
            long result = 0;
            int zeroCount = 0;
            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] == 0) zeroCount++;
                else result += zeroCount;                
            }
            if (result > 1000000000) result = -1;
            return (int)result;
        }
    }
}
