﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class MinAvgTwoSlice
    {
        public static int solution(int[] A)
        {
            //double min = 0; 
            //int result = 0;
            //double avg = 0;
            //for (int i = 0; i < A.Length - 1; i++)
            //{
            //    avg = FindTwoAveSlice(A[i], A[i + 1]);
            //    if (i == 0 || avg < min) { min = avg; result = i; }
            //}
            //return result;

            double min = sortMean(A, 0);
            int minIndex = 0;
            for (int i = 1; i < A.Length - 1; i++)
            {
                double currentMean = sortMean(A, i);
                if (currentMean < min)
                {
                    min = currentMean;
                    minIndex = i;
                }
            }
            return minIndex;
        }

        private static double FindTwoAveSlice(int num1, int num2)
        {
            return (num1 + num2) / 2;
        }

        private static double sortMean(int[] A, int position)
        {
            double twoSlice = A[position] + A[position + 1];
            double threeSlice = A.Length - 2 == position ? twoSlice * 3 / 2 : twoSlice + A[position + 2];
            return Math.Min(twoSlice / 2, threeSlice / 3);
        }

       

    }
}
