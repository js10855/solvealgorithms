﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class GenomicRangeQuery
    {
        public static int[] solution(string S, int[] P, int[] Q)
        {
            int[] result= new int[P.Length];
            
            for (int i = 0; i < P.Length; i++)
            {
                int[] ACGT = new int[4];
                for (int j = P[i]; j <= Q[i]; j++)
                {
                    switch (S[j])
                    {
                        case 'A': ACGT[0] = 1; break;
                        case 'C': ACGT[1] = 2; break;
                        case 'G': ACGT[2] = 3; break;
                        case 'T': ACGT[3] = 4; break;
                    }
                }
               
                //int[] ACGT = FindACGTValue(S, P[i], Q[i]);
                result[i] = ACGT.Where(x => x > 0).Min();
                //FindMin(ACGT);
                     
            }
            

            return result;
        }

        private static int[] FindACGTValue(string S, int P, int Q)
        {
            int[] ACGT = new int[4];
            for (int j = P; j <= Q; j++)
            {
                switch (S[j])
                {
                    case 'A': ACGT[0] = 1; break;
                    case 'C': ACGT[1] = 2; break;
                    case 'G': ACGT[2] = 3; break;
                    case 'T': ACGT[3] = 4; break;
                }
            }
            return ACGT;
        }

        private static int FindMin(int[] A)
        {
            int min = 0;
            foreach(int num in A)
            {
                if (min == 0) min = num;
                else { 
                if (num > 0 && num < min) min = num;
                }
            }
            return min;
        }
    }
}
