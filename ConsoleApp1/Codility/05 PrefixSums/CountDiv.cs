﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Codility
{
    class CountDiv
    {
        public static int solution(int A, int B, int K)
        {
            //int result = 0;
            //for (int i = A; i <= B; i++)
            //{
            //    result += i % K == 0 ? 1 : 0;
            //}
            //return result;

            int before = A % K == 0 ? A / K - 1 : A / K;
            return B / K - before;
        }

    }
}
