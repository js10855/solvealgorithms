﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class beautifulDays
    {
        // Complete the beautifulDays function below.
        public static int solution(int i, int j, int k)
        {
            int result = 0;
            for (int x = i; x <= j; x++)
            {
                int minus = Math.Abs(x - ReverseNum(x));
                if (minus % k == 0) result++;
            }
            return result;
           
        }

        private static int ReverseNum(int num)
        {            
            int reverse = 0;
            while (num > 0)
            {
                int remainder = num % 10;
                reverse = (reverse * 10) + remainder;
                num = num / 10;
            }
            return reverse;
        }

        private static string ReverseString(string str)
        {
            //StringBuilder sb = new StringBuilder();
            //for (int x = num.ToString().Length - 1; x >= 0; x--)
            //{
            //    sb.Append(num.ToString()[x]);
            //}
            //return sb.ToString();

            string reverse = "";
            int Length = 0;
            Length = str.Length - 1;
            while (Length >= 0)
            {
                reverse = reverse + str[Length];
                Length--;
            }
            return reverse;
        }
}
}
