﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class taumBday
    {
        public static long solution(long b, long w, long bc, long wc, long z)
        {
            long result = 0;
            long minResult = 0;
            long bz = 0;
            long wz = 0;
            result = b * bc + w * wc;            
            if (bc > wc)
                {
                    if (bc < bc * w + z)
                    {
                        bz = b * z;
                        minResult = (b + w) * wc + (bz);
                    }
                }
            else
                {
                    if (wc < wc * b + z)
                    {
                        wz = w * z;
                        minResult = (b + w) * bc + (wz);
                    }
                }
            result = Math.Min(result, minResult);           
            return Convert.ToInt32(result);
        }
    }
}
