﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class camelcase
    {
        // Complete the camelcase function below.
        public static int solution(string s)
        {
            int result = 1;
            foreach(char c in s)
            {
                if (c >= 65 && c <= 90) result++;
            }
            return result;
        }
    }
}
