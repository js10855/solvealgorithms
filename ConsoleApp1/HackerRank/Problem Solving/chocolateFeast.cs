﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class chocolateFeast
    {
        public static int solution(int n, int c, int m)
        {
            int result = 0, val = 0;
            result = n / c;
            val = result;
            while (val >= m)
            {
                int ex_chocs = val / m;
                val = ex_chocs + val % m;
                result += ex_chocs;
            }
            return result;


            //while (val>0)
            //{
            //    if (m <= val)
            //    {                     
            //        val = val / m;
            //        int temp = val-m;
            //        result = result + val;
            //        val = val + temp;
            //    }
            //    else break;
            //}            

        }
    }
}
