﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class pangrams
    {
        // Complete the pangrams function below.
        public static string solution(string s)
        {
            string result = "pangram";
            int[] letters = new int[26];
            s = s.ToLower();           

            foreach(char c in s)
            {
                if (c !=32)  letters[c-97]++;
            }

            foreach(int num in letters)
            {
                if (num < 1) { result =  "not pangram"; break; }
            }

            return result;
        }

        public static string RemoveWhitespace(string str)
        {
            //return string.Join("", str.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries));
            //return new string(str.Where(c => !isWhiteSpace(c)).ToArray());
            return str.ToCharArray()
                 .Where(c => !Char.IsWhiteSpace(c))
                 .Select(c => c.ToString())
                 .Aggregate((a, b) => a + b);
        }

        public static bool isWhiteSpace(char ch)
        {
            // this is surprisingly faster than the equivalent if statement
            switch (ch)
            {
                case '\u0009':
                case '\u000A':
                case '\u000B':
                case '\u000C':
                case '\u000D':
                case '\u0020':
                case '\u0085':
                case '\u00A0':
                case '\u1680':
                case '\u2000':
                case '\u2001':
                case '\u2002':
                case '\u2003':
                case '\u2004':
                case '\u2005':
                case '\u2006':
                case '\u2007':
                case '\u2008':
                case '\u2009':
                case '\u200A':
                case '\u2028':
                case '\u2029':
                case '\u202F':
                case '\u205F':
                case '\u3000':
                    return true;
                default:
                    return false;
            }
        }
    }
}
