﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class stringConstruction
    {
        // Complete the stringConstruction function below.
        public static int solution(string s)
        {
            int result = 0;
            int[] chars = new int[26];
            foreach(char c in s) { chars[c-97]++; }
            foreach(int num in chars)
            {
                if (num > 0) result++;
            }
            return result;
        }
    }
}
