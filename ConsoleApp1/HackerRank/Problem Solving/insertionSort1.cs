﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class insertionSort1
    {
        // Complete the insertionSort1 function below.
        public static void solution(int n, int[] arr)
        {
            int lowerIndex = arr[arr.Length - 1];
            bool result = false;
            for (int i = arr.Length - 1; i >= 0; i--)
            {
                if (i == 0)
                {
                    arr[i] = lowerIndex;
                }
                else if (arr[i - 1] > lowerIndex & i != 0) arr[i] = arr[i - 1];
                else
                {
                    arr[i] = lowerIndex; result = true;
                }
                Console.WriteLine(string.Join(" ", arr));
                if (result) break;
            }

        }
    }
}
