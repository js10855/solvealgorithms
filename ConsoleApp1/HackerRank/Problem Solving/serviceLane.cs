﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class serviceLane
    {
        // Complete the serviceLane function below.
        public static int[] solution(int n,int[] w, int[][] cases)
        {
            int[] result = new int[cases.Length];
            for (int i = 0; i < cases.Length; i++)
            {
                int start = cases[i][0];
                int end = cases[i][1];
                int min = 0;
                for (int j = start; j <= end; j++)
                {
                    if (min == 0) min = w[j];
                    else if (w[j] < min) min = w[j];
                }
                result[i] = min;
            }
            return result;
        }

    }
}
