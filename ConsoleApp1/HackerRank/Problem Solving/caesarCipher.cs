﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class caesarCipher
    {
        public static string solution(string s, int k)
        {
            //string result = "";

            //for(int i=0;i<s.Length; i++)
            //{
            //    int ind = 0;                

            //    if (Enum.IsDefined(typeof(letter),  s[i].ToString().ToLower()))
            //    {
            //        ind = (int)Enum.Parse(typeof(letter), s[i].ToString().ToLower()) + k;
            //        if (ind > 26) ind = ind - 26;                    
            //        var val = Enum.GetName(typeof(letter), ind);
            //        if (char.IsUpper(s[i])) val = val.ToUpper();
            //        result += val;                    
            //    }
            //    else  result += s[i].ToString();                
            //}
            //return result;

            string result = "";
            if (k == 0) return s;
            int newChar;
            k = k % 26;
            int len = s.Length;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] >= 'A' && s[i] <= 'Z')
                {
                    newChar = s[i] + k;
                    if (newChar > 'Z')
                    {
                        newChar = 64 + newChar - 'Z';
                    }
                    result += (char)newChar;
                }
                else if (s[i] >= 'a' && s[i] <= 'z')
                {
                    newChar = s[i] + k;
                    if (newChar > 'z')
                    {
                        newChar = 96 + newChar - 'z';
                    }                   
                    result += (char)newChar;
                }
                else result += s[i];
            }
            return result;

        }

        enum letter
        {
            a = 1,
            b = 2,
            c = 3,
            d = 4,
            e = 5,
            f = 6,
            g = 7,
            h = 8,
            i = 9,
            j = 10,
            k = 11,
            l = 12,
            m = 13,
            n = 14,
            o = 15,
            p = 16,
            q = 17,
            r = 18,
            s = 19,
            t = 20,
            u = 21,
            v = 22,
            w = 23,
            x = 24,
            y = 25,
            z = 26
        }
    }
}
