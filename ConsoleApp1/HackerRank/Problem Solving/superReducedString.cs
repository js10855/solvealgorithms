﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class superReducedString
    {

        // Complete the superReducedString function below.
        public static string solution(string s)
        {            
            string result = "Empty String";
            //First solution
            //bool opDone = false;
            //int counter = 0;
            // while (!opDone)
            //{
            //    if (counter + 1 >= s.Length) { opDone = true; break; }
            //    if (s[counter] == s[counter + 1]) { s = s.Remove(counter, 2); counter = 0; }
            //    else counter++;
            //}            
            //if (!string.IsNullOrEmpty(s)) result = s;
            //return result;

            //Another solution
            StringBuilder sb = new StringBuilder(s);
            for (int i = 1; i < sb.Length; i++)
            {
                if (sb[i] == sb[i - 1])
                {
                    sb.Remove(i - 1, i + 1);
                    // str = str.substring(0, i-1) + str.substring(i+1);
                    i = 0;
                }
            }
            if (sb.Length != 0) result =sb.ToString();
            return result;
        }
    }
}
