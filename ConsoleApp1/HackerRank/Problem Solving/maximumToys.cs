﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class maximumToys
    {
       public static int solution(int[] prices, int k)
        {
            int maxToys = 0;
            int priceTotal = 0;
            Array.Sort(prices);
            for (int i = 0; i < prices.Length; i++)
            {
                priceTotal += prices[i];
                if (priceTotal > k) { maxToys = i; break; }                
            }
            return maxToys;
        }
    }
}
