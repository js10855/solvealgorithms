﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class Anagram
    {
        // Complete the anagram function below.
        public static int solution(string s)
        {
            int result = 0;
            if (s.Length == 1) result = -1;
            else if (s.Distinct().Count() == 1) result = 0;
            else if (s.Length % 2 != 0) result = -1;
            else
            {
                int halfVal = s.Length / 2;
                string s1 = s.Substring(0, halfVal);
                string s2 = s.Substring(halfVal , halfVal);
                int[] chars = new int[26];
                foreach (char c11 in s1.ToCharArray()) { chars[c11 - 97]++; }
                foreach (char c22 in s2.ToCharArray()) { chars[c22 - 97]--; }                
                foreach (int i in chars) {
                    if (i > 0) { 
                    result += Math.Abs(i);
                    }
                }
            }
            return result;
        }
    }
}
