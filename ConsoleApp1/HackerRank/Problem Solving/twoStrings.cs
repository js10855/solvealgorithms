﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class twoStrings
    {
        // Complete the twoStrings function below.
        public static string solution(string s1, string s2)
        {
            //string result = "NO";
            //foreach(char c1 in s1)
            //{     
            //    if (s2.IndexOf(c1)>-1)
            //    { result = "YES"; break; }            
            //}
            //for (int i = 97; i < (97 + 25); i++)
            //{
            //    if (s1.IndexOf(s1[i]) != -1 && s2.IndexOf(s2[i]) != -1)
            //    {
            //        result = "YES";
            //        break;
            //    }
            //}
            //return result;
            var distS1 = s1.Distinct();
            var distS2 = s2.Distinct();

            if (distS1.Intersect(distS2).Any())
            {
                return "YES";
            }
            else
            {
                return "NO";
            }
        }
    }
}
