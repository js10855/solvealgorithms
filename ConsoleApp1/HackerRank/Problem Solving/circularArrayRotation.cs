﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class circularArrayRotation
    {
        public static int[] Solution(int[] a, int k, int[] queries)
        {
            int[] arr = new int[a.Length];

            for (int i = 0; i < a.Length; i++)
                arr[(i + k) % a.Length] = a[i];

            for (int i = 0; i < queries.Length; i++)
                queries[i] = arr[queries[i]];

            return queries;

            //int[] arrResult = new int[queries.Length];
            //int count = 0;
            //while (count != k)
            //{ 
            //    for (int i = 0; i < a.Length; i++)
            //    {
            //        int first = a[i];
            //        int last = a[a.Length - 1];
            //        a[i] = last;
            //        a[a.Length - 1] = first;
            //    }
            //count++;
            //}
            //for (int i=0; i < queries.Length; i++)
            //{
            //    arrResult[i] = a[queries[i]];
            //}

            //return arrResult;
        }
    }
}
