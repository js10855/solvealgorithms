﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class introTutorial
    {
        // Complete the introTutorial function below.
        public static int solution(int V, int[] arr)
        {                   
           return Array.IndexOf(arr, V);
        }

    }
}
