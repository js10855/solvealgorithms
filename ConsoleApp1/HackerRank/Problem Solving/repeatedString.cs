﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class repeatedString
    {
        public static long solution(string s, long n)
        {
            long result = 0;
            int len = s.Length;
            int initialCount= s.Where(x => x == 'a').Count();
            result = (n / len) * initialCount;
            int rem = Convert.ToInt32(n % len);
            if (rem != 0)
            {
                for (int i = 0; i < rem; i++)
                {
                    if (s[i] == 'a') result++;
                }
            }
            return result;
        }
    }
}
