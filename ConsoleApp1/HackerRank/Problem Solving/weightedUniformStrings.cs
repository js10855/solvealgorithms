﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class weightedUniformStrings
    {
        // Complete the weightedUniformStrings function below.
        public static string[] solution(string s, int[] queries)
        {
            int currentCharCount = 1;
            int lastAlphaNum = 0;
            HashSet<int> numList = new HashSet<int>();
            for (int i = 0; i < s.Length; i++)
            {
                int alphaNum = s[i] - 96;
                if (alphaNum == lastAlphaNum) currentCharCount++;
                else
                {
                    currentCharCount = 1;
                    lastAlphaNum = alphaNum;
                }
                numList.Add(alphaNum * currentCharCount);
            }

            string[] result = new string[queries.Length];
            for (int a0 = 0; a0 < queries.Length; a0++)
            {                
                result[a0]= (numList.Contains(queries[a0]) ? "Yes" : "No");
            }
            return result;

            
        }
    }
}
