﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class cutTheSticks
    {
        public static int[] solution(int[] arr)
        {
            ArrayList al = new ArrayList();            
            while (true)
            {
                int count = 0;
                int min = FindMin(arr);
                if (min == 0) break;
                for (int i = 0; i < arr.Length; i++)
                {                    
                    if (arr[i] > 0) { arr[i] = arr[i] - min; count++; }
                }                
                al.Add(count);                
            }
            return al.ToArray(typeof(int)) as int[];
            
        }

        private static int FindMin(int[] A)
        {
            int min = 0;
            foreach (int num in A)
            {
                if (min == 0) min = num;
                else
                {
                    if (num > 0 && num < min) min = num;
                }
            }
            return min;
        }
    }
}
