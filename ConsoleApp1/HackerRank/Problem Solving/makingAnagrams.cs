﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class makingAnagrams
    {
        // Complete the makingAnagrams function below.
        public static int solution(string s1, string s2)
        {
            //int result = 0;
            //var c1 = s1.ToCharArray();
            //var c2 = s2.ToCharArray();

            //foreach (char c in c1)
            //{
            //    if (!c2.Contains(c)) { result += 2; }
            //    else
            //    {
            //        int ind = Array.IndexOf(c2, c2.Where(x => x == c).First());
            //        c2[ind] = '0';
            //    }
            //}

            //result = result + Math.Abs(s1.Length-s2.Length);

            int[] chars = new int[26];
            foreach (char c11 in s1.ToCharArray()) { chars[c11 - 97]++; }
            foreach(char c22 in s2.ToCharArray()) { chars[c22 - 97]--; }
            int count = 0;
            foreach (int i in chars) { count += Math.Abs(i); }
            return count;

        }
    }
}
