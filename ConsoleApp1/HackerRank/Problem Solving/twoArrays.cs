﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class twoArrays
    {
        // Complete the twoArrays function below.
        public static string solution(int k, int[] A, int[] B)
        {
            string result = "YES";
            Array.Sort(A);
            Array.Sort(B);
            Array.Reverse(B);

            for (int i = 0; i < A.Length; i++)
            {
                bool res = false;
                if (A[i] + B[i] >= k)
                {
                    res = true;
                }
                if (!res)
                {
                    result = "NO"; break;
                }               
            }
            return result;
        }

        public int[] reverseArr(int[] arr)
        {
            //Method 1:
            int i = 0;
            int j = arr.Length - 1;
            while (i < j)
            {
                var temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j--;
            }
            return arr;

            //Method 2:
            //for (int i = 0; i < arr.Length / 2; i++)
            //{
            //    int tmp = arr[i];
            //    arr[i] = arr[arr.Length - i - 1];
            //    arr[arr.Length - i - 1] = tmp;
            //}

            //Method 3:
            //arr = (from a in arr
            //       orderby a descending
            //       select a).ToArray();
        }
    }
}
