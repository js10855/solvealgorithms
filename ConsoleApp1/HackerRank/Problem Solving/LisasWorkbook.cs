﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class LisasWorkbook
    {
        // Complete the workbook function below.
        public static int solution(int n, int k, int[] arr)
        {
            int result = 0;
            int pageno = 1;
            int arrVal = 0;
            int count = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                arrVal = arr[i];               
                for (int j = 1; j <= arrVal; j++)
                {
                    count++;
                    if (count > k) {
                        pageno++;
                        count = 1;                        
                    }
                    if (j == pageno) result++;
                }
                count = 0;
                pageno++;
            }

            return result;

        }
    }
}
