﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class separateNumbers
    {
        // Complete the separateNumbers function below.
        public static void solution(string s)
        {                
                bool valid = false;
                long firstx = -1;
                // Try each possible starting number
                for (int i = 1; i <= s.Length / 2; ++i)
                {
                    long x = long.Parse(s.Substring(0, i));
                    firstx = x;
                    // Build up sequence starting with this number
                    String test = x.ToString();
                    while (test.Length < s.Length)
                    {
                        test += (++x).ToString();
                    }
                    // Compare to original
                    if (test.Equals(s))
                    {
                        valid = true;
                        break;
                    }
                }
                Console.WriteLine(valid ? "YES " + firstx : "NO");
           

            //int previousNum = 0;
            //string tempNum = "";
            //int firstNum = 0;
            //bool result = true;
            //if (s.Length <= 1) Console.WriteLine("NO");
            //else
            //{
            //    for (int i = 0; i < s.Length; i++)
            //    {
            //        if (i == 0)
            //        {
            //            previousNum = (int)Char.GetNumericValue(s[i]);
            //            tempNum = previousNum.ToString();                       
            //            if (Char.GetNumericValue(s[i]) == 0) { Console.WriteLine("NO"); result = false; break; }
            //        }
            //        else
            //        {
            //            if (Char.GetNumericValue(s[i]) == previousNum) { tempNum += Char.GetNumericValue(s[i]); continue; }
            //            else {
            //                firstNum = Convert.ToInt32(tempNum);
            //                if  (Char.GetNumericValue(s[i]) - previousNum != 1) { Console.WriteLine("NO");result = false; break; }
            //            }
            //        }
            //        previousNum = (int)Char.GetNumericValue(s[i]);
            //    }

            //}
            //if (result)Console.WriteLine("YES " + firstNum);
        }
            
        
    }
}
