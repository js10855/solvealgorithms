﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class hackerrankInString
    {
        // Complete the hackerrankInString function below.
        public static string solution(string s)
        {
            string result = "NO";
            string strToCompare = "hackerrank";
            int j = 0;

            if (s.Length < strToCompare.Length)
                return result;
            else
            {                
                for (int i = 0; i < s.Length; i++)
                {
                    if (j<strToCompare.Length && s[i] == strToCompare[j]) j++;
                }
            }
            result = (j==strToCompare.Length)? "YES":"NO";
            return result;
        }
    }
}
