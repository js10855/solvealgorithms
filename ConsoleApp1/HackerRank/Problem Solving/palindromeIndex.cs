﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class palindromeIndex
    {
        // Complete the palindromeIndex function below.
        //This test case does not pass all test cases due to time out error.
        public static int solution(string s)
        {
            int flag = 0, result = 0;
            if (!CheckPalindrome(s))
            {
                for (int j = 0; j < s.Length; j++)
                {
                    if (CheckedPalindrome(j, s))
                    {
                        flag = 1;
                        result = j;
                        break;
                    }
                }
            }
            if (flag == 0) result = -1;
            return result;
        }

        private static bool CheckPalindrome(string s)
        {
            int result = 1;
            int i, k, flag = 0;
            for (i = 0, k = s.Length - 1; i < (s.Length) / 2 && k >= (s.Length) / 2; i++, k--)
            {
                if (s[i] != s[k])
                {
                    flag = 1;
                    result = 0;
                    break;
                }
            }
            return Convert.ToBoolean(result);
        }

        private static bool CheckedPalindrome(int j, string s1)
        {
            int result = 1;
            int i, k; for (i = 0, k = s1.Length - 1; i < (s1.Length) / 2 && k >= (s1.Length) / 2; i++, k--)
            {

                if (i == j)
                { i++; }
                if (k == j)
                { k--; }

                if (s1[i] != s1[k])
                {
                    result = 0;
                }

            }
            return Convert.ToBoolean(result);
        }
    }
}
