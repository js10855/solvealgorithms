﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class divisibleSumPairs
    {
        public static int solution(int n, int k, int[] ar)
        {
            int result = 0;
            for (int i = 0; i < ar.Length; i++)
            {
                for (int j = i + 1; j < ar.Length; j++)
                {
                    int sum = ar[i] + ar[j];
                    if (sum % k == 0) result++;
                }
            }
            return result;
        }
    }
}
