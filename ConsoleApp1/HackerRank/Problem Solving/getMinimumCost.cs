﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class getMinimumCost
    {
        //he'll multiply the price of each flower 
        //by the number of that customer's previously purchased flowers plus 1.
        //The first flower will be original price(0+1)*org.price,  
        //the next will (1+1)*org.price be  and so on.
        //n = number of flowers
        //k = number of friends
        public static int solution(int k, int[] c)
        {
            int minCost = 0;                        
            Array.Sort<int>(c,
                     new Comparison<int>(
                             (i1, i2) => i2.CompareTo(i1)
                     ));

               
            for (int i = 0; i < c.Length; i++)
                {
                   minCost += (i / k + 1) * c[i];
                }                
            
            return minCost;
        }
    }
}
