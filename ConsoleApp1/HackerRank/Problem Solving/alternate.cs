﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class alternate
    {
        // Complete the alternate function below.
        public static int solution(string s)
        {
            //int result = 0;
            //Dictionary<char, int> char_dict = new Dictionary<char, int>();
            //int[] char_arr = new int[26];

            //foreach (char c in s) char_arr[c - 97]++;
            //for (int i = 0; i < char_arr.Length; i++)
            //{
            //    if (char_arr[i] > 0)
            //    {
            //        char ch = (char)(i + 97);
            //        char_dict.Add(ch, char_arr[i]);
            //    }
            //}

            //for (int i = 0; i < char_dict.Count; i++)
            //{
            //    string tempStr = s;
            //    char ch = char_dict.Keys.ElementAt(i);
            //    tempStr = tempStr.Replace(ch.ToString(), "");
            //    if (isAlternate(tempStr))
            //    {
            //        result = Math.Max(result, tempStr.Length);
            //    }
            //    for (int j = i + 1; j < char_dict.Count; j++)
            //    {
            //        string tempStr2 = tempStr;
            //        ch = char_dict.Keys.ElementAt(j);
            //        tempStr2 = tempStr2.Replace(ch.ToString(), "");
            //        if (isAlternate(tempStr2))
            //        {
            //            result = Math.Max(result, tempStr2.Length);
            //        }
            //    }
            //}
            //return result;

            //check for below:
            //28

            //asdcbsdcagfsdbgdfanfghbsfdab
            //Ans is 8

            int temp = 0;
            for (int i = 0; i < s.Length; i++)
            {
                for (int j = i + 1; j < s.Length; j++)
                {
                    if (s[i] != s[j])
                    {
                        bool gfn = true;
                        Stack<char> sta = new Stack<char>();
                        foreach (char c in s)
                        {
                            if (c == s[i] || c == s[j])
                            {
                                sta.Push(c);
                            }
                        }
                        char[] ca = sta.ToArray();
                        for (int k = 0; k < ca.Length - 1; k++)
                        {
                            if (ca[k] != ca[k + 1] && gfn == true)
                            {
                                gfn = true;
                            }
                            else
                            {
                                gfn = false;
                            }
                        }
                        if (gfn == true && ca.Length > temp)
                        {
                            temp = ca.Length;
                        }
                    }
                }
            }
            return temp;
        }

        private static bool isAlternate(string s)
        {
            bool result = true;
            char prevC = ' ';            
            if (s.Length == 1) result = false;
            IEnumerable<char> char_arr = s.Distinct();
            if (char_arr.Count() > 2) result = false;
            for (int i = 0; i < s.Length; i++)
            {               
               if (s[i] == prevC) { result = false; break; }                
               prevC = s[i];               
            }
            return result;
        }

    }
}
