﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class alternatingCharacters
    {
        // Complete the alternatingCharacters function below.
        public static int solution(string s)
        {
            int result = 0;
            char previousCh='c';
            foreach(char c in s)
            {
                if (c == previousCh) result++;
                previousCh = c;
            }
            return result;
        }
    }
}
