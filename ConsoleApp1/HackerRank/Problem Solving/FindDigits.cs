﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class FindDigits
    {
        public static int solution(int n)
        {
            int[] arr = Array.ConvertAll(n.ToString().ToArray(), x => (int)x-48);
            int result = 0;
            foreach (int c in arr)
            {
               if (c == 0) continue;
               else if (n % c == 0) result++;                
            }
            return result;
        }
    }
}
