﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class jimOrders
    {
        // Complete the jimOrders function below.
        public static int[] solution(int[][] orders)
        {            
            int[] result = new int[orders.Length];
            Dictionary<int, int> dict = new Dictionary<int, int>();
            for (int i = 0; i < orders.Length; i++)
            {
                int val = orders[i][0] + orders[i][1];                
                dict.Add(i, val);
            }

            var items = from pair in dict
                        orderby pair.Value ascending
                        select pair;

            int count = 0;
            foreach (KeyValuePair<int, int> pair in items)
            {
                result[count] = pair.Key +1;
                count++;
            }      
            
            //Above loop using for loop
            for (int i = 0; i < items.Count(); i++)
            {

            }

            return result;
        }
    }
}
