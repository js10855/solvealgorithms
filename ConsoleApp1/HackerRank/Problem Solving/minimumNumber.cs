﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class minimumNumber
    {
        public static int solution(int n, string password)
        {
            // Return the minimum number of characters to make the password strong
            int result = 0;
            string numbers = "0123456789";
            string lower_case = "abcdefghijklmnopqrstuvwxyz";
            string upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string special_characters = "!@#$%^&*()-+";
            
            char[] num_arr = numbers.ToArray();
            char[] lower_arr = lower_case.ToArray();
            char[] upper_arr = upper_case.ToArray();
            char[] special_arr = special_characters.ToArray();
            if (password.IndexOfAny(num_arr)<0) result++;
            if (password.IndexOfAny(lower_arr) < 0) result++;
            if (password.IndexOfAny(upper_arr) < 0) result++;
            if (password.IndexOfAny(special_arr) < 0) result++;

            if (result + password.Length < 6) result = 6 - password.Length;
           
            return result;
        }
    }
}
