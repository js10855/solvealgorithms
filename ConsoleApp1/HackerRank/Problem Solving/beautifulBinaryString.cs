﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class beautifulBinaryString
    {
        // Complete the beautifulBinaryString function below.
        public static int solution(string b)
        {
            int result = 0;
            StringBuilder sb = new StringBuilder();
            foreach(char c in b)
            {
                sb.Append(c);
                if (sb.ToString().Contains("010")) { result++; sb.Clear(); }
            }
            return result;
        }
    }
}
