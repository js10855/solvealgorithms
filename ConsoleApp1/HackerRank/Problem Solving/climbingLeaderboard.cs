﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class climbingLeaderboard
    {
        public static int[] solution(int[] scores, int[] alice)
        {
            //solution 2
            //int[] result = new int[alice.Length];            
            //var output = scores.GroupBy(x => x);            

            //int max = scores[0];
            //for (int i = 0; i < alice.Length; i++)
            //{
            //    int key = 0;                
            //    if (alice[i] < max) {                    
            //        foreach (IGrouping<int, int> petGroup in output)
            //        {
            //            if (petGroup.Key > alice[i]) key++;
            //            else break;
            //        }
            //    }
            //    result[i] = key + 1;
            //}           
            //return result;

            //solution 1
            //Dictionary<int, int> dictScore = new Dictionary<int, int>();
            //int[] result = new int[alice.Length];

            //int previous = scores[0];
            //int rank = 1;
            //foreach (int num in scores)
            //{
            //    if (num == previous && !dictScore.ContainsKey(rank)) dictScore.Add(rank, num); else if (num != previous) { rank++; dictScore.Add(rank, num); }
            //    previous = num;
            //}
            //int max = scores[0];
            //for (int i = 0; i < alice.Length; i++)
            //{
            //    int key = 0;
            //    if (alice[i] < max) key = dictScore.Where(x => x.Value > alice[i]).Last().Key;
            //    result[i] = key + 1;
            //}
            //return result;
            int[] result = new int[alice.Length];
            int[] array = scores.Distinct().ToArray();
            int i = array.Length - 1;
            int count = 0;
            foreach (int score in alice)
            {
                while (i >= 0)
                {
                    if (score >= array[i]) i--;
                    else { result[count]=i + 2; break; }
                }
                if (i < 0) result[count]=1;
                count++;
            }
            return result;
        }
    }
}
