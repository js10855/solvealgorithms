﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class maxMin
    {
        public static int solution(int k, int[] arr)
        {
            int result = 0;
            int min = 0, max = 0;
            Array.Sort(arr);            

            for (int i = 0; i < arr.Length - k + 1; i++)
            {
                min = arr[i];
                max = arr[i + k - 1];
                if ((max - min) < result || i ==0)
                    result = max - min;
            }
            return result;

        }
    }
}
