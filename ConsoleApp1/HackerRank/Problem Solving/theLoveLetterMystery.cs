﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class theLoveLetterMystery
    {
        public static int solution(string s)
        {
            var ops = 0;

            // Ignore the character in the middle, if any (it will automatically be excluded by the condition in the for-loop)
            for (int i = 0, j = s.Length - 1; i < (s.Length / 2) && j >= (s.Length / 2); i++, j--)
                ops += Math.Abs(s[j] - s[i]);

            return ops;
        }
    }
}
