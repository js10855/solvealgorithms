﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class largestPermutation
    {
        // Complete the largestPermutation function below.
        public static int[] solution(int k, int[] arr)
        {
            //int count = 1;
            //int max = 0;           
            //int maxIndex = 0;

            //for (int i = 0; i < arr.Length-1; i++)
            //    {
            //        if (count == 1)
            //        {
            //            max = arr.Max();
            //            maxIndex = arr.ToList().IndexOf(max);                       
            //            if (arr[0] != max)
            //            {
            //                swap(arr, 0, maxIndex);                            
            //            }                        
            //        }
            //        else if(k>=count)
            //        {
            //            int val = arr[i + 1];
            //            max = GetMaxForStartPos(arr, i + 2);
            //            maxIndex = arr.ToList().IndexOf(max);
            //            if (max > val) swap(arr, i + 1, maxIndex);                                       
            //        }
            //        count++;
            //}            
            //return arr;


            
            //int n = arr.Length;            
            int[] index = new int[arr.Length + 1];
            for (int i = 0; i < arr.Length; i++)
            {               
                index[arr[i]] = i;
            }
            for (int i = 0; i < arr.Length && k > 0; i++)
            {
                if (arr[i] == arr.Length - i)
                {
                    continue;
                }
                arr[index[arr.Length - i]] = arr[i];
                index[arr[i]] = index[arr.Length - i];
                arr[i] = arr.Length - i;
                index[arr.Length - i] = i;
                k--;
            }
            return arr;
           

        }

        private static int GetMaxForStartPos(int[] arr,int startPos)
        {
            int max = 0;
            for (int i = startPos; i < arr.Length; i++)
            {
                if (arr[i] > max) max = arr[i];
            }
            return max;
        }

        private static void swap(int[] arr,int pos1,int pos2)
        {
            int temp = arr[pos1];
            arr[pos1] = arr[pos2];
            arr[pos2] = temp;
        }

    }
}
