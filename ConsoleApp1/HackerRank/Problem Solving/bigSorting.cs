﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class bigSorting
    {
        // Complete the bigSorting function below.
        public static string[] solution(string[] unsorted)
        {
            Array.Sort(unsorted, (string a, string b) => {
                if (a.Length == b.Length)
                    return string.Compare(a, b, StringComparison.Ordinal);
                return a.Length - b.Length;
            });
            return unsorted;

            //ulong[] result = new ulong[unsorted.Length];
            //for (int i = 0; i < unsorted.Length; i++)
            //{
            //    result[i]= Convert.ToUInt64(unsorted[i]);
            //}
            //Array.Sort(result);
            //for (int i = 0; i < result.Length; i++)
            //{
            //    unsorted[i]  = result[i].ToString();
            //}
            //return unsorted;
        }
    }
}
