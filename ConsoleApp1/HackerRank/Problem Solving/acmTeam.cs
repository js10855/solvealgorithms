﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class acmTeam
    {
        // Complete the acmTeam function below.
        public static int[] solution(string[] topic)
        {
            Dictionary<int, int> res = new Dictionary<int, int>();
            res.Add(topic.Length, topic[0].Length);

            int maxTopic = 0, noofTeams = 0;
            int[] result = new int[2];
            for (int i = 0; i < topic.Length; i++)
            {
                int knowtopic = 0;
                string val1 = topic[i];
                for (int j = 0; j< val1.Length; j++)
                {
                    if (Convert.ToInt32(val1[j]) == 49) knowtopic++;
                    else
                    {
                        for (int k = i + 1; k < topic.Length; k++)
                        {
                            if (k < topic.Length)
                            {
                                string val2 = topic[k];
                                if (Convert.ToInt32(val2[j]) == 49) knowtopic++;
                            }
                        }
                        if (knowtopic >= maxTopic) noofTeams++;
                        maxTopic = Math.Max(knowtopic, maxTopic);
                    }
                }                    
            }
            result[0] = maxTopic;
            result[1] = noofTeams-1;
            return result;
        }

    }
}
