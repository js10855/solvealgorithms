﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class marsExploration
    {
        // Complete the marsExploration function below.
        public static int solution(string s)
        {
            int result = 0;            
            int count = 0;

            foreach (char c in s)
            {
                if (count == 3) count = 1;
                else count++;

                if (count == 1 || count ==3)
                {
                    if (c != 'S') result++;
                }
                else if (count==2)
                {
                    if (c != 'O') result++;
                }
            }
            return result;

            //Second solution
            //String sos = "SOS";
            ////int count = 0;
            //for (int i = 0; i < s.Length; i++)
            //{
            //    if (s[i] != sos[i % 3]) count++;
            //}
            //return count;
        }
    }
}
