﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class appendAndDelete
    {
        // Complete the appendAndDelete function below.
        public static string solution(string s, string t, int k)
        {
            //string result = "";
            //int deletion = 0, append =0;
            //if (s == t) result = "Yes";
            //else 
            //{
            //    int val = Math.Min(s.Length, t.Length);
            //    if (s.Length>t.Length ) deletion = s.Length - t.Length;
            //    else if (s.Length<t.Length) append = t.Length - s.Length;
            //    for (int i = 0; i < val; i++)
            //    {
            //        if (s[i] != t[i])
            //        {
            //            deletion = s.Length - i;
            //            append = t.Length - i;
            //            break;
            //        }
            //    }
            //    if (deletion + append == k) result = "Yes";
            //    else result = "No";
            //}

            //return result;
            int commonLength = 0;
            string result = "";
            for (int i = 0; i < Math.Min(s.Length, t.Length); i++)
            {
                if (s[i] == t[i])
                    commonLength++;
                else
                    break;
            }
            //CASE A
            if ((s.Length + t.Length - 2 * commonLength) > k)
            {
                result = "No";
            }
            //CASE B
            else if ((s.Length + t.Length - 2 * commonLength) % 2 == k % 2)
            {
                result = "Yes";
            }
            //CASE C
            else if ((s.Length + t.Length - k) < 0)
            {
                result = "Yes";
            }
            //CASE D
            else
            {
                result = "No";
            }
            return result;




        }
    }
}
