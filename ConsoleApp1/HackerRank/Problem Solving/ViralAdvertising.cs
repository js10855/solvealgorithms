﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class ViralAdvertising
    {
        public static int solution(int n)
        {
            int start = 5;
            int mod = 2;
            int result = 0;
            for (int i = 1; i <= n; i++)
            {
                if (i == 1) result = start / mod;
                else { start = (start / mod) * 3; result += start/mod; }
            }
            return result;
        }
    }
}
