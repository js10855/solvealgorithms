﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class SinglyLinkedListNode
    {
        public int data;
        public SinglyLinkedListNode next;

        public SinglyLinkedListNode(int nodeData)
        {
            this.data = nodeData;
            this.next = null;
        }
    }

    class SinglyLinkedList
    {
        public SinglyLinkedListNode head;
        public SinglyLinkedListNode tail;

        public SinglyLinkedList()
        {
            this.head = null;
            this.tail = null;
        }

        public void InsertNode(int nodeData)
        {
            SinglyLinkedListNode node = new SinglyLinkedListNode(nodeData);

            if (this.head == null)
            {
                this.head = node;
            }
            else
            {
                this.tail.next = node;
            }

            this.tail = node;
        }

        public static SinglyLinkedListNode insertNodeAtTail(SinglyLinkedListNode head, int data)
        {
            //SinglyLinkedListNode node = new SinglyLinkedListNode(data);

            //if (head == null)
            //{
            //    head = node;
            //}
            //else
            //{
            //    head.next = node;
            //}
            //Console.WriteLine(data);
            //return node;
            SinglyLinkedListNode node = new SinglyLinkedListNode(data);
            if (head == null)
            {
                head = node;                
            }
            else
            {
               node = head;
                while (node.next != null)
                {
                    node = node.next;
                }
                node.next = new SinglyLinkedListNode(data);
                //node.next.data = data;
            }
            return head;

        }
    }

    class printLinkedList
    {
        static void solution(SinglyLinkedListNode head)
        {
            Console.WriteLine(head.data);
            SinglyLinkedListNode llistNode = head.next;
            solution(llistNode);
        }

        static void PrintSinglyLinkedList(SinglyLinkedListNode node, string sep)
        {
            while (node != null)
            {
                Console.WriteLine(node.data);

                node = node.next;

                if (node != null)
                {
                    Console.WriteLine(sep);
                }
            }
        }

    }


}
