﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class PriyankaAndToys
    {
        public static int solution(int[] w)
        {
            //once container can contain only 5 units
            int minWeight = 0;
            int result = 0;
            Array.Sort(w);
            for (int i = 0; i < w.Length; i++)
            {
                if (i == 0 || w[i] > minWeight + 4) { minWeight = w[i]; result++; }                
            }
            return result;
        }
    }
}
