﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class jumpingOnClouds1
    {
        public static int solution(int[] c)
        {
            int num = 0;
            int result = 0;
            while (num < c.Length)
            {
                if (num + 2 < c.Length)
                {
                    if (c[num + 2] != 1) { result++; num = num + 2; }
                    else if (c[num + 1] != 1) { result++; num = num + 1; }
                }
                else if (num + 1 < c.Length)
                {
                    if (c[num + 1] != 1) { result++; num = num + 1; }
                }
                else break;
            }
            return result;
        }
    }
}
