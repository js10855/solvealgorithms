﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class funnyString
    {
        // Complete the funnyString function below.
        public static string solution(string s)
        {
            string result = "Funny";
            string b = string.Join("",s.Reverse());

            //Another way of doing string reverse
            //string s = "Mounica";
            //string reverse = "";

            //for (int i = s.Length - 1; i >= 0; i--)
            //{
            //    reverse += s[i];
            //}
            //Console.WriteLine(reverse);


            for (int i = 0; i < s.Length-1; i++)
            {
                int diff1 = Math.Abs((s[i] - 97)-(s[i+1]-97)) ;
                int diff2 = Math.Abs((b[i] - 97) - (b[i + 1] - 97));                
                if (diff1 != diff2) { result = "Not Funny"; break; }                             
            }
            return result;
        }
    }
}
