﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class equalizeArray
    {
        public static int solution(int[] arr)
        {
            Array.Sort(arr);
            int result =0;
            int previousVal = 0;
            int maxCount = 0;
            for(int i=0;i<arr.Length;i++)
            {
                int count = arr.Where(x => x == arr[i]).Count();
                if (count > maxCount) maxCount = count;
                if (result == 0 || arr[i] != previousVal) result += count;                
                previousVal = arr[i];
                i = i + count-1;                
            }
            return result-maxCount;
        }
    }
}
