﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class SherlockAndValidString
    {
        // Complete the isValid function below.
        public static string solution(string s)
        {
           string GOOD = "YES";
           string BAD = "NO";

            if (string.IsNullOrEmpty(s)) return BAD;
            if (s.Length <= 3) return GOOD;

            int[] letters = new int[26];
            for (int i = 0; i < s.Length; i++)
            {
                letters[s[i] - 'a']++;
            }
            Array.Sort(letters);
            int j = 0;
            while (letters[j] == 0)
            {
                j++;
            }
            int min = letters[j];   //the smallest frequency of some letter
            int max = letters[25]; // the largest frequency of some letter
            String ret = BAD;
            if (min == max) ret = GOOD;
            else
            {
                // remove one letter at higher frequency or the lower frequency 
                if (((max - min == 1) && (max > letters[24])) ||
                    (min == 1) && (letters[j + 1] == max))
                    ret = GOOD;
            }
            return ret;

            //s = "aabbcd"; //"abcdefghhgfedecba";
            //string result = "YES";
            //int[] chars = new int[26];
            //foreach (char c in s)   {chars[c - 97]++;}                        
            //IEnumerable<int> dist= chars.Distinct().Where(x=>x!=0);           
            //int first = dist.FirstOrDefault();
            //bool oneSubtract = false;
            //foreach (int num in dist)
            //{
            //    if (num == first) { }
            //    else if (Math.Abs(num - first) == 1 && !oneSubtract) { oneSubtract = true; }                
            //    else { result = "NO"; break; }               
            //}
            //return result;

            //int max = chars.Max();
            //foreach (int num in chars)
            //{
            //    if (num < max && num==max-1 && !oneSubtract)
            //    {
            //        oneSubtract = true;
            //    }
            //    else { result = "NO"; break; }


            //if (num != max && num !=0)
            //    {
            //        if (num != max - 1 || oneSubtract)
            //        { 
            //            if (oneSubtract) { result = "NO"; break; }
            //            else oneSubtract = true;
            //        }
            //        else oneSubtract = true;
            //}
            //}
            //return result;
        }
    }
}
