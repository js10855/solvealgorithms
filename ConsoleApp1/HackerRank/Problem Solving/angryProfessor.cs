﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class angryProfessor
    {
        public static string solution(int k, int[] a)
        {            
            int val = a.Where(x => x <= 0).Count();
            if (val < k) return "YES";
            else return "NO";            
        }
    }
}
