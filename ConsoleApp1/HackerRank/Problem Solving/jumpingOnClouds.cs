﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class jumpingOnClouds
    {
        public static int solution(int[] c, int k)
        {
            int result = 100;
            int counter = 0;
            do
            {               
                if (c[counter] == 0) result--;
                else result = result - 1 - 2;
                if (counter + k >= c.Length) counter = 0;
                //else if (counter + k > c.Length) counter = c.Length - 1 - k;
                else counter += k;
            }
            while (counter != 0);

            return result;
        }
    }
}
