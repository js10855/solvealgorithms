﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class SherlockAndSquares
    {
        public static int solution(int a, int b)
        {
            int count = 0;
            int num1 = Convert.ToInt32(Math.Sqrt(a));
            int num2 = Convert.ToInt32(Math.Sqrt(b));

            for (int i = num1; i <= num2; i++)
            {
                long val = i * i;
                if (val >= a && val <= b) count++;
            }
            return count;
        }
    }
}
