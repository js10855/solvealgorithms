﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class gameOfThrones
    {
        // Complete the gameOfThrones function below.
        public static string solution(string s)
        {
             string result = "YES";
             int charCount = 0;
             Dictionary<string, int> dict = new Dictionary<string, int>();            

             foreach(var c in s)
            {                
                if (!dict.ContainsKey(c.ToString()))
                {
                    int count = s.Where(x => x == c).Count();
                    dict.Add(c.ToString(), count);
                }
                s = s.Replace(c.ToString(), "");
            }

             //for (int i = s.Length-1; i >=0 ; i--)
             //{
             //   int count = s.Where(x => x == s[i]).Count();
             //   if (!dict.ContainsKey(s[i].ToString()))
             //   {
             //       dict.Add(s[i].ToString(), count);
             //   }
             //   s=s.Replace(s[i].ToString(), "");
             //}

             foreach(KeyValuePair<string,int>c in dict)
            {
                if (c.Value % 2 != 0) charCount++;
            }

            if (charCount > 1) result = "NO";
            return result;
        }
    }
}
