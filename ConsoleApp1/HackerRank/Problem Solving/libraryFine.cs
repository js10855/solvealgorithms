﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class libraryFine
    {
        public static int solution(int d1, int m1, int y1, int d2, int m2, int y2)
        {
            //Hacker Rank Day 26 Nested logic problem
            int[] returnDate = Array.ConvertAll(Console.ReadLine().Split(' '), hTemp => Convert.ToInt32(hTemp));
            int[] ExpectedDate = Array.ConvertAll(Console.ReadLine().Split(' '), hTemp => Convert.ToInt32(hTemp));
            int result = 0;

            if (returnDate[2] > ExpectedDate[2]) result = 10000;
            else if (returnDate[1] > ExpectedDate[1] && returnDate[2] >= ExpectedDate[2])
            {
                { result = 500 * Math.Abs(ExpectedDate[1] - returnDate[1]); }
            }
            else if (returnDate[0] > ExpectedDate[0])
            {
                if (returnDate[2] >= ExpectedDate[2] && returnDate[1] >= ExpectedDate[1])
                    result = 15 * Math.Abs(ExpectedDate[0] - returnDate[0]);
            }
            else result = 0;
            return result;
        }
    }
}
