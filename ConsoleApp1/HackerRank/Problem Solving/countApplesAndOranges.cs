﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank.Problem_Solving
{
    class countApplesAndOranges
    {
        // Complete the countApplesAndOranges function below.
        public static void solution(int s, int t, int a, int b, int[] apples, int[] oranges)
        {
            int appleCount = 0,orageCount = 0;
            foreach(int apple in apples)
            {
                int val = a + apple;
                if (val >= s && val <= t) appleCount++;
            }
            foreach (int orange in oranges)
            {
                int val = b + orange;
                if (val >= s && val <= t) orageCount++;
            }
            Console.WriteLine(appleCount);
            Console.WriteLine(orageCount);
        }
    }
}
