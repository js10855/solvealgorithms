﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.HackerRank._30_Days_of_code
{
    class Day29_Bitwise_AND
    {
        public static int solution(int n, int k)
        {
            int max = 0;
            for (int i = 1; i < n-1; i++)
            {
                for (int j = i + 1; j<= n; j++)
                {
                    int num = i & j;
                    if (num > max && num < k) max = num;
                }
            }
            return max;
        }
    }

}
