﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.CCI
{
    class differenceOfPair
    {
        public static int solution(int[] arr,int k)
        {
            int result = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (Math.Abs(arr[i] - arr[j]) == k) result++;
                }
            }
            return result;
        }
    }
}
