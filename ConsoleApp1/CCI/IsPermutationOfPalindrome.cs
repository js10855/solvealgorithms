﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.CCI
{
    class IsPermutationOfPalindrome
    {
        public static bool solution(string s)
        {
            bool result = false;
            int[] charA = new int[26];
            foreach (var item in s)
            {
                var val = char.ToLower(item) - 'a';
                if (0 <= val && val <= 25) { }                
                else val =-1;
                if (val !=-1) charA[val]++;
                //This line will work only for lowercase characters
                //charA[item - 97]++;
            }
            foreach (var count in charA)
            {
                if (count % 2 == 1)
                {
                    if (result)
                    {
                        return false;                        
                    }
                    result = true;
                }
            }
            return result;
        }
    }
}
