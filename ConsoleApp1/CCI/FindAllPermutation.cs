﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.CCI
{
    class FindAllPermutation
    {
        public static void Solution(string b, string s)
        {
            ArrayList al = new ArrayList();
            string temps = s;
            for (int i = 0; i < b.Length; i++)
            {
                if (temps.Contains(b[i]))
                {
                    temps = temps.Remove(temps.IndexOf(b[i]), 1);
                    if (string.IsNullOrEmpty(temps))
                    {
                        al.Add(i - 3);
                        temps = s;
                    }
                }
                else temps = s;
            }
            foreach (int val in al)
            {
                Console.WriteLine(val);
            }
            Console.ReadLine();
        }
    }
}
