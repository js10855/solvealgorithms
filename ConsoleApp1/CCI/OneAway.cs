﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.CCI
{
    class OneAway
    {
        public static bool solution(string a, string b)
        {
            bool result = true;
            if (a.Length == b.Length || Math.Abs(a.Length - b.Length) == 1)
            {
                int[] charA = new int[26];
                int[] charB = new int[26];
                foreach (var item in a) charA[item - 97]++;
                foreach (var item in b) charB[item - 97]++;
                //bool foundOdd = false;
                int counter = 0;
                for (int i = 0; i < charA.Length; i++)
                {
                    if (charA[i] != charB[i])
                    {
                        //if (foundOdd)  return false; 
                        //foundOdd = true;
                        counter++;
                        if (counter > 2) return false;
                    }
                }
            }
            else result = false;
            return result;
        }
    }
}
