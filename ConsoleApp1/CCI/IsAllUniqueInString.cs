﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.CCI
{
    class IsAllUniqueInString
    {
        public static bool solution(string s)
        {
            bool result = true;
            int[] num = new int[128];
            foreach (char c in s)
            {
                num[c]++;
                if (num[c] > 1) { result = false; break; }
            }
            return result;
        }
    }
}
