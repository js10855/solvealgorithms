﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.CCI
{
    class IsPermutation
    {
        public static bool solution (string word1, string word2)
        {
            bool result = true;
            int[] charA = new int[26];
            int[] charB = new int[26];
            foreach (var item in word1) charA[item-97]++;
            foreach (var item in word2) charB[item-97]++;

            for (int item = 0; item < charA.Length; item++)
            {
                if (charA[item] != charB[item])
                {
                    result = false;
                    break;
                }
            }
            return result;
        }
    }
}
