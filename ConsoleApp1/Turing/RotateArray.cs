﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Turing
{
    /*Given an array, Rotate the arrayh to the right by k steps, where k is non negative*/
    //Time complexity: O(n +n) =O(2n)
    //Space complexity: O(n)
    internal class RotateArray
    {
        public void Rotate(int[] nums, int k)
        {
            int[] result = new int[nums.Length];
            for (int i = 0; i < nums.Length; i++)
            {
                result[(i + k) % nums.Length] = nums[i];
            }

            for (int i = 0; i < nums.Length; i++)
            {
                nums[i] = result[i];
            }
            
        }
    }
}
