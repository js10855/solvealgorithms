﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Turing
{
    /* 
     * Given an array of integers, a lucky integer is an integer that has a frequency in the array
     * equal to its value. 
     * Return the largest lucky integer in the array. If there is no lucky integer return -1;
     */
    // Time Complexity : O(n + n)=O(2n)
    // Space Compexity: O(n)
    internal class LuckyInteger
    {
        public int findLucky(int[] arr)
        {
            Dictionary<int, int> map = new Dictionary<int, int>();
            foreach (int val in arr)
            {
                if (!map.ContainsKey(val)) map.Add(val, 1);
                else map[val]++;
            }

            int largest = -1;
            foreach(KeyValuePair<int,int> kvp in map)
            {
                if (kvp.Key == kvp.Value && kvp.Key>largest) largest = kvp.Key;
            }
            return largest;

        }
    }
}
