﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Turing
{
    internal class CalculatePoints
    {
        public int CalPoint(string[] ops)
        {
            int sum = 0;
            List<int> prev = new List<int>();
            foreach (var val in ops)
            {
                int num = 0;
                if (int.TryParse(val, out num))
                {
                    sum += num;
                    prev.Add(num);
                }
                else if (val == "+")
                {
                    sum += prev[prev.Count - 1] + prev[prev.Count - 2];
                    prev.Add(prev[prev.Count - 1] + prev[prev.Count - 2]);
                }
                else if (val == "D")
                {
                    sum += prev[prev.Count - 1] * 2;
                    prev.Add(prev[prev.Count - 1] * 2);
                }
                else if (val == "C")
                {
                    sum -= prev[prev.Count - 1];
                    prev.RemoveAt(prev.Count - 1);
                    
                }
            }
            return sum;
        }
       
	

	
    }
}
