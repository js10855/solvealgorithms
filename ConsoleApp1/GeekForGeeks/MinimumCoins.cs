﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.GeekForGeeks
{
    class MinimumCoins
    {
        public static int solution(int[] coins,int change)
        {
            //int[] result = new int[coins.Length];
            //while (value > 0)
            //{
            //    for (int i = 0; i < coins.Length; i++)
            //    {
            //        if (coins[i] <= value)
            //        {
            //            result[i] = coins[i];
            //            value = value - coins[i];
            //            if (value == 0) break;
            //        }
            //    }
            //}
            //return result;

            int[] counts = new int[change + 1];
            counts[0] = 0;

            for (int i = 1; i <= change; i++)
            {
                int count = int.MaxValue;
                foreach (int coin in coins)
                {
                    int total = i - coin;
                    if (total >= 0 && count > counts[total])
                    {
                        count = counts[total];
                    }
                }
                counts[i] = (count < int.MaxValue) ? count + 1 : int.MaxValue;
            }
            return counts[change];
        }
    }
    }

