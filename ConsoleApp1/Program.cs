﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using ConsoleApp1.TheDailyByte;

namespace ConsoleApp1
{
    class Program
    {
        static String location;
        static DateTime time;
        static double radius;
        delegate double AreaCalculator(int r);

        static void Main(string[] args)
        {

            //GFGAndCCIProblem();
            //CheckForDouble();
            //CheckForExpressionTree();
            //HackerRankProblem();
            //CodilityProblem();          
            //CheckForBoxingAndUnboxing();
            //CheckForDelegatePredicateFuncLambda();
            //CheckForLinQ();
            // CheckForShortestMissingInteger();
            //CheckForReturnTypeOverloading();
            //CheckForDateAndNull();
            //CheckForArray();
            //CheckForOutParam();
            // CheckIEnumerableAndIEnumerator();
            // CheckMainThread();
            // CheckEquals();
            //CheckForNullable();
            // CheckForEnum();
            // CheckDataTypes();
            //CheckForDynamicAndVar();
            //CheckForTimeNullCompare();
            //CheckForMultipleCatch();
            //CheckForNullValue();
            //SumAllEvenNum();            
            //SendEmail();
            // TheDailyByte.SpotTheDifference.solution("a","aa");
            //TheDailyByte.UncommonFromSentences.solution("apple apple", "banana");
            //int[] num1 = { 1, 2, 3 };
            //int[] num2 = { 2, 4, 6 };
            //LeetCode.FindDifference.solution(num1,num2);

            //Console.WriteLine(TheDailyByte.AddBinary.solution("100", "1"));
            //Console.ReadLine();

            //LinkedListFunctions.AddMain();
            //TheDailyByte.LinkedListFunctions.LinkedListFunctions.AddNodes();
            // string s = "a##c";
            //string t = "#a#c";

            //  Console.WriteLine(TheDailyByte.CompareKeystrokes.BackspaceCompare(s, t));

            //string s = "aababaab";
            //Console.WriteLine(TheDailyByte.RemoveAdjacentDuplicates.RemoveDuplicates(s));
            //int[] num1 = { 2,4 };
            //int[] num2 = { 1, 2, 3, 4 };
            //TheDailyByte.GreaterElements.NextGreaterElement(num1, num2);
            //ListNode node = new ListNode(1);            
            //node.AddToEnd(2);
            //node.AddToEnd(3);
            //node.AddToEnd(4);
            //node.AddToEnd(5);

            //node.Print();
            //Console.WriteLine();

            //RemoveNthToLastNode.RemoveNthFromEnd(node, 2);

            //node.Print();
            //string s = "a1b2";
            //var list = StringPermutations.LetterCasePermutation(s);
            //foreach(var str in list)
            //{
            //    Console.WriteLine(str);
            //}

            //string s = "23";
            //GenerateTextMessages.LetterCombinations(s);

            //char[][] board = new char[3][];
            //board[0] = new char[4] { 'C', 'A', 'T', 'F' };
            //board[1] = new char[4] { 'B', 'G', 'E', 'S' };
            //board[2] = new char[4] { 'I', 'T', 'A', 'E' };           
            //string word = "CAT";
            //Console.WriteLine(WordSearch.Exist(board, word));

            //Program pm = new Program();
            //pm.PrintSeries(3);
            //PrintSeries(3);

            //int[][] grid = new int[3][];
            //grid[0] = new int[3] {0,2,0};
            //grid[1] = new int[3] {8,6,3 };
            //grid[2] = new int[3] {0,9,0 };

            //GoldRush gr = new GoldRush();
            //Console.WriteLine(gr.GetMaximumGold(grid));
            //ParenthesesGeneration pg = new ParenthesesGeneration();
            //var list = pg.GenerateParenthesis(3);
            //foreach (var item in list)
            //{
            //    Console.WriteLine(item);
            //}

            // UniqueCombinations uc = new UniqueCombinations();
            // var list = uc.CombinationSum(new int[] { 2, 3, 6, 7 }, 7);
            // PalindromeSplitting ps = new PalindromeSplitting();
            //ps.Partition("aab");

            // MaxPoints mp = new MaxPoints();
            // Console.WriteLine(mp.BagOfTokensScore(new int[] { 300 }, 200));

            // ThrowingStones ts = new ThrowingStones();
            //Console.WriteLine(ts.LastStoneWeight(new int[] {2, 7,4,1,8,1 }));
            //Console.WriteLine(ts.LastStoneWeight(new int[] { 10,4,2,10 }));

            //PopsicleStand ps = new PopsicleStand();
            //Console.WriteLine(ps.LemonadeChange(new int[] {5,5,10,10,20 }));
            //Console.WriteLine(ps.LemonadeChange(new int[] { 5, 5, 5, 5, 20, 20, 5, 5, 5, 5 }));
            //FlightPrices fp = new FlightPrices();
            //int[][] costs = new int[6][];
            //costs[0] = new int[2] {259,770};
            //costs[1] = new int[2] {448,54 };
            //costs[2] = new int[2] { 926, 667 };
            //costs[3] = new int[2] { 184, 139 };
            //costs[4] = new int[2] { 840, 118 };
            //costs[5] = new int[2] { 577, 469 };
            // fp.TwoCitySchedCost(costs);

            //MovingBricks mb = new MovingBricks();
            //mb.findNumberOfBricks(new int[] { 1000, 200, 150, 200 });

            //WhoWins ww = new WhoWins();
            //Console.WriteLine(ww.PredictTheWinner(new int[] {1,5,233,7 }));

            // Stairmaster sm = new Stairmaster();
            // sm.MinCostClimbingStairs(new int[] { 10, 15, 20 });

            //MakingChange mc = new MakingChange();
            //Console.WriteLine(mc.CoinChange(new int[] { 186, 419, 83, 408 }, 6249));

            //int[][] costs = new int[6][];
            //costs[0] = new int[2] { 1, 1 };
            //costs[1] = new int[2] { 2, 2 };
            //costs[2] = new int[2] { 1, 1 };
            //costs[3] = new int[2] { 1, 2 };
            //costs[4] = new int[2] { 1, 2 };
            //costs[5] = new int[2] { 1, 1 };
            //LeetCode.DominoPairs dp = new LeetCode.DominoPairs();
            //dp.NumEquivDominoPairs(costs);
            //LongestCommonSubsequenceClass lc = new LongestCommonSubsequenceClass();
            //Console.WriteLine(lc.LongestCommonSubsequence("abca", "acea"));
            //ComplementaryNumbers cn = new ComplementaryNumbers();
            //cn.FindComplement(5);
            //OneGoneMissing onG = new OneGoneMissing();
            //onG.MissingNumber(new int[] {3,0,1 });
            //BuggySoftware bs = new BuggySoftware();
            //bs.FirstBadVersion(5);
            //Trie trie = new Trie();
            //trie.Insert("apple");
            //trie.Search("apple");
            //trie.Search("app");
            //trie.StartsWith("app");
            //Stairs stair = new Stairs();
            //Console.WriteLine(stair.ClimbStairs(4));
            //int[][] costs = new int[3][];            
            //costs[0] = new int[3] { 1, 1,1 };
            //costs[1] = new int[3] { 1, 1,1 };
            //costs[2] = new int[3] { 1, 1,1 };
            //PaintingHouses ph = new PaintingHouses();
            //Console.WriteLine(ph.minCost(costs));
            //CountingPrimes cp = new CountingPrimes();
            //Console.WriteLine(cp.CountPrimes(7));
            // ReapingChildren RC = new ReapingChildren();
            // var result = RC.GetProcessIds(new int[] { 2, 4, 3, 7 }, new int[] {0,2,2,3 },3);
            //  var result = RC.GetProcessIds(new int[] { 1,3,10,5,20,40 }, new int[] { 3,0,5,3,10,20 }, 5);
            //var result = RC.GetProcessIds(new List<int> { 1, 2,3 }, new List<int> {  0, 1, 1 }, 2);
            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}
            //TreeNode tn = new TreeNode();
            //tn.val = 2;
            //TreeNode left = new TreeNode();
            //tn.left = left;
            //tn.left.val = 1;

            //TreeNode right = new TreeNode();
            //tn.right = right;
            //tn.right.val = 3;

            //IterativeInorderTraversal iT = new IterativeInorderTraversal();
            //iT.InorderTraversal(tn);
            // CharacterScramble cS = new CharacterScramble();
            // Console.WriteLine( cS.IsScramble("dog", "didnotgo"));
            // Console.WriteLine(cS.CountCharacters(new string[] { "hello", "world", "leetcode" }, "welldonehoneyr"));
            //ReverseVowelsClass rvc = new ReverseVowelsClass();
            //Console.WriteLine(rvc.ReverseVowels("hello"));

            // ListNode node = new ListNode(1);
            //node.AddToEnd(2);
            //node.AddToEnd(3);
            //node.AddToEnd(4);
            // RemoveValue rv = new RemoveValue();
            // rv.RemoveElements(node, 3);
            //FindMiddleElement fme = new FindMiddleElement();
            //fme.MiddleNode(node);
            //Compression c = new Compression();
            //c.Compress(new char[] { 'a', 'a', 'b', 'b', 'c', 'c' });

            // RemoveVowelClass rvc = new RemoveVowelClass();
            // Console.WriteLine(rvc.RemoveVowel("xyz"));

            //int[][] grid = new int[3][];
            //grid[0] = new int[3] { 1, 1, 3 };
            //grid[1] = new int[3] { 2, 3, 1 };
            //grid[2] = new int[3] { 4, 6, 1 };
            //MinimizePath mp = new MinimizePath();
            //Console.WriteLine(mp.MinPathSum(grid));

            //HammingDistanceClass hd = new HammingDistanceClass();
            //hd.HammingDistance(680142203, 1111953568);
            //char[] chars = new char[] { 'a', 'a', 'b', 'b', 'c', 'c' };
            //char[] chars = new char[] { 'a', 'a', 'a', 'a', 'a', 'a' };
            //Compression c = new Compression();
            //Console.WriteLine(c.Compress(chars));
            // RearrangeElements re = new RearrangeElements();
            // re.MoveZeroes(new int[] { 3, 7, 0, 5, 0, 2 });
            //Averages ave=new Averages();
            // ave.AverageOfLevels()
            // KeyboardRow kr = new KeyboardRow();
            // kr.FindWords(new string[] { "Hello", "Alaska", "Dad", "Peace"});
            //IdenticalElements ie = new IdenticalElements();
            //ie.ContainsNearbyDuplicate(new int[] { 1, 0, 1, 1 }, 1);

            //TreeNode bt = new TreeNode(new int[] {1,6,8,1,5});           
            //Averages ave = new Averages();
            //ave.AverageOfLevels(bt);

            //TreeNode root = new TreeNode(10);

            //root.left = new TreeNode(5);
            //root.right = new TreeNode(15);

            //root.left.left = new TreeNode(3);
            //root.left.left = new TreeNode(7);

            ////root.right.left = new TreeNode(3);
            //root.right.right = new TreeNode(18);
            //SumWithinBounds sb = new SumWithinBounds();
            //sb.RangeSumBST(root, 7, 15);
            //LeetCode.RansomNote rn = new LeetCode.RansomNote();
            //rn.CanConstruct("aa", "aab");

            //Turing.Test1 test = new Turing.Test1();
            //test.CalPoint(new string[] { "5", "2", "C", "D", "+" });
            //Turing.RotateArray ra = new Turing.RotateArray();
            //ra.Rotate(new int[] { 1, 2, 3, 4, 5, 6, 7 }, 3);
            // SwapWords sw = new SwapWords();
            // sw.ReverseWords("a good   example");
            //int[] nums  = {1,2,3,4 };
            //Products p = new Products();
            //p.ProductExceptSelf(nums);

            // ClassPresident cp = new ClassPresident();
            // cp.findPresident(new int[] { 1, 3, 2, 3, 1, 2, 3, 3, 3 });

            //Partners p = new Partners();
            //p.findPartnerCounts(new int[] { 5, 5, 3, 1, 1, 3, 3 });
            // IndexOf i = new IndexOf();
            // Console.WriteLine( i.StrStr("abc", "xyz"));
            // CracktheCode cr = new CracktheCode();
            // Console.WriteLine(cr.WordPattern("abba", "dog cat cat dog"));

            //int[][] intervals = new int[2][];
            //intervals[0] = new int[2] {1,3};
            //intervals[1] = new int[2] {1,8 };
            //// intervals[2] = new int[2] {0,9 };
            //CombineTimesfor ctf = new CombineTimesfor();
            //ctf.Merge(intervals);
            AddOne ao = new AddOne();
            var values= ao.PlusOne(new int[] { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0});
            foreach(var num in values)
            {
                Console.Write(num);
            }

            Console.ReadLine();
        }

        

        static int x = 0;

        void PrintSeries(int NumTimes)
        {
            int LocalNo;            

            //Return to the caller when NumTimes is 0. >100 is to avoid Stack overflow.
            if (NumTimes == 0 || NumTimes > 100)
                return;

            //Print the value in x and store that in Local Variable LocalNo.
            x++;
            Console.WriteLine("X:{0}", x);
            LocalNo = x;

            //Make a recursive call giving a parameter by reducing it value by 1. Helpful to make a exit based on NumTimes == 0

            NumTimes = NumTimes - 1;
            PrintSeries(NumTimes);


            //See the Magic. It makes you to understand what is Function stack
            Console.WriteLine("LocalNo:{0}", LocalNo);
            

            //The value in the Static variable at the bottom of the call statck
            if (LocalNo == 1)
                Console.WriteLine("Static variable value: {0} ", x);
        }


      

        static void GFGAndCCIProblem()
        {
            //Given an array of distinct integer values, 
            //count the number of pairs of integers that
            //have difference k.
            //For example, given the array { 1, 7, 5, 9, 2, 12, 3}
            //and the differencek = 2
            //int[] arr = { 1, 3, 5, 9, 2, 12, 3 };
            //int k = 2;
            //int result = CCI.differenceOfPair.solution(arr, k);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //Example: Given a smaller strings and a bigger string b, 
            //design an algorithm to find all permutations
            //of the shorter string within the longer one.
            //Print the location of each permutation.
            //string s = "abbc";
            //string b = "cbabadcbbabbcbabaabccbabc";
            //CCI.FindAllPermutation.Solution(b,s);

            //Implement an algorithm to determine if a string has all unique characters.What if you
            //cannot use additional data structures ?            
            //string[] words = { "abcde", "hello", "apple", "kite", "padle" };
            //foreach (var word in words)
            //{
            //    Console.WriteLine(word + ": " + CCI.IsAllUniqueInString.solution(word));
            //}
            //Console.ReadLine();

            //Given two strings, write a method to decide if one is a permutation of the
            //other.
            //string[][] pairs =
            //{
            //    new string[]{"apple", "papel"},
            //    new string[]{"carrot", "tarroc"},
            //    new string[]{"hello", "llloh"}
            //};

            //foreach (var pair in pairs)
            //{
            //    var word1 = pair[0];
            //    var word2 = pair[1];
            //    var result1 = CCI.IsPermutation.solution(word1, word2);
            //    //var result2 = IsPermutation2(word1, word2);
            //    Console.WriteLine("{0}, {1}: {2} ", word1, word2, result1);
            //}
            //Console.ReadLine();

            //URLify: Write a method to replace all spaces in a string with '%20'.You may assume that the string
            //has sufficient space at the end to hold the additional characters, and that you are given the "true"
            //length of the string. (Note: If implementing in Java, please use a character array so that you can
            //perform this operation in place.)
            //string s = "Mr John Smith   ";
            //s = s.Trim();
            //s=s.Replace(" ", "%20");
            //Console.WriteLine(s);
            //Console.ReadLine();

            //Palindrome Permutation: Given a string, write a function to check if it is a permutation of a palindrome.
            //A palindrome is a word or phrase that is the same forwards and backwards.A permutation
            //is a rearrangement of letters. The palindrome does not need to be limited to just dictionary words.
            //1.5
            //1.6
            //EXAMPLE
            //Input: Tact Coa
            //Output: True(permutations: "taco cat", "atco eta", etc.)
            //string s = "Tact Coa";
            //Console.WriteLine(CCI.IsPermutationOfPalindrome.solution(s));
            //Console.ReadLine();

            //One Away: There are three types of edits that can be performed on strings: insert a character,
            //remove a character, or replace a character. Given two strings, write a function to check if they are
            //one edit(or zero edits) away.
            //string[] a = { "pale", "pales", "pale", "pale" };
            //string[] b = { "ple", "pale", "bale", "bake" };
            //for (int i = 0; i < a.Length; i++)
            //{
            //    Console.WriteLine(CCI.OneAway.solution(a[i], b[i]));                
            //}
            //Console.ReadLine();

            //int[] coins = { 9, 6, 5, 1 };
            ////{ 25, 10, 5 };
            //int V = 11; //30;
            //int res =GeekForGeeks.MinimumCoins.solution(coins, V);
            //Console.WriteLine(res);
            //Console.ReadLine();


        }

        static void CodilityProblem()
        {
            Codility.Other.CheckDominoString.solution();

            //Codility Lesson 1-Binary Gap problem
            //int n = Convert.ToInt32(Console.ReadLine());
            //int res = Codility.BinaryGap.solution(n);
            //Console.WriteLine(res);
            //Console.ReadLine();

            //Codility Lesson 2- Cyclic Rotation problem
            //int[] A = { 3, 8, 9, 7, 6 };
            //int[] res = Codility.CyclicRotation.solution(A, 3);
            //foreach (int num in res) Console.WriteLine(num + " ");
            //Console.ReadLine();


            // Codility Lesson 2-OddOccurrenceInArray
            //int[] A = { 42 }; // { 9, 3, 9, 3, 9, 7, 9 }; { 2, 2, 3, 3, 4 }; 
            //int val = Codility.OddOccurrenceInArray.solution(A);
            //Console.WriteLine(val);
            //Console.ReadLine();

            //Codility Lesson 3- FrogJmp problem 
            //Console.WriteLine(Codility.FrogJmp.solution(10, 85, 30));
            //Console.ReadLine();

            //Codility Lesson 3-PermMissingElem problem
            //int[] A = { 2, 3 };// { 1,3,4,5 };
            //int result = Codility.PermMissingElem.solution(A, A.Length);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //Codility Lesson 3-TapEqui problem 
            //int[] A = { -1000, 1000 }; //{ -10, -5, -3, -4, -5 };//{ 3, 1, 2, 4, 3 };
            //Console.WriteLine(Codility.TapeEquilibrium.solution(A));
            //Console.ReadLine();

            //Codility Lesson 4-FrogRiverOne problem
            //int[] A = { 6, 1, 3, 1, 4, 2, 3, 5, 4 }; //{ 2, 2, 2, 2 };  //{ 1, 1, 1, 1 }; //// //
            //int res = Codility.FrogRiverOne.solution(5, A);
            //Console.WriteLine(res);
            //Console.ReadLine();

            //Codility Lesson 4-PermCheck problem 
            //int[] A = { 4, 1, 3, 2 }; //{ 9, 5, 7, 3, 2, 7, 3, 1, 10, 8 };   { 2 };
            //int res = Codility.PermCheck.solution(A);
            //Console.WriteLine(res);
            //Console.ReadLine();

            //Codility Lesson 4-MissingInteger problem
            //int[] A = { 1, 3, 6, 4, 1, 2 };//{ -1, -3 }; //{ 1, 2, 3 };
            //Console.WriteLine(Codility.MissingInteger.solution(A));
            //Console.ReadLine();

            //CodiLity Lesson 4-Max counters problem
            //int[] A = { 3, 4, 4, 6, 1, 4, 4 };
            //int[] res = Codility.MaxCounters.solution(5, A);

            //Codility Lesson 5-passing cars problem
            //int[] A = { 0, 1, 0, 1, 1 };
            //int res =Codility.PassingCars.solution(A);
            //Console.WriteLine(res);
            //Console.ReadLine();

            //Codility Lesson 5-GenomicRangeQuery problem
            //string S = "CAGCCTA";
            //int[] P = { 2, 5, 0 };
            //int[] Q = { 4, 5, 6 };
            //int[] res = Codility.GenomicRangeQuery.solution(S, P, Q);
            //foreach(int num in res) Console.Write(num);
            //Console.ReadLine();

            //Codility Lesson 5-MinAvgTwoSlice problem
            //int[] A = { -3, -5, -8, -4, -10 };
            //    //{ 4, 2, 2, 5, 1, 5, 8 };
            //int res = Codility.MinAvgTwoSlice.solution(A);
            //Console.WriteLine(res);
            //Console.ReadLine();

            //Codility Lesson 5-CountDiv problem
            //int res = Codility.CountDiv.solution(6, 11, 2);
            //Console.WriteLine(res);
            //Console.ReadLine();

            //Codility Lesson 6-Distinct problem
            //int[] A = { 2,1,1,2,3,1 };
            //int res = Codility.Distinct.solution(A);
            //Console.WriteLine(res);
            //Console.ReadLine();

            //Codility Lesson 6-MaxProductOfThree problem
            //int[] A = { -5,5,-5,4}; //{ -3,1,2,-2,5,6};
            //int res = Codility.MaxProductOfThree.solution(A);
            //Console.WriteLine(res);
            //Console.ReadLine();

            //Codility Lesson 6-Triangle problem
            //int[] A = { 10,2,5,1,8,20 }; //{ -3,1,2,-2,5,6};
            //int res = Codility.Triangle.solution(A);
            //Console.WriteLine(res);
            //Console.ReadLine();

            //Codility Lesson 6-NumberOfDiscIntersections problem
            //int[] A = { 10, 2, 5, 1, 8, 20 }; //{ -3,1,2,-2,5,6};
            //int res = Codility.NumberOfDiscIntersections.solution(A);
            //Console.WriteLine(res);
            //Console.ReadLine();

            //Codility Lesson 8-Dominator problem
            //int[] A = { 1, 2, 1,1,2 };// { 1, 0, 0, 1, 1, 1 }; //{ 3, 4, 3, 2, 3, -1, 3, 3 };
            //int res = Codility.Dominator.solution(A);
            //Console.WriteLine(res);
            //Console.ReadLine();

            //Codility Lesson 9-MaxSliceSum problem
            //int[] A = { 3,2,-6,4,0 };
            //int res = Codility.MaxSliceSum.solution(A);
            //Console.WriteLine(res);
            //Console.ReadLine();

            //Codility toptal test problem
            //string[] T = new string[] { "test1a", "test2", "test1b", "test1c", "test3" };
            //string[] R = new string[] { "Wrong answer", "OK", "Runtime error", "OK", "Time limit exceeded" };
            //int res = Codility.ToptalTestProblem.solution(T, R);
            //Console.WriteLine(res);
            //T = new string[] { "codility1", "codility3", "codility2", "codility4b", "codility4a" };
            //R = new string[] { "Wrong answer", "OK", "OK", "Runtime error", "OK" };
            //res = Codility.ToptalTestProblem.solution(T, R);
            //Console.WriteLine(res);
            //Console.ReadLine();

            //Codility Lesson 9-MaxProfit problem
            //int[] A = {23171,21011,21123,21366,21013,21367};
            //int res = Codility.MaxProfit.solution(A);
            //Console.WriteLine(res);
            //Console.ReadLine();

            //Remaining 5- GeonomicRangeQuey, 6- numberofdiscIntersections, 7- all, 8-EquiLeader

            //int[] A = { 4, 4, 3, 3, 1, 0 };            //{ 4,2,0};            //{ 3, 4, 3, 0, 2, 2, 3, 0, 0 };
            //int res = Codility.RankSoldier.solution(A);
            //Console.WriteLine(res);
            //Console.ReadLine();

            //string s = "abbabba"; //"codility"; //
            //int res = Codility.PrefixSuffixCount.solution(s);
            //Console.WriteLine(res);
            //Console.ReadLine();

            //A vending machine has the following denominations: 1c, 5c, 10c, 25c, 50c, and $1.
            //Your task is to write a program that will be used in a vending machine to return change.
            // Assume that the vending machine will always want to return the least number of coins or notes.
            // Devise a function getChange(M, P) where M is how much money was inserted
            //into the machine and P the price of the item selected,
            //that returns an array of integers representing the number of each denomination to return.
            //Example:
            //getChange(5, 0.99) // should return [1,0,0,0,0,4]
            //decimal val1 = Convert.ToDecimal(Console.ReadLine());
            //decimal val2 = Convert.ToDecimal(Console.ReadLine());
            //decimal[] res = Codility.LeastVendingMachineDenomination.getChange(val1, val2);
            //foreach (decimal val in res)
            //{
            //    Console.Write(val + " ");
            //}
            //Console.ReadLine();

        }

        static void CheckForDouble()
        {
            decimal MyFortune = 456.452m;
            decimal val= Decimal.Subtract(MyFortune, Decimal.Floor(MyFortune));

            double a = 164.7194;
            double b = Math.Truncate(a);

            double d = 456.45;
            double x = d - Math.Truncate(d);
            if (x < 0.5) x = -x;
            Console.WriteLine(x);

            decimal value = 456.45m;
            value = value % 1;
            value = value % 1;
            //  if (result < 0.5M)

            //for (int i = 1; i <= 10; i++)
            //{
            //    double MoneyEntered = double.Parse(Console.ReadLine());
            //    double cost = double.Parse(Console.ReadLine());
            //    int[] rem = vendingMachine(MoneyEntered, cost);
            //    Console.WriteLine(string.Join(" ", rem));               
            //}

            double myMoney = 187.543;
            double yourMoney = 10.25;
            double result = myMoney - yourMoney; //177.293
            double fourtime = 10.25 * 4.0; //41
            double decreasedMoney = yourMoney - 1.25; //9
            double dividedMoney = decreasedMoney / 2; //4.5
            double addToMyMoney = dividedMoney + 100.34; //104.84

            double someDouble = 7.05;
            int someInt= 2;
            var res = someDouble * someInt; //15
            res = someDouble + someInt; //9.5
            res = someDouble / someInt; //3.75
            res = someDouble - someInt; //5.5

            string someString = "Hello ";
            double result1 = someDouble + someInt; // Ok - adding double and int
            string result2 = someString + someDouble; // Ok - append double to string
            string result3 = someString + someInt; // Ok - append int to string
            //string result4 = someString * someInt; // NOT ok - can't multiply int and string
            //string result5 = someString / someDouble; // NOT ok - can't divide string on double
            //string result6 = someString * someString; // NOT ok - can't multiply strings

            double temperature = 17.5;
            Console.WriteLine(temperature); // Outputs: 17.5
            Console.WriteLine("Temp is " + temperature); // Outputs: Temp is 17.5
            Console.WriteLine($"Temp is {temperature}"); // Outputs: Temp is 17.5   

            //Create a double variable and assign its value as 3.14. Output to the console "Pi is close to 3.14" using your variable.
            double pi = 3.14;
            Console.WriteLine($"pi is close to  {pi} ");

            string numString = Console.ReadLine();
            double someVal = double.Parse(numString);

            // Write a program that reads two doubles from the console, each from a new line, and outputs their sum and difference. For example:
            // > 7.1
            // > 3.5
            // 10.6
            // 3.6
            double num1 = double.Parse(Console.ReadLine());
            double num2;
            double.TryParse(Console.ReadLine(), out num2);
            Console.WriteLine($"sum is {num1 + num2}");
            Console.WriteLine($"diff is {num1 - num2}");

            //            Write a program that reads two doubles from the console, each from a new line, and outputs their product and quotient. If the second number is 0.0, you should output their product and then "Can't divide by zero!" instead of the quotient. For example:
            //> 4.4
            //> 2.2
            //9.68
            //2
            //Or, if the second number is zero:
            //> 4.4
            //> 0
            //0
            //Can't divide by zero!
            double.TryParse(Console.ReadLine(), out num1);
            double.TryParse(Console.ReadLine(), out num2);
            Console.WriteLine(num1 * num2);
            if (num2 == 0) Console.WriteLine("Can't divide by zero!");
            else Console.WriteLine(num1 / num2);

            // Put in a shift at the factory that makes sewer covers. 
            //Your main job will be to manually calculate the area 
            //of the round sewer cover based on its radius. 
            //You can automate this by writing a program that takes 
            //one double number as an input - the radius of the sewer cover 
            //- and outputs one number - its area. Use 3.14 for Pi in your program.
            double.TryParse(Console.ReadLine(), out num1);
             pi = 3.14159;
            double area = pi * num1 * num1;
            Console.WriteLine(area);

            //Write a program that reads an integer count and a double number 
            //from the console.Then print the double number count times 
            //to the console, adding 1 to the number each time it prints.For example:
            //> 3
            //> 7.926
            //7.926
            //8.926
            //9.926
            int.TryParse(Console.ReadLine(), out someInt);
            double.TryParse(Console.ReadLine(), out num1);
            for (int i = 1; i <= someInt; i++)
            {
                Console.WriteLine(num1);
                num1 += 1;
            }

            someDouble = 1.5;
            float someFloat = 2.5f;

            double sum = someDouble + someFloat;
            Console.WriteLine(sum);                 // Outputs 4

            double subtract = someDouble - someFloat;
            Console.WriteLine(subtract);            // Outputs -1

            double multiply = someDouble * someFloat;
            Console.WriteLine(multiply);            // Outputs 3.75

            double divide = someDouble / someFloat;
            Console.WriteLine(divide);              // Outputs 0.6

            //Write a program that reads 5 double numbers from the console input,
            //each from a new line, and then writes only those that are less than zero
            //to the console output. For example:
            //> -3.25
            //> 7.926
            //> 14.131
            //> -2.768
            //> 999.999
            //- 3.25
            //- 2.768
            double.TryParse(Console.ReadLine(),out num1);
            double.TryParse(Console.ReadLine(), out num2);
            double.TryParse(Console.ReadLine(), out double num3);
            double.TryParse(Console.ReadLine(), out double num4);
            double.TryParse(Console.ReadLine(), out double num5);
            if (num1 < 0) Console.WriteLine(num1);
            if (num2 < 0) Console.WriteLine(num2);
            if (num3 < 0) Console.WriteLine(num3);
            if (num4 < 0) Console.WriteLine(num4);
            if (num5 < 0) Console.WriteLine(num5);
        }

               

        

        static void HackerRankProblem()
        {
            //HackerRank.Problem_Solving.SinglyLinkedList llist = new HackerRank.Problem_Solving.SinglyLinkedList();

            //int llistCount = Convert.ToInt32(Console.ReadLine());

            //for (int i = 0; i < llistCount; i++)
            //{
            //    int llistItem = Convert.ToInt32(Console.ReadLine());
            //    HackerRank.Problem_Solving.SinglyLinkedListNode llist_head = HackerRank.Problem_Solving.SinglyLinkedList.insertNodeAtTail(llist.head, llistItem);
            //    llist.head = llist_head;
            //}

            //const int x = 100;
            //int y = 10;
            //int z = y / 5 * 100 * y;
            //Console.WriteLine(x * z);
            //Console.ReadLine();

            //int q = Convert.ToInt32(Console.ReadLine());
            //for (int qItr = 0; qItr < q; qItr++)
            //{
            //    string s = Console.ReadLine();
            //    int result = HackerRank.Problem_Solving.sherlockAndAnagrams.solution(s);
            //    Console.WriteLine(result);
            //}
            //Console.ReadLine();

            //string s = Console.ReadLine();
            //string result = HackerRank.Problem_Solving.superReducedString.solution(s);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //int l = Convert.ToInt32(Console.ReadLine().Trim());
            //string s = Console.ReadLine();
            //int result = HackerRank.Problem_Solving.alternate.solution(s);
            //Console.WriteLine(result);
            //Console.ReadLine();


            //int n = Convert.ToInt32(Console.ReadLine());
            //string password = Console.ReadLine();
            //int answer = HackerRank.Problem_Solving.minimumNumber.solution(n, password);
            //Console.WriteLine(answer);
            //Console.ReadLine();

            //string s = Console.ReadLine();
            //int result = HackerRank.Problem_Solving.camelcase.solution(s);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //int n = Convert.ToInt32(Console.ReadLine());
            //int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp));
            //HackerRank.Problem_Solving.insertionSort1.solution(n, arr);
            //Console.ReadLine();

            //int V = Convert.ToInt32(Console.ReadLine());
            //int n = Convert.ToInt32(Console.ReadLine());
            //int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp));
            //int result = HackerRank.Problem_Solving.introTutorial.solution(V, arr);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //int n = Convert.ToInt32(Console.ReadLine());
            //string[] unsorted = new string[n];
            //for (int i = 0; i < n; i++)
            //{
            //    string unsortedItem = Console.ReadLine();
            //    unsorted[i] = unsortedItem;
            //}

            //string[] result = HackerRank.Problem_Solving.bigSorting.solution(unsorted);
            //Console.WriteLine(string.Join("\n", result));



            //int x = 10;
            //x = x++;
            //Console.WriteLine(x);

            //x = 10;
            //x = ++x;
            //Console.WriteLine(x);

            //x = 10;
            //x++;
            //Console.WriteLine(x);

            //int q = Convert.ToInt32(Console.ReadLine());
            //for (int qItr = 0; qItr < q; qItr++)
            //{
            //    string s = Console.ReadLine();
            //    HackerRank.Problem_Solving.separateNumbers.solution(s);
            //}
            //Console.ReadLine();

            //string s = Console.ReadLine();
            //int queriesCount = Convert.ToInt32(Console.ReadLine());
            //int[] queries = new int[queriesCount];
            //for (int i = 0; i < queriesCount; i++)
            //{
            //    int queriesItem = Convert.ToInt32(Console.ReadLine());
            //    queries[i] = queriesItem;
            //}
            //string[] result = HackerRank.Problem_Solving.weightedUniformStrings.solution(s, queries);
            //Console.WriteLine(string.Join("\n", result));
            //Console.ReadLine();

            //int q = Convert.ToInt32(Console.ReadLine());

            //for (int qItr = 0; qItr < q; qItr++)
            //{
            //    string s = Console.ReadLine();
            //    string result = HackerRank.Problem_Solving.hackerrankInString.solution(s);
            //    Console.WriteLine(result);
            //}
            //Console.ReadLine();

            //string s = Console.ReadLine();
            //int result = HackerRank.Problem_Solving.marsExploration.solution(s);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //string s = Console.ReadLine();
            //string result = HackerRank.Problem_Solving.pangrams.solution(s);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //int q = Convert.ToInt32(Console.ReadLine());
            //for (int qItr = 0; qItr < q; qItr++)
            //{
            //    string s = Console.ReadLine();
            //    string result = HackerRank.Problem_Solving.funnyString.solution(s);
            //    Console.WriteLine(result);
            //}
            //Console.ReadLine();

            //int n = Convert.ToInt32(Console.ReadLine());
            //string b = Console.ReadLine();
            //int result = HackerRank.Problem_Solving.beautifulBinaryString.solution(b);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //int q = Convert.ToInt32(Console.ReadLine());
            //for (int qItr = 0; qItr < q; qItr++)
            //{
            //    string s = Console.ReadLine();
            //    int result = HackerRank.Problem_Solving.alternatingCharacters.solution(s);
            //    Console.WriteLine(result);
            //}
            //Console.ReadLine();


            //int n = Convert.ToInt32(Console.ReadLine());
            //string[] arr = new string[n];
            //for (int i = 0; i < n; i++)
            //{
            //    string arrItem = Console.ReadLine();
            //    arr[i] = arrItem;
            //}
            //int result = HackerRank.Problem_Solving.gemstones.solution(arr);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //int q = Convert.ToInt32(Console.ReadLine());
            //for (int qItr = 0; qItr < q; qItr++)
            //{
            //    string s = Console.ReadLine();
            //    int result = HackerRank.Problem_Solving.theLoveLetterMystery.solution(s);
            //    Console.WriteLine(result);
            //}
            //Console.ReadLine();

            //int q = Convert.ToInt32(Console.ReadLine());
            //for (int qItr = 0; qItr < q; qItr++)
            //{
            //    string s = Console.ReadLine();
            //    int result = HackerRank.Problem_Solving.palindromeIndex.solution(s);
            //    Console.WriteLine(result);
            //}
            //Console.ReadLine();

            //SherlockAndValidString problem
            //string s = Console.ReadLine();
            //string result = HackerRank.Problem_Solving.SherlockAndValidString.solution(s);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //stringConstruction problem
            //int q = Convert.ToInt32(Console.ReadLine());
            //for (int qItr = 0; qItr < q; qItr++)
            //{
            //    string s = Console.ReadLine();
            //    int result = HackerRank.Problem_Solving.stringConstruction.solution(s);
            //    Console.WriteLine(result);
            //}

            //Anagram problem
            //int q = Convert.ToInt32(Console.ReadLine());
            //for (int qItr = 0; qItr < q; qItr++)
            //{
            //    string s = Console.ReadLine();
            //    int result = HackerRank.Problem_Solving.Anagram.solution(s);
            //    Console.WriteLine(result);
            //}

            //makingAnagrams problem
            //string s1 = Console.ReadLine();
            //string s2 = Console.ReadLine();
            //int result = HackerRank.Problem_Solving.makingAnagrams.solution(s1, s2);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //twoString problem
            //int q = Convert.ToInt32(Console.ReadLine());
            //for (int qItr = 0; qItr < q; qItr++)
            //{
            //    string s1 = Console.ReadLine();
            //    string s2 = Console.ReadLine();
            //    string result = HackerRank.Problem_Solving.twoStrings.solution(s1, s2);
            //    Console.WriteLine(result);
            //}
            //Console.ReadLine();

            //twoArrays problem
            //int q = Convert.ToInt32(Console.ReadLine());
            //for (int qItr = 0; qItr < q; qItr++)
            //{
            //    string[] nk = Console.ReadLine().Split(' ');
            //    int n = Convert.ToInt32(nk[0]);
            //    int k = Convert.ToInt32(nk[1]);
            //    int[] A = Array.ConvertAll(Console.ReadLine().Split(' '), ATemp => Convert.ToInt32(ATemp));
            //    int[] B = Array.ConvertAll(Console.ReadLine().Split(' '), BTemp => Convert.ToInt32(BTemp));
            //    string result = HackerRank.Problem_Solving.twoArrays.solution(k, A, B);
            //    Console.WriteLine(result);
            //}
            //Console.ReadLine();

            //gameOfThrones palindrom string problem
            //string s = Console.ReadLine();
            //string result = HackerRank.Problem_Solving.gameOfThrones.solution(s);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //jimOrders problem
            //int n = Convert.ToInt32(Console.ReadLine());
            //int[][] orders = new int[n][];
            //for (int i = 0; i < n; i++)
            //{
            //    orders[i] = Array.ConvertAll(Console.ReadLine().Split(' '), ordersTemp => Convert.ToInt32(ordersTemp));
            //}
            //int[] result = HackerRank.Problem_Solving.jimOrders.solution(orders);
            //Console.WriteLine(string.Join(" ", result));
            //Console.ReadLine();

            //maxMin problem
            //int n = Convert.ToInt32(Console.ReadLine());
            //int k = Convert.ToInt32(Console.ReadLine());
            //int[] arr = new int[n];
            //for (int i = 0; i < n; i++)
            //{
            //    int arrItem = Convert.ToInt32(Console.ReadLine());
            //    arr[i] = arrItem;
            //}
            //int result = HackerRank.Problem_Solving.maxMin.solution(k, arr);
            //Console.WriteLine(result);

            //getMinimumCost problem
            //string[] nk = Console.ReadLine().Split(' ');
            //int n = Convert.ToInt32(nk[0]);
            //int k = Convert.ToInt32(nk[1]);
            //int[] c = Array.ConvertAll(Console.ReadLine().Split(' '), cTemp => Convert.ToInt32(cTemp));
            //int minimumCost = HackerRank.Problem_Solving.getMinimumCost.solution(k, c);
            //Console.WriteLine(minimumCost);
            //Console.ReadLine();

            //maximumToys problem
            //string[] nk = Console.ReadLine().Split(' ');
            //int n = Convert.ToInt32(nk[0]);
            //int k = Convert.ToInt32(nk[1]);
            //int[] prices = Array.ConvertAll(Console.ReadLine().Split(' '), pricesTemp => Convert.ToInt32(pricesTemp));
            //int result = HackerRank.Problem_Solving.maximumToys.solution(prices, k);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //largestPermutation problem
            //string[] nk = Console.ReadLine().Split(' ');
            //int n = Convert.ToInt32(nk[0]);
            //int k = Convert.ToInt32(nk[1]);
            //int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp));
            //int[] result = HackerRank.Problem_Solving.largestPermutation.solution(k, arr);
            //Console.WriteLine(string.Join(" ", result));
            //Console.ReadLine();

            //acmTeam problem
            //string[] nm = Console.ReadLine().Split(' ');
            //int n = Convert.ToInt32(nm[0]);
            //int m = Convert.ToInt32(nm[1]);
            //string[] topic = new string[n];
            //for (int i = 0; i < n; i++)
            //{
            //    string topicItem = Console.ReadLine();
            //    topic[i] = topicItem;
            //}
            //int[] result = HackerRank.Problem_Solving.acmTeam.solution(topic);
            //Console.WriteLine(string.Join("\n", result));
            //Console.ReadLine();

            ////taumBday problem
            //int t = Convert.ToInt32(Console.ReadLine());
            //for (int tItr = 0; tItr < t; tItr++)
            //{
            //    string[] bw = Console.ReadLine().Split(' ');
            //    int b = Convert.ToInt32(bw[0]);
            //    int w = Convert.ToInt32(bw[1]);
            //    string[] bcWcz = Console.ReadLine().Split(' ');
            //    int bc = Convert.ToInt32(bcWcz[0]);
            //    int wc = Convert.ToInt32(bcWcz[1]);
            //    int z = Convert.ToInt32(bcWcz[2]);
            //    long result = HackerRank.Problem_Solving.taumBday.solution(b, w, bc, wc, z);
            //    Console.WriteLine(result);                
            //}
            //Console.ReadLine();

            //repeatedString problem
            //string s = Console.ReadLine();
            //long n = Convert.ToInt64(Console.ReadLine());
            //long result = HackerRank.Problem_Solving.repeatedString.solution(s, n);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //Priyanka and Toys problem
            //int n = Convert.ToInt32(Console.ReadLine());
            //int[] w = Array.ConvertAll(Console.ReadLine().Split(' '), wTemp => Convert.ToInt32(wTemp));
            //int result = HackerRank.Problem_Solving.PriyankaAndToys.solution(w);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //Equalizer Array problem
            //int n = Convert.ToInt32(Console.ReadLine());
            //int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp));
            //int result = HackerRank.Problem_Solving.equalizeArray.solution(arr);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //jumpingOnClouds1 problem
            //int n = Convert.ToInt32(Console.ReadLine());
            //int[] c = Array.ConvertAll(Console.ReadLine().Split(' '), cTemp => Convert.ToInt32(cTemp));
            //int result = HackerRank.Problem_Solving.jumpingOnClouds1.solution(c);
            //Console.WriteLine(result);

            //Sherlock and Beast problem
            //int t = Convert.ToInt32(Console.ReadLine().Trim());
            //for (int tItr = 0; tItr < t; tItr++)
            //{
            //    int n = Convert.ToInt32(Console.ReadLine().Trim());
            //    HackerRank.Problem_Solving.decentNumber.solution(n);
            //}

            //angryProfessor problem
            //int t = Convert.ToInt32(Console.ReadLine());
            //for (int tItr = 0; tItr < t; tItr++)
            //{
            //    string[] nk = Console.ReadLine().Split(' ');
            //    int n = Convert.ToInt32(nk[0]);
            //    int k = Convert.ToInt32(nk[1]);
            //    int[] a = Array.ConvertAll(Console.ReadLine().Split(' '), aTemp => Convert.ToInt32(aTemp));
            //    string result = HackerRank.Problem_Solving.angryProfessor.solution(k, a);
            //    Console.WriteLine(result);
            //}

            //appendAndDelete problem
            //string s = Console.ReadLine();
            //string t = Console.ReadLine();
            //int k = Convert.ToInt32(Console.ReadLine());
            //string result = HackerRank.Problem_Solving.appendAndDelete.solution(s, t, k);
            //Console.WriteLine(result);

            //Lisa's workbook problem
            //string[] nk = Console.ReadLine().Split(' ');
            //int n = Convert.ToInt32(nk[0]);
            //int k = Convert.ToInt32(nk[1]);
            //int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp));
            //int result = HackerRank.Problem_Solving.LisasWorkbook.solution(n, k, arr);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //caesarCipher problem
            //int n = Convert.ToInt32(Console.ReadLine());
            //string s = Console.ReadLine();
            //int k = Convert.ToInt32(Console.ReadLine());
            //string result = HackerRank.Problem_Solving.caesarCipher.solution(s, k);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //gettotalX problem
            //string[] nm = Console.ReadLine().Split(' ');
            //int n = Convert.ToInt32(nm[0]);
            //int m = Convert.ToInt32(nm[1]);
            //int[] a = Array.ConvertAll(Console.ReadLine().Split(' '), aTemp => Convert.ToInt32(aTemp));
            //int[] b = Array.ConvertAll(Console.ReadLine().Split(' '), bTemp => Convert.ToInt32(bTemp));
            //int total = HackerRank.Problem_Solving.getTotalX.solution(a, b);
            //Console.WriteLine(total);

            //divisibleSumPairs problem
            //string[] nk = Console.ReadLine().Split(' ');
            //int n = Convert.ToInt32(nk[0]);
            //int k = Convert.ToInt32(nk[1]);
            //int[] ar = Array.ConvertAll(Console.ReadLine().Split(' '), arTemp => Convert.ToInt32(arTemp));
            //int result = HackerRank.Problem_Solving.divisibleSumPairs.solution(n, k, ar);
            //Console.WriteLine(result);


            //serviceLane problem
            //string[] nt = Console.ReadLine().Split(' ');
            //int n = Convert.ToInt32(nt[0]);
            //int t = Convert.ToInt32(nt[1]);

            //int[] width = Array.ConvertAll(Console.ReadLine().Split(' '), widthTemp => Convert.ToInt32(widthTemp));
            //int[][] cases = new int[t][];
            //for (int i = 0; i < t; i++)
            //{
            //    cases[i] = Array.ConvertAll(Console.ReadLine().Split(' '), casesTemp => Convert.ToInt32(casesTemp));
            //}
            //int[] result = HackerRank.Problem_Solving.serviceLane.solution(n,width, cases);
            //Console.WriteLine(string.Join("\n", result));
            //Console.ReadLine();

            //SherlockAndSquares problem
            //int q = Convert.ToInt32(Console.ReadLine());

            //for (int qItr = 0; qItr < q; qItr++)
            //{
            //    string[] ab = Console.ReadLine().Split(' ');
            //    int a = Convert.ToInt32(ab[0]);
            //    int b = Convert.ToInt32(ab[1]);
            //    int result = HackerRank.Problem_Solving.SherlockAndSquares.solution(a, b);
            //    Console.WriteLine(result);
            //    Console.ReadLine();
            //}

            //Find Digit problem
            //int t = Convert.ToInt32(Console.ReadLine());
            //for (int tItr = 0; tItr < t; tItr++)
            //{
            //    int n = Convert.ToInt32(Console.ReadLine());
            //    int result = HackerRank.Problem_Solving.FindDigits.solution(n);
            //    Console.WriteLine(result);
            //}
            //Console.ReadLine();

            ////Sequence Equation problem
            //int n = Convert.ToInt32(Console.ReadLine());
            //int[] p = Array.ConvertAll(Console.ReadLine().Split(' '), pTemp => Convert.ToInt32(pTemp));
            //int[] result = HackerRank.Problem_Solving.permutationEquation.solution(p);
            //Console.WriteLine(string.Join("\n", result));
            //Console.ReadLine();

            //jumpingOnClouds problem
            //string[] nk = Console.ReadLine().Split(' ');
            //int n = Convert.ToInt32(nk[0]);
            //int k = Convert.ToInt32(nk[1]);
            //int[] c = Array.ConvertAll(Console.ReadLine().Split(' '), cTemp => Convert.ToInt32(cTemp));
            //int result = HackerRank.Problem_Solving.jumpingOnClouds.solution(c, k);
            //Console.WriteLine(result);
            //Console.ReadLine();

            ////Problem 1 Find sum of all elements of array
            //int[] arr = new int[] { 1, 3, 5 };
            //Console.WriteLine(FindSumOfArray(arr));

            ////Problem 2 Find score difference of Bob and Alice as array
            //List<int> a = new List<int>() { 5, 6, 7 };
            //List<int> b = new List<int>() { 3, 6, 10 };
            //List<int> res =   FindScoreDiff(a, b);
            //Console.WriteLine(string.Join(" ", res));

            ////Problem 3 Find sum of long numbers
            //long[] ar = new long[] {1354646, 465757673, 57686786867 };
            //Console.WriteLine(FindSumOfBigNumbers(ar));

            //Problem 4-Find a diagnoal difference
            //int n = Convert.ToInt32(Console.ReadLine());

            //int[][] arr1 = new int[n][];

            //for (int i = 0; i < n; i++)
            //{
            //    arr1[i] = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp));
            //}

            //int result = diagonalDifference(arr1);

            //Console.WriteLine(result);

            //Problem 5 -FindPlusMinus
            //int n = Convert.ToInt32(Console.ReadLine());

            //int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp));

            //plusMinus(arr);

            //Console.ReadLine();

            //Problem 6 -Stair case
            //int n = Convert.ToInt32(Console.ReadLine());

            //staircase(n);
            //Console.ReadLine();

            //Problem 7 - Min Mix sum
            //int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp));
            //miniMaxSum(arr);

            //Problem 8 - Birthday cake Candles            
            //int arCount = Convert.ToInt32(Console.ReadLine());
            //int[] ar = Array.ConvertAll(Console.ReadLine().Split(' '), arTemp => Convert.ToInt32(arTemp));
            //int result = birthdayCakeCandles(ar);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //Problem 9 -Time Conversion
            //string s = Console.ReadLine();
            //string result = timeConversion(s);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //Problem 10-Build a string
            //int t = Convert.ToInt32(Console.ReadLine());
            //for (int tItr = 0; tItr < t; tItr++)            {
            //    string[] nab = Console.ReadLine().Split(' ');
            //    int n = Convert.ToInt32(nab[0]);
            //    int a = Convert.ToInt32(nab[1]);
            //    int b = Convert.ToInt32(nab[2]);
            //    string s = Console.ReadLine();
            //    int result = buildString(a, b, s);
            //    Console.ReadLine();
            //} 

            //Problem-10 Grading Students
            //int n = Convert.ToInt32(Console.ReadLine());

            //int[] grades = new int[n];

            //for (int gradesItr = 0; gradesItr < n; gradesItr++)
            //{
            //    int gradesItem = Convert.ToInt32(Console.ReadLine());
            //    grades[gradesItr] = gradesItem;
            //}

            //int[] result = gradingStudents(grades);

            //Console.WriteLine(string.Join("\n", result));
            //Console.ReadLine();

            //Day 3 problem
            //double meal_cost = Convert.ToDouble(Console.ReadLine());
            //int tip_percent = Convert.ToInt32(Console.ReadLine());
            //int tax_percent = Convert.ToInt32(Console.ReadLine());
            //solve(meal_cost, tip_percent, tax_percent);

            //Problem 11-Kangaroo
            //string[] x1V1X2V2 = Console.ReadLine().Split(' ');
            //int x1 = Convert.ToInt32(x1V1X2V2[0]);
            //int v1 = Convert.ToInt32(x1V1X2V2[1]);
            //int x2 = Convert.ToInt32(x1V1X2V2[2]);
            //int v2 = Convert.ToInt32(x1V1X2V2[3]);
            //string result = kangaroo(x1, v1, x2, v2);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //Quiz 6
            //ArrayList al = new ArrayList();
            //al.Add(1);
            //al.Add('1');
            //al.Add("1");
            //foreach(int num in al)
            //{
            //    Console.WriteLine(num);
            //}

            //Quiz 7
            //Console.ReadLine();
            //int y = 3;
            //Write(out y);
            //return 0;

            //Day Problem for Arrays
            //int[] arr = new int[] {1,4,3,2};
            //for (int i = arr.Length - 1; i >= 0; i--)
            //{
            //    Console.Write(arr[i] + " ");
            //}
            //Console.ReadLine();

            //Breaking the Records
            //int n = Convert.ToInt32(Console.ReadLine());
            //int[] scores = Array.ConvertAll(Console.ReadLine().Split(' '), scoresTemp => Convert.ToInt32(scoresTemp))            ;
            //int[] result = breakingRecords(scores);
            //Console.WriteLine(string.Join(" ", result));
            //Console.ReadLine();

            //Checking interfaces with same method name
            //C1 objI1 = new C1();
            //objI1.Method1();
            //C1 objI2 = new C1();
            //objI2.Method1();
            //I1 objI3 = new C1();
            //objI3.Method1();
            //I2 objI4 = new C1();
            //objI4.Method1();
            //Console.ReadLine();

            //Check for Params
            //Add(12, 14, 15);

            //Quiz 10
            //string str = "Hello World?";
            //Console.WriteLine(String.Compare(str, "Hello World?"));
            //Console.ReadLine();

            //Day 8-Dictionary usage
            //Dictionary<string, int> dict = new Dictionary<string, int>();
            //int n = Convert.ToInt32(Console.ReadLine());
            //for (int i = 0; i < n; i++)
            //{                
            //    string[] str = Console.ReadLine().Split(' ');
            //    dict.Add(str[0],Convert.ToInt32(str[1]));
            //}

            //while (true)
            //{
            //    string strQuery = Console.ReadLine();
            //    if (string.IsNullOrEmpty(strQuery)) { break; }
            //    else
            //    {                   
            //        if (dict.ContainsKey(strQuery))
            //        {
            //            Console.WriteLine(dict[strQuery]);
            //        }
            //        else
            //        {
            //            Console.WriteLine("Not found");                        
            //        }                    
            //    }
            //}

            //Birth Day Chocolate Problem
            //int n = Convert.ToInt32(Console.ReadLine().Trim());
            //List<int> s = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(sTemp => Convert.ToInt32(sTemp)).ToList();
            //string[] dm = Console.ReadLine().TrimEnd().Split(' ');
            //int d = Convert.ToInt32(dm[0]);
            //int m = Convert.ToInt32(dm[1]);
            //int result = birthday(s, d, m);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //Day 10 Recursion problem
            //int n = Convert.ToInt32(Console.ReadLine());
            //int result = factorial(n);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //Day 10 problem
            //int n = Convert.ToInt32(Console.ReadLine());
            //StringBuilder sb = new StringBuilder();
            //while (n > 0)
            //{
            //    sb.Append(Convert.ToString(n % 2));
            //    n = n / 2;
            //}
            //int cnt = 0;
            //int max = 0;
            //for (int i = 0; i < sb.Length; i++)
            //{
            //    if (sb[i].ToString() == Convert.ToString(1))
            //    {
            //        cnt += 1;
            //        if (cnt>=max)
            //        {
            //            max = cnt;
            //        }
            //    }
            //    else
            //    {
            //        cnt = 0;
            //    }
            //}
            //Console.WriteLine(max);
            //Console.ReadLine();

            //Migratory Birds problem
            //int arrCount = Convert.ToInt32(Console.ReadLine().Trim());
            //List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();
            //int result = migratoryBirds(arr);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //Day 11-2D Arrays
            //int[][] arr = new int[6][];
            //for (int i = 0; i < 6; i++)
            //{
            //    arr[i] = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp));
            //}
            //int fRow,mRow,lRow,sum,finalResult = 0;           
            //for (int i = 0; i < 4; i++)
            //{
            //    for (int j = 0; j < 4; j++)
            //    {
            //        fRow = arr[i][j] + arr[i][j + 1] + arr[i][j + 2];
            //        mRow = arr[i + 1][j + 1];
            //        lRow = arr[i + 2][j] + arr[i + 2][j + 1] + arr[i + 2][j + 2];
            //        sum = fRow + mRow + lRow;
            //        if (i ==0 && j==0) { finalResult = sum; }
            //        else if (sum>finalResult) { finalResult = sum; }
            //    }
            //}
            //Console.WriteLine(finalResult);
            //Console.ReadLine();

            //Console.WriteLine((int)WeekDays.Monday);
            //Console.WriteLine((int)WeekDays.Tuesday);
            //Console.WriteLine((int)WeekDays.Wednesday);
            //Console.WriteLine((int)WeekDays.Thursday);
            //Console.WriteLine((int)WeekDays.Friday);
            //Console.WriteLine((int)WeekDays.Saturday);
            //Console.WriteLine((int)WeekDays.Sunday);

            //int[] intArr = new int[5] { 2, 4, 1, 3, 5 };
            //Array.Sort(intArr);
            //Array.Reverse(intArr);
            //foreach(int num in intArr)
            //{
            //    Console.WriteLine(num);
            //}
            //Console.ReadLine();

            //int year = Convert.ToInt32(Console.ReadLine().Trim());
            //string result = dayOfProgrammer(year);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //int i, j, k, l = 0;
            //int num = i;
            //num = j;
            //num = k;
            //num = l;

            //int[] A = { 1, 3, 6, 4, 1, 2 };//            { 2 };
            //int result = solution(A);
            //Console.WriteLine(result);
            //Console.ReadLine();



            //BonAppetit problem
            //string[] nk = Console.ReadLine().TrimEnd().Split(' ');
            //int n = Convert.ToInt32(nk[0]);
            //int k = Convert.ToInt32(nk[1]);
            //List<int> bill = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(billTemp => Convert.ToInt32(billTemp)).ToList();
            //int b = Convert.ToInt32(Console.ReadLine().Trim());
            //bonAppetit(bill, k, b);            

            //Day 15 Linked List.
            //Node head = null;
            //int T = Int32.Parse(Console.ReadLine());
            //while (T-- > 0)
            //{
            //    int data = Int32.Parse(Console.ReadLine());
            //    head = insert(head, data);
            //}
            //display(head);
            //Console.ReadLine();

            //Sock Merchant problem
            //int n = Convert.ToInt32(Console.ReadLine());
            //int[] ar = Array.ConvertAll(Console.ReadLine().Split(' '), arTemp => Convert.ToInt32(arTemp))            ;
            //int result = sockMerchant(n, ar);
            //Console.WriteLine(result);

            //Day 16 check for bad string
            //string s="3";
            //int a=0;
            //Console.WriteLine(int.TryParse(s,out a)? s: "Bad String");

            //Drawing book problem
            //int n = Convert.ToInt32(Console.ReadLine());
            //int p = Convert.ToInt32(Console.ReadLine());
            //int result = pageCount(n, p);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //Utopian Tree problem
            //int t = Convert.ToInt32(Console.ReadLine());
            //for (int tItr = 0; tItr < t; tItr++)            {
            //    int n = Convert.ToInt32(Console.ReadLine());
            //    int result = utopianTree(n);
            //   Console.WriteLine(result);
            //}
            //Console.ReadLine();



            //Hacker Rank Counting Valleys problem
            //int n = Convert.ToInt32(Console.ReadLine());
            //string s = Console.ReadLine();
            //int result = countingValleys(n, s);
            //Console.WriteLine(result);
            //Console.ReadLine();





            //Find different number who does not have match in negative numbers
            //int[] arr = new int[] { 15, 6, -9, 4, 15, 9,
            //          -6, -4, 15, 15 };
            ////{ 1, 8, -6,-1, 6, 8,8 };
            //int n = arr.Length;
            //Console.Write(findInteger(arr, n));
            //Console.ReadLine();



            //Day 21-Solve Generic problem on HR
            //int[] A = { 1, 1, 1, 1 };
            //string[] B = { "jigar", "vrushti", "mahi" };
            //PrintArray(A);
            //PrintArray(B);
            //Console.ReadLine();

            //HR Money spent problem
            //string[] bnm = Console.ReadLine().Split(' ');
            //int b = Convert.ToInt32(bnm[0]);
            //int n = Convert.ToInt32(bnm[1]);
            //int m = Convert.ToInt32(bnm[2]);
            //int[] keyboards = Array.ConvertAll(Console.ReadLine().Split(' '), keyboardsTemp => Convert.ToInt32(keyboardsTemp));
            //int[] drives = Array.ConvertAll(Console.ReadLine().Split(' '), drivesTemp => Convert.ToInt32(drivesTemp));
            //int moneySpent = getMoneySpent(keyboards, drives, b);
            //Console.WriteLine(moneySpent);
            //Console.ReadLine();



            //Hacker Rank BST
            //BSTNode root = null;
            //int T = Int32.Parse(Console.ReadLine());
            //while (T-- > 0)
            //{
            //    int data = Int32.Parse(Console.ReadLine());
            //    root = BSTSolution.insert(root, data);
            //}
            //int height = BSTSolution.getHeight(root);
            //Console.WriteLine(height);
            //Console.ReadLine();

            //Hacker Rank Mouse and Cat problem
            //int q = Convert.ToInt32(Console.ReadLine());
            //for (int qItr = 0; qItr < q; qItr++)            {
            //    string[] xyz = Console.ReadLine().Split(' ');
            //    int x = Convert.ToInt32(xyz[0]);
            //    int y = Convert.ToInt32(xyz[1]);
            //    int z = Convert.ToInt32(xyz[2]);
            //    string result = catAndMouse(x, y, z);
            //    Console.WriteLine(result);
            //}
            //Console.ReadLine();




            //Hacker Rank BST traversal with queue
            // BSTNode root = null;
            //int T = Int32.Parse(Console.ReadLine());
            //while (T-- > 0)
            //{
            //    int data = Int32.Parse(Console.ReadLine());
            //    root = BSTSolution.insert(root, data);
            //}
            //BSTSolution.levelOrder(root);
            //Console.ReadLine();



            //Hacker Rank Day 25 Prime number problem
            //int t = Convert.ToInt32(Console.ReadLine());
            //for (int tItr = 0; tItr < t; tItr++)
            //{
            //    int n = Convert.ToInt32(Console.ReadLine());
            //    IsPrime(n);
            //}
            //Console.ReadLine();


            //Hacker Rank Design pdf viewer problem
            //int[] h = Array.ConvertAll(Console.ReadLine().Split(' '), hTemp => Convert.ToInt32(hTemp));
            //string word = Console.ReadLine();
            //int result = designerPdfViewer(h, word);
            //Console.WriteLine(result);

            //Hacker Rank Nested Logic problem
            //Console.WriteLine(NestedLogic());
            //Console.ReadLine();

            //Hacker Rank Picking Numbers
            //int n = Convert.ToInt32(Console.ReadLine().Trim());
            //List<int> a = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(aTemp => Convert.ToInt32(aTemp)).ToList();
            //int result = pickingNumbers(a);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //Hacker Ranke Day 28-RegEx
            //int N = Convert.ToInt32(Console.ReadLine());
            //string[] names = new string[N];
            //for (int NItr = 0; NItr < N; NItr++)
            //{
            //    string[] firstNameEmailID = Console.ReadLine().Split(' ');

            //    string firstName = firstNameEmailID[0];

            //    string emailID = firstNameEmailID[1];

            //    string pattern = @"@gmail.com";
            //    Match result = Regex.Match(emailID, pattern);
            //    if (result.Success)
            //    {
            //        names[NItr] = firstName;
            //    }
            //}
            //Array.Sort(names);
            //foreach (string name in names)
            //{
            //    if (!string.IsNullOrEmpty(name)) Console.WriteLine(name);
            //}



            //HackerRank.Problem_Solving
            //string[] ijk = Console.ReadLine().Split(' ');
            //int i = Convert.ToInt32(ijk[0]);
            //int j = Convert.ToInt32(ijk[1]);
            //int k = Convert.ToInt32(ijk[2]);
            //int result = HackerRank.Problem_Solving.beautifulDays.solution(i, j, k);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //HackerRank Day 29
            //int t = Convert.ToInt32(Console.ReadLine());
            //for (int tItr = 0; tItr < t; tItr++)            {
            //    string[] nk = Console.ReadLine().Split(' ');
            //    int n = Convert.ToInt32(nk[0]);
            //    int k = Convert.ToInt32(nk[1]);
            //    Console.WriteLine(ConsoleApp1.HackerRank._30_Days_of_code.Day29_Bitwise_AND.solution(n, k));
            //}
            //Console.ReadLine();

            //HackerRank ViralAdvertising
            //int n = Convert.ToInt32(Console.ReadLine());
            //int result = HackerRank.Problem_Solving.ViralAdvertising.solution(n);
            //Console.WriteLine(result);
            //Console.ReadLine();

            //HackerRank Prisoner
            //int t = Convert.ToInt32(Console.ReadLine());
            //for (int tItr = 0; tItr < t; tItr++)
            //{
            //    string[] nms = Console.ReadLine().Split(' ');
            //    int n = Convert.ToInt32(nms[0]);
            //    int m = Convert.ToInt32(nms[1]);
            //    int s = Convert.ToInt32(nms[2]);
            //    int result = HackerRank.Problem_Solving.Prisoner.saveThePrisoner(n, m, s);
            //    Console.WriteLine(result);
            //}
            //Console.ReadLine();

            //HackerRank Circular Rotation
            //string[] nkq = Console.ReadLine().Split(' ');
            //int n = Convert.ToInt32(nkq[0]);
            //int k = Convert.ToInt32(nkq[1]);
            //int q = Convert.ToInt32(nkq[2]);
            //int[] a = Array.ConvertAll(Console.ReadLine().Split(' '), aTemp => Convert.ToInt32(aTemp));
            //int[] queries = new int[q];
            //for (int i = 0; i < q; i++)
            //{
            //    int queriesItem = Convert.ToInt32(Console.ReadLine());
            //    queries[i] = queriesItem;
            //}

            //int[] result = ConsoleApp1.HackerRank.Problem_Solving.circularArrayRotation.Solution(a, k, queries);
            //Console.WriteLine(string.Join("\n", result));

            //countApplesAndOranges problem
            //string[] st = Console.ReadLine().Split(' ');
            //int s = Convert.ToInt32(st[0]);
            //int t = Convert.ToInt32(st[1]);
            //string[] ab = Console.ReadLine().Split(' ');
            //int a = Convert.ToInt32(ab[0]);
            //int b = Convert.ToInt32(ab[1]);
            //string[] mn = Console.ReadLine().Split(' ');
            //int m = Convert.ToInt32(mn[0]);
            //int n = Convert.ToInt32(mn[1]);
            //int[] apples = Array.ConvertAll(Console.ReadLine().Split(' '), applesTemp => Convert.ToInt32(applesTemp));
            //int[] oranges = Array.ConvertAll(Console.ReadLine().Split(' '), orangesTemp => Convert.ToInt32(orangesTemp));
            //HackerRank.Problem_Solving.countApplesAndOranges.solution(s, t, a, b, apples, oranges);

            //climbingLeaderboard problem
            //int scoresCount = Convert.ToInt32(Console.ReadLine());
            //int[] scores = Array.ConvertAll(Console.ReadLine().Split(' '), scoresTemp => Convert.ToInt32(scoresTemp));
            //int aliceCount = Convert.ToInt32(Console.ReadLine());
            //int[] alice = Array.ConvertAll(Console.ReadLine().Split(' '), aliceTemp => Convert.ToInt32(aliceTemp));
            //int[] result = HackerRank.Problem_Solving.climbingLeaderboard.solution(scores, alice);
            //Console.WriteLine(string.Join("\n", result));

            //cutTheSticks problem
            //int n = Convert.ToInt32(Console.ReadLine());
            //int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp));
            //int[] result = HackerRank.Problem_Solving.cutTheSticks.solution(arr);
            //Console.WriteLine(string.Join("\n", result));
            //Console.ReadLine();

            //chocolateFeast problem
            //int t = Convert.ToInt32(Console.ReadLine());
            //for (int tItr = 0; tItr < t; tItr++)
            //{
            //    string[] ncm = Console.ReadLine().Split(' ');
            //    int n = Convert.ToInt32(ncm[0]);
            //    int c = Convert.ToInt32(ncm[1]);
            //    int m = Convert.ToInt32(ncm[2]);
            //    int result = HackerRank.Problem_Solving.chocolateFeast.solution(n, c, m);
            //    Console.WriteLine(result);
            //}
          

            //Console.ReadLine();
        }

        public static int pickingNumbers(List<int> a)
        {
            int[] arr = a.ToArray();
            Array.Sort(arr);
            int len = arr.Length;
            int max = 0;
            for (int i = len - 1; i >= 0; i--)
            {
                int count = 1;
                for (int j = i - 1; j >= 0; j--)
                {

                    if ((arr[i] - arr[j]) < 2)
                    {
                        count++;
                    }

                }
                max = count > max ? count : max;
            }

            return max;
        }
    

       

        static int designerPdfViewer(int[] h, string word)
        {
            //foreach (char c in word)
            //{               
            //     foreach (string str in Enum.GetNames(typeof(letter)))
            //    {
            //        char ch = Convert.ToChar(str);
            //        if (ch ==c )
            //    }
            //}
            int maxHeight = 0;
            for (int i = 0; i < word.Length; i++)
            {
                int asci_val = word[i];
                int height = h[asci_val - 97];
                if (height > maxHeight)
                    maxHeight = height;
            }
            int area = word.Length * maxHeight;
            return area;

        }

        enum letter
        {
            a=1,
            b=2,
            c=3,
            d=4,
            e=5,
            f=6,
            g=7,
            h=8,
            i=9,
            j=10,
            k=11,
            l=12,
            m=13,
            n=14,
            o=15,
            p=16,
            q=17,
            r=18,
            s=19,
            t=20,
            u=21,
            v=22,
            w=23,
            x=24,
            y=25,
            z=26
        }

        static void IsPrime(int n)
        {
            bool result = true;
            if (n ==1) Console.WriteLine("Not prime");
            else if (n == 2) Console.WriteLine("Prime");
            else
            {
                var limit = Math.Ceiling(Math.Sqrt(n));
                for (int i = 2; i <= limit; i++)
                {
                    if (n % i == 0) { result = false; break; }
                }
                if (result) Console.WriteLine("Prime");
                else Console.WriteLine("Not prime");
            }
            
        }

       

        static int hurdleRace(int k, int[] height)
        {
            int max = height.Max();
            if (k >= max) return 0;
            else return max - k;

        }
        
     

        private static void incAll(int[] counters, int max)
        {
            for (int i = 0; i < counters.Length; i++)
            {
                counters[i] = max;
            }
        }

        private static int Max(int n, int m)
        {
            int result = n;
            if (m > n)
            {
                result = m;
            }
            return result;
        }


        static string catAndMouse(int x, int y, int z)
        {
            string res = "";
            int catA = Math.Abs(z - x);
            int catB = Math.Abs(z - y);
            if (catA<catB)  res="Cat A"; 
            else if (catA > catB)  res="Cat B"; 
            else  res= "Mouse C"; 
            return res;
        }

       

        

        static int getMoneySpent(int[] keyboards, int[] drives, int b)
        {
            int result = -1;
            for (int i = 0; i < keyboards.Length; i++)
            {
                for (int j = 0; j < drives.Length; j++)
                {
                    if (keyboards[i] + drives[j] <= b && keyboards[i] + drives[j] > result)
                        result = keyboards[i] + drives[j];
                }
            }
            return result;

        }


        static void PrintArray<T>(T[] arr)
        {
            foreach (var value in arr)
            {
                Console.WriteLine(value);
            }
        }



       

        


       

      

        static int findInteger(int[] arr, int n)
        {
            int neg = 0, pos = 0;
            int sum = 0;

            for (int i = 0; i < n; i++)
            {
                sum += arr[i];

                // If negative, then increment 
                // neg count. 
                if (arr[i] < 0)
                    neg++;

                // Else increment pos count.. 
                else
                    pos++;
            }

            return (sum / Math.Abs(neg - pos));
        }

      

        

        static int countingValleys(int n, string s)
        {
            int up = 0, down = 0, count = 0;
            foreach (char c in s)
            {
                if (c == 'U') up++;
                if (c == 'D') down++;

                if (down - up == 0 && c == 'U') count++;
            }
            return count;

        }

       

        static int utopianTree(int n)
        {
            int res = 1;
            for (int i = 1; i <= n; i++)
            {
                if (i % 2 != 0) res *= 2;
                else res++;
            }
            return res;
        }

        static int pageCount(int n, int p)
        {
            int total = n / 2;
            int right = p / 2;
            int left = total - right;
            if (right < left)
            {
                return right;
            }
            else
            {
                return left;
            }

            //int diffFirst = p - 1;
            //int diffLast = n - p;
            //int res = Math.Min(diffFirst, diffLast);




            //int cnt = 0,first=0, last=0;
            //for (int i = 1; i < n; i++)
            //{
            //    if (p == i && i == 1) cnt = 0;
            //    else if (p == i + 1 || p == i + 2) { cnt++; break; }
            //    //else if (p == i + 2) break;
            //    else if (p > i + 1 || p > i + 2) cnt++;
            //    else break;
            //}

            //first = cnt;
            //cnt = 0;
            //for (int i = n; i >= 1; i--)
            //{
            //    if (p == i && i == n) cnt = 0;
            //    else if (p == i - 1) { cnt++; break; }
            //    else if (p == i - 2) break;
            //    else if (p < i - 1 || p < i - 2) cnt++;
            //    else break;              
            //}
            //last = cnt;
            //int result = Math.Min(first, last);
            //return result;
        }

        static int sockMerchant(int n, int[] ar)
        {
            //Array.Sort(ar);
            //int ans = 0;
            //for (int i = 1; i < n; i++)
            //{
            //    if (ar[i] == ar[i - 1]) { ans++; i++; }
            //}
            //return ans;

            //return ar.GroupBy(x => x).Select(x => x.Count() / 2).Sum();
            IEnumerable<IGrouping<int, int>> temp;
            temp = ar.GroupBy(x => x);
            IEnumerable<int> temp1 = temp.Select(x => x.Count() / 2);
            return temp1.Sum();


            // int res = ar.GroupBy(x => x)
            //.Where(y => y.Count() >= 2)
            //.Select(z => z.Count() / 2).Sum();
            // Console.WriteLine(res);

            //int result = 0;
            //int count;
            //int previous=0;
            //Array.Sort(ar);
            //for (int i = 0; i < ar.Length; i++)
            //{
            //    if (previous !=ar[i])
            //    {
            //        count = ar.Where(x => x == ar[i]).Count();
            //        if (count % 2 == 0) result += count / 2;
            //        else if ((count - 1) % 2 == 0) result += (count - 1) / 2;
            //        previous = ar[i];
            //    }                
            //    //i = count - 1;
            //}

            //return result;
        }

        //public static Node insert(Node head, int data)
        //{
        //    //Complete this method
        //    //Node n= new Node(data);
        //    if (head is null)
        //    {
        //        return new Node(data);
        //    }
        //    else if (head.next is null)
        //    {
        //        head.next = new Node(data);
        //    }
        //    else insert(head.next, data);

        //    return head;
        //}

        //public static void display(Node head)
        //{
        //    Node start = head;
        //    while (start != null)
        //    {
        //        Console.Write(start.data + " ");
        //        start = start.next;
        //    }
        //}

       

        // Complete the bonAppetit function below.
        static void bonAppetit(List<int> bill, int k, int b)
        {
            int totalBill = 0;
            for (int i = 0; i < bill.Count; i++)
            {
                if (i != k) totalBill += bill[i];
            }
            if ((totalBill / 2) == b) Console.WriteLine("Bon Appetit");
            else Console.WriteLine(b - (totalBill / 2));

            Console.ReadLine();
        }

        

        static string dayOfProgrammer(int year)
        {
            string result = "";
            if (year == 1918)
            {
                if (GregorianLeapYear(year)) result = FindDay(16, year);
                else result = FindDay(15, year);
                return result;
            }
            else if (year >= 1700 && year <= 1917)
            {
                if (JulianLeapYear(year)) result = FindDay(29, year);
                else result = FindDay(28, year);
                return result;
            }
            else
            {
                if (GregorianLeapYear(year)) result = FindDay(29, year);
                else result = FindDay(28, year);
                return result;
            }
        }

        static string FindDay(int FebDays, int year)
        {
            int Days = 0;
            int DayNeeded = 256;
            int temp = 0;
            int date = 0;
            int month = 0;
            for (int i = 1; i <= 12; i++)
            {
                if (new[] { 1, 3, 5, 7, 8, 10, 12 }.Contains(i))
                {
                    Days += 31;
                    if (Days > DayNeeded)
                    {
                        temp = Days - DayNeeded;
                        date = 30 - temp;
                        month = i - 1;
                        break;
                    }
                }
                else if (new[] { 4, 6, 9, 11 }.Contains(i))
                {
                    Days += 30;
                    if (Days > DayNeeded)
                    {
                        temp = Days - DayNeeded;
                        date = 31 - temp;
                        month = i;
                        break;
                    }
                }
                else Days = FebDays;
            }
            string strResult = date.ToString("00") + "." + month.ToString("00") + "." + year;
            return strResult;
        }

        static bool GregorianLeapYear(int year)
        {
            if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) return true;
            else return false;
        }

        static bool JulianLeapYear(int year)
        {
            if (year % 4 == 0) return true;
            else return false;
        }

        enum Month
        {
            Jaunary = 1,
            February = 2,
            March = 3,
            April = 4,
            May = 5,
            June = 6,
            July = 7,
            August = 8,
            September = 9,
            Octomber = 10,
            November = 11,
            December = 12
        }


        enum WeekDays
        {
            Monday = 10,
            Tuesday,
            Wednesday,
            Thursday = 11,
            Friday,
            Saturday,
            Sunday
        }

        static int migratoryBirds(List<int> arr)
        {
            Dictionary<int, int> res = new Dictionary<int, int>();
            int cnt = 1;
            foreach (int num in arr)
            {
                if (res.Where(x => x.Key == num).Count() > 0)
                {
                    cnt = cnt + 1;
                    res[num] += 1;
                }
                else { res.Add(num, 1); };
            }

            int finalResult = res.Values.Max();
            int finalKey = 0;
            res = res.Where(x => x.Value == finalResult).ToDictionary(x => x.Key, y => y.Value);

            foreach (KeyValuePair<int, int> entry in res)
            {
                if (finalKey == 0) { finalKey = entry.Key; }
                else if (!(finalKey < entry.Key)) { finalKey = entry.Key; }

            }
            return finalKey;
        }

        static int factorial(int n)
        {
            if (n == 1) return n;
            else return n * factorial(n - 1);

        }

        static int birthday(List<int> s, int d, int m)
        {
            int result = 0;

            for (int num = 0; num < s.Count; num++)
            {
                int val = s[num];
                for (int i = 1; i < m; i++)
                {
                    if (num + i < s.Count)
                    {
                        val += s[num + i];
                    }
                }
                if (val == d) { result++; }
            }

            return result;
        }

        void Add(int num1, int num2, int num3, int num4)
        {

        }

        static void Add(params int[] arr)
        {

        }

        // Complete the breakingRecords function below.
        static int[] breakingRecords(int[] scores)
        {
            int HighScore = 0, LowScore = 0;
            int[] result = new int[2];
            for (int num = 0; num < scores.Length; num++)
            {
                if (num == 0)
                {
                    HighScore = LowScore = scores[num];
                }
                else
                {
                    if (scores[num] > HighScore) { HighScore = scores[num]; result[0] += 1; }
                    else if (scores[num] < LowScore) { LowScore = scores[num]; result[1] += 1; }
                }
            }
            return result;
        }

        static void FindEvenOddOfString()
        {
            int T = Convert.ToInt32(Console.ReadLine());
            string[] strVal = new string[T];


            for (int i = 0; i < T; i++)
            {
                strVal[i] = Console.ReadLine();
            }

            for (int i = 0; i < strVal.Length; i++)
            {
                char[] val = strVal[i].ToArray();
                string odd = "";
                string even = "";
                for (int j = 0; j < val.Length; j++)
                {
                    if (j % 2 == 0) { even += val[j]; }
                    else { odd += val[j]; }
                }
                Console.WriteLine(even + " " + odd);
            }
            Console.ReadLine();
        }

        private static void Write(out int y)
        {
            y = 0; //Comment this line to get error.
            Console.WriteLine(y);
            y = 1;
        }

        // Complete the kangaroo function below.
        // 0 3 4 2
        // 0 2 5 3
        // 21 6 47 3
        // 28 8 96 2-No failing for this.
        //43 2 70 2-No
        static string kangaroo(int x1, int v1, int x2, int v2)
        {
            bool val = Convert.ToBoolean((x2 - x1) % (v1 - v2));
            if (v1 > v2 && !(val))
                return "YES";
            else
                return "NO";

            //string s2 = "NO";
            //int k1_last = x1 + v1;
            //int k2_last = x2 + v2;
            //if ((x1 < x2 && v1 < v2) || (x2 < x1 && v2 < v1))
            //{

            //}
            //else if (x1 > x2)
            //{

            //}
            //else
            //{
            //    for (int i = 1; i <= int.MaxValue && i > 0; i++)
            //    {
            //        if (k1_last == k2_last)
            //        {
            //            s2 = "YES";
            //            break;
            //        }
            //        k1_last = k1_last + v1;
            //        k2_last = k2_last + v2;
            //        i = k1_last > k2_last ? k1_last : k2_last;
            //    }
            //}
            //return s2;

            //Second solution
            //var tmp = Console.ReadLine().Split(' ');
            //int x1 = int.Parse(tmp[0]);
            //int v1 = int.Parse(tmp[1]);
            //int x2 = int.Parse(tmp[2]);
            //int v2 = int.Parse(tmp[3]);

            //float t1 = (float)(x1 - x2) / (float)(v2 - v1);
            //if (t1 < 0 || t1 != Math.Floor(t1))
            //{
            //    Console.WriteLine("NO");
            //}
            //else
            //{
            //    Console.WriteLine("YES");
            //}

            //Third Solution
            //string[] tokens_x1 = Console.ReadLine().Split(' ');
            //int x1 = Convert.ToInt32(tokens_x1[0]);
            //int v1 = Convert.ToInt32(tokens_x1[1]);
            //int x2 = Convert.ToInt32(tokens_x1[2]);
            //int v2 = Convert.ToInt32(tokens_x1[3]);

            //while (((x1 < x2 && v1 > v2) || (x1 > x2 && v1 < v2)) && x1 != x2)
            //{
            //    x1 += v1;
            //    x2 += v2;
            //}
            //Console.WriteLine(x1 == x2 ? "YES" : "NO");
        }

        static void WriteLog(string log)
        {
            string fileName = string.Empty;
            try
            {
                fileName = Path.GetFullPath(@"/ApiLog.txt");

                using (StreamWriter sw = File.AppendText(fileName))
                {
                    sw.WriteLine(log);
                }
            }
            catch (Exception e)
            {
                //fileName = Path.GetFullPath(@"~/App_Data/ApiLog.txt");
                //using (StreamWriter sw = File.AppendText(fileName))
                //{
                //    sw.WriteLine(DateTime.Now.ToString() + ": " + log);
                //}
            }

        }


        static int FindSumOfArray(int[] arr)
        {
            return arr.Sum(x => x);
        }

        static List<int> FindScoreDiff(List<int> a, List<int> b)
        {
            int alisScore = 0;
            int bobScore = 0;
            for (int i = 0; i < a.Count; i++)
            {
                if (a[i] > b[i]) { alisScore++; }
                else if (a[i] < b[i]) { bobScore++; }
            }
            List<int> score = new List<int> { alisScore, bobScore };
            return score;
        }

        static long FindSumOfBigNumbers(long[] arr)
        {
            return arr.Sum(x => (long)x);
        }

        static int diagonalDifference(int[][] arr)
        {
            int res = 0;
            int[] d1 = new int[arr.Length];
            int[] d2 = new int[arr.Length];
            for (int i = 0; i < arr.Length; i++)
            {
                d1[i] = arr[i][i];
                d2[i] = arr[i][arr.Length - i - 1];
            }

            res = d1.Sum(x => x) - d2.Sum(x => x);
            return Math.Abs(res);
        }

        static void plusMinus(int[] arr)
        {
            decimal pNumber = arr.Where(x => x > 0).Count();
            decimal nNumber = arr.Where(x => x < 0).Count();
            decimal zNumber = arr.Where(x => x == 0).Count();

            Console.WriteLine((pNumber / arr.Length).ToString("N6"));
            Console.WriteLine((nNumber / arr.Length).ToString("N6"));
            Console.WriteLine((zNumber / arr.Length).ToString("N6"));
        }

        static void staircase(int n)
        {
            //for (int i= 1; i <= n; i++)
            //{
            //    int j = i;               
            //    while (j != n)
            //    { 
            //    Console.Write(" ");
            //        j++;
            //    }
            //    j = 0;
            //    while (j != i)
            //    {
            //        Console.Write("#");
            //        j++;
            //    }                
            //    Console.WriteLine();
            //}

            //Second solution
            for (int i = 0; i < n; i++)
                Console.WriteLine(new String('#', i + 1).PadLeft(n, ' '));

        }

        // Complete the miniMaxSum function below.
        static void miniMaxSum(int[] arr)
        {
            Array.Sort(arr);
            long max = arr.Sum(x => (long)x) - arr[0];
            long min = arr.Sum(x => (long)x) - arr[arr.Length - 1];
            Console.WriteLine(min + " " + max);
            Console.ReadLine();
        }

        static int birthdayCakeCandles(int[] ar)
        {
            Array.Sort(ar);
            return ar.Where(x => x == ar[ar.Length - 1]).Count();
            //return ar.Where(x => x == ar.Max()).Count();


        }

        static string timeConversion(string s)
        {
            //DateTime date = DateTime.ParseExact(s, "HH:mm:ss", CultureInfo.InvariantCulture);
            //return date.ToString();
            string date = "";
            date = DateTime.ParseExact(s, "hh:mm:sstt", CultureInfo.InvariantCulture).ToString("HH:mm:ss");
            return date;
        }

        static void FindCommonDenominator()
        {
            // Console.WriteLine("Please enter two numbers with space in it");            
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp));
            int a = arr[0];
            int b = arr[1];
            int count = 0;
            int i = a >= b ? a : b;
            for (int j = 1; j <= i; j++)
            {
                if (a % j == 0 && b % j == 0)
                {
                    count++;
                }
            }
            Console.WriteLine(count);
            Console.ReadLine();
        }

        static void AddToGreaterSum()
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp));
            //int[] arr = new int[] { 1, 2, 3, 4, 5 };
            arr = arr.OrderBy(x => x).ToArray();
            int cSum = arr.Sum();
            int totalNum = arr.Length;
            int result = 0;
            for (int i = 0; i < arr[totalNum - 1]; i++)
            {
                if ((i * totalNum) > cSum) { result = i; break; }
            }
            Console.WriteLine(result);
            Console.ReadLine();
        }

        static int[] gradingStudents(int[] grades)
        {
            int[] result = new int[grades.Length];
            for (int num = 0; num < grades.Length; num++)
            {
                if (grades[num] < 38) { result[num] = grades[num]; }
                else
                {
                    int m5 = FindMultiplierOf5(grades[num]);
                    if (m5 - grades[num] < 3) { result[num] = m5; }
                    else { result[num] = grades[num]; }
                }
            }
            return result;
        }

        static int FindMultiplierOf5(int num)
        {
            while (num % 5 != 0)
            {
                num++;
            }
            return num;
        }

        static void DataTypeCheck()
        {
            int i = 4;
            double d = 4.0;
            string s = "HackerRank ";

            int i2;
            double d2;
            string s2;
            i2 = Convert.ToInt32(Console.ReadLine());
            d2 = Convert.ToDouble(Console.ReadLine());
            s2 = Convert.ToString(Console.ReadLine());

            Console.WriteLine(i + i2);
            Console.WriteLine((d + d2).ToString("0.0"));
            Console.WriteLine(s + s2);
        }

        // Complete the solve function below.
        static void solve(double meal_cost, int tip_percent, int tax_percent)
        {
            double res = meal_cost + (meal_cost * tip_percent) / 100 + (meal_cost * tax_percent) / 100;
            Console.WriteLine(Math.Round(res));
            Console.ReadLine();

        }

        static int buildString(int a, int b, string s)
        {
            int res = 0;
            StringBuilder sb = new StringBuilder();
            char[] ch = s.ToArray();
            for (int i = 0; i < s.Length; i++)
            {
                sb.Append(ch[i]);
            }

            return res;

        }

       

        static void CheckForBoxingAndUnboxing()
        {
            int num = 23;
            object obj = num;
            int i = (int)obj;
            Console.WriteLine("value of obj is:" + obj);
            Console.WriteLine("value of i is:" + i);
            Console.ReadLine();
        }


        //static void CheckForSumAllEvenNum()
        //{
        //    int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        //    long result = numbers.Where(num => num % 2 == 0).Sum(x => (long) x);
        //    Console.WriteLine(result);
        //    Console.ReadLine();

        //}

        static void CheckForExpressionTree()
        {

        }

        static void ConvertToMeters(int km)
        {
            Console.WriteLine(km * 1000);

        }

        static void CheckForDelegatePredicateFuncLambda()
        {
            //Using Delegate way
            AreaCalculator arCalc = CalculateArea;
            int r = 20;
            double result = arCalc.Invoke(r);
            Console.WriteLine(result);

            //Using Anonymous method way
            double result1;
            AreaCalculator arCalc1 = new AreaCalculator(delegate (int r1)
            {
                return result1 = 3.14 * r1 * r1;
            });
            result1 = arCalc1(20);
            Console.WriteLine(result1);

            //Using Lambda way
            double result2;
            //AreaCalculator arCalc2 = new AreaCalculator(r2 => 3.14 * r2 * r2);
            AreaCalculator arCalc2 = r2 => 3.14 * r2 * r2;
            result2 = arCalc2(20);
            Console.WriteLine(result2);

            //Using Lambda + Func (Generic delegates)
            double result3;
            Func<double, double> arCalc3 = r2 => 3.14 * r2 * r2;
            result3 = arCalc3(20);
            Console.WriteLine(result2);

            //Using Action generic delegates
            Action<string> myAction = x => Console.WriteLine(x);
            myAction("Hello");

            //Using Predicate generic delegates
            Predicate<string> myPredicate = x => x.Length > 5;
            Console.WriteLine(myPredicate("Hello1"));
            //Predicate<Student> studPredicate = new Predicate<Student>(FindStudent);
            //return students.Find(stud => studPredicate(stud));
            List<string> someValues = new List<string>();
            someValues.Add("Jigar");
            someValues.Add("Mahi123");
            string str = someValues.Find(myPredicate);
            Console.WriteLine(str);

            //Func with Any
            Func<string, bool> myFunc = x => x.Length > 5;
            Console.WriteLine(someValues.Any(myFunc));

            Console.ReadLine();
        }

        static double CalculateArea(int r)
        {
            return 3.14 * r * r;
        }

        static void CheckForLinQ()
        {
            List<Account> myAccounts = new List<Account>
            {
                new Account { status="active", balance=10 },
                new Account { status="active", balance=20 },
                new Account { status="inactive", balance=30 }
            };

            int total = myAccounts.Where(account => account.status == "active").Sum(x => x.balance);
            Console.WriteLine(total);
            Console.ReadLine();


            //int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            //1=>Find Even numbers using linq query way
            //GridView1.DataSource = from number in numbers
            //                       where number % 2 != 0
            //                       orderby number descending
            //                       select number;

            //2=>Find Gender=="M" using linq query way-directly bind to grid
            //Student student = new Student();
            //GridView1.DataSource = from stud in student.GetAllStudents()
            //                       where stud.Gender == "M"
            //                       select stud;

            //3=>Find Gender=="M" using linq query way-take this into IEnumerable<Student> type
            //IEnumerable<Student> stud1 = from stud in student.GetAllStudents()
            //                        where stud.Gender == "M"
            //                        select stud;
            // GridView1.DataSource = stud1;

            //4=>Find Gender=="M" using linq lambda expression way
            //GridView1.DataSource = student.GetAllStudents().Where(stud => stud.Gender == "M");

            //calling code
            //student = student.GetStudent();
            //List<Student> students = new List<Student>();
            //students.Add(student);
            //GridView1.DataSource = students;
            //GridView1.DataBind();


        }

        static void CheckForShortestMissingInteger()
        {
            Solution s = new Solution();
            int[] A = new int[] { 1, 3, 6, 4, 1, 2 };
            int result = s.solution(A);
            Console.WriteLine(result);

            A = new int[] { -1, -3 };
            result = s.solution(A);
            Console.WriteLine(result);

            A = new int[] { 1, 2, 3 };
            result = s.solution(A);
            Console.WriteLine(result);

            Console.ReadLine();
        }

        static void CheckForReturnTypeOverloading()
        {
            Hello hello1 = new Hello();
            int i;
            i = hello1.testfunc();
            Console.WriteLine(i);

            Single j;
            j = hello1.testfunc();
            Console.WriteLine(j);

            Console.ReadLine();
        }

        static void CheckForDateAndNull()
        {
            DateTime dt1 = DateTime.Now;
            //dt1 = null;
            Console.WriteLine(dt1.ToString());
            Console.WriteLine("By default datetime variable cannot have null value");

            DateTime? dt2;
            dt2 = null;
            Console.WriteLine("with nullable value is:" + dt2.ToString());
            Console.WriteLine("But you can make it nullable and assign null values");

            Console.ReadLine();
        }

        static void CheckForArray()
        {
            Console.WriteLine("String Array...");
            string[] arr = new string[4];
            arr[0] = Convert.ToString(12);
            arr[1] = "hello!";
            arr[2] = Convert.ToString(false);
            arr[3] = Convert.ToString(12.22);

            foreach (string s in arr)
            {
                Console.WriteLine(s);
            }

            Console.WriteLine("Object Array...");
            object[] arr1 = new object[4];
            arr1[0] = 12;
            arr1[1] = "hello!";
            arr1[2] = false;
            arr1[3] = 12.22;

            foreach (object s in arr1) //or use var instead of object here
            {
                Console.WriteLine(s);
            }

            Console.WriteLine("ArrayList...");
            ArrayList arr2 = new ArrayList();
            arr2.Add(12);
            arr2.Add("12");
            // arr2.Add('12');
            arr2.Add(12.22);

            foreach (var s in arr2) //or use var instead of object here
            {
                Console.WriteLine(s);
            }

            Console.ReadLine();
        }

        static void CheckForOutParam()
        {
            int i = 3;
            int j = 2;
            Func1(ref i);
            Func2(out j);
            Console.WriteLine(i + " " + j);
            Console.ReadLine();
        }

        static void Func1(ref int num)
        {
            num = num + num;
        }

        static void Func2(out int num)
        {
            num = 4;
            num = num * num;
        }

        static void CheckIEnumerableAndIEnumerator()
        {
            List<int> years = new List<int>();
            years.Add(1991);
            years.Add(1992);
            years.Add(2001);
            years.Add(2002);

            Console.WriteLine("Using List");
            foreach (int i in years)
            {
                Console.WriteLine(i);
            }

            IEnumerable<int> iEnumerable = (IEnumerable<int>)years;
            Console.WriteLine("Using IEnumerable");
            years90iEnumerable(iEnumerable);
            //foreach (int i in iEnumerable)
            //{
            //    Console.WriteLine(i);
            //}

            IEnumerator<int> iEnumerator = years.GetEnumerator();
            Console.WriteLine("Using IEnumerator");
            years90(iEnumerator);
            //while (iEnumerator.MoveNext())
            //{
            //    Console.WriteLine(iEnumerator.Current);
            //}

            Console.ReadLine();
        }

        static void years90iEnumerable(IEnumerable<int> iEnumerable)
        {
            foreach (int i in iEnumerable)
            {
                Console.WriteLine(i);
                if (i > 2000) { years20iEnumerable(iEnumerable); }
            }

            //while (iEnumerator.MoveNext())
            //{
            //    Console.WriteLine(iEnumerator.Current);
            //    if (iEnumerator.Current > 2000)
            //    { years20(iEnumerator); }
            //}
        }

        static void years20iEnumerable(IEnumerable<int> iEnumerable)
        {
            foreach (int i in iEnumerable)
            {
                Console.WriteLine(i);
            }
        }

        static void years90(IEnumerator<int> iEnumerator)
        {
            while (iEnumerator.MoveNext())
            {
                Console.WriteLine(iEnumerator.Current);
                if (iEnumerator.Current > 2000)
                { years20(iEnumerator); }


            }
        }

        static void years20(IEnumerator<int> iEnumerator)
        {
            while (iEnumerator.MoveNext())
            {
                Console.WriteLine(iEnumerator.Current);
            }
        }

        static void CheckMainThread()
        {
            CheckAsyncAndAwait();
            Console.WriteLine("Main Thread");
            Console.ReadLine();
        }

        static async void CheckAsyncAndAwait()
        {
            await Task.Run(new Action(LongTask));
            Console.WriteLine("function is called now!");
            Console.ReadLine();
        }

        static void LongTask()
        {
            Thread.Sleep(20000);
            Console.WriteLine("Within Function");
        }

        static void CheckEquals()
        {
            object o = "some test";
            //object o1 = o;
            object o1 = new string("some test".ToCharArray());
            Console.WriteLine(o1 == o);
            Console.WriteLine(o1.Equals(o));

            string s1 = "some test";
            string s2 = new string("some test".ToCharArray());
            Console.WriteLine(s1 == s2);
            Console.WriteLine(s1.Equals(s2));

            Console.ReadLine();
        }


        static void CheckForNullable()
        {
            Nullable<bool> male = null;
            int? num = null;
            DateTime? dt = null;
            Nullable<DateTime> dt1 = null;
        }

        static void CheckForEnum()
        {
            foreach (int i in Enum.GetValues(typeof(Days)))
                Console.WriteLine(i);
            Console.ReadLine();
        }

        enum Days
        {
            Monday = 1,
            Tuesday = 2,
            Wednesday = 3,
            Thursday,
            Friday,
            Saturday,
            Sunday
        }

        static void CheckDataTypes()
        {
            //float are 32 bit data types, double are 64 bit data types.
            //Decimal are 128 bit data types.
            Console.WriteLine("below all are integer number");
            //below are integers
            Add(1, 1);
            Console.WriteLine("======================");

            Console.WriteLine("below all are double number");
            Add(1.0, 1.0);
            Add(1.0, 1);
            Add(1, 1.0);
            Add(1f, 1.0);
            Add(1.0, 1f);
            Console.WriteLine("======================");

            //below three are floats
            Console.WriteLine("below all are float number");
            Add(1f, 1);
            Add(1, 1f);
            Add(1f, 1f);
            Console.WriteLine("======================");

            //below are decimal
            Console.WriteLine("below all are decimal number");
            Add(1m, 1m);
            Console.ReadLine();
        }

        //static void Add(Single i, Single j)
        //{
        //    Console.WriteLine("both single");
        //}

        static void Add(float i, float j)
        {
            Console.WriteLine("both float");
        }

        static void Add(double i, double j)
        {
            Console.WriteLine("both double");
        }

        static void Add(decimal i, decimal j)
        {
            Console.WriteLine("both decimal");
        }

        static void Add(int i, int j)
        {
            Console.WriteLine("both int");
        }

        static void CheckForDynamicAndVar()
        {
            //This line gives compile time error
            var a1 = 1;
            dynamic b1 = 2;


            //a1 = "abc"; 
            b1 = "abc";
            Console.WriteLine(a1);
            Console.WriteLine(b1);
            Console.ReadLine();
        }

        static double Calculate(Func<double, double> op)
        {
            return op(radius);
        }

        static void CheckForTimeNullCompare()
        {
            if (time == null)
            {
                Console.WriteLine("it is null");
            }
            else
            {
                Console.WriteLine("it is not null");
            }
            Console.ReadLine();
        }

        static void SumAllEvenNum()
        {
            int[] numArr = new int[] { 15, 5, 7, 9, -6, -10 };
            int result = 0;
            foreach (int num in numArr)
            {
                if (num % 2 == 0)
                {
                    result += num;
                }
            }
            //return numArr.Where(i => i % 2 == 0).Sum(i => (long)i);
            //return (from i in numArr where i % 2 == 0 select (long)i).Sum();
            Console.WriteLine(result.ToString());
            Console.ReadLine();
        }

        static void CheckForNullValue()
        {
            Console.WriteLine(location == null ? "location is null" : location);
            Console.WriteLine(time == null ? "time is null" : time.ToString());
            Console.ReadLine();
        }

        protected static void SendEmail()
        {
            MailMessage mail = new MailMessage();

            mail.To.Add(new MailAddress("js10855@gmail.com"));
            mail.Subject = "Book Order";
            mail.Body = "Please find Attached books you ordered.";

            SmtpClient smtp = new SmtpClient();
            smtp.UseDefaultCredentials = false;
            smtp.Host = "smtp.office365.com";
            smtp.Port = 25;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            System.Net.NetworkCredential credential = new System.Net.NetworkCredential();
            credential.UserName = "store@rante.com";
            mail.From = new MailAddress(credential.UserName);
            credential.Password = "Rante@CA23";
            smtp.Credentials = credential;
            smtp.EnableSsl = true;
            try
            {
                smtp.Send(mail);
                Console.WriteLine("Email sent successfully.");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error occurred as:" + e.Message);
            }
            Console.ReadLine();
        }

        protected static void CheckForMultipleCatch()
        {
            int x = 0;
            int div = 0;
            try
            {
                div = 100 / x;
                Console.WriteLine("Not executed line");
            }
            catch (DivideByZeroException de)
            {
                Console.WriteLine("DivideByZeroException");
            }
            catch (Exception ee)
            {
                Console.WriteLine("Exception");
            }
            finally
            {
                Console.WriteLine("Finally Block");
            }
            Console.WriteLine("Result is {0}", div);
            Console.ReadLine();
        }

        static int solution(int[] A)
        {
            Array.Sort(A);
            int last = 1;
            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] <= 0) continue;
                else if (last == A[i]) last += 1;
            }
            return last;
        }
    }

    class Solution
    {
        //public int solution(int[] A)
        //{
        //    // write your code in C# 6.0 with .NET 4.5 (Mono)
        //    int result;
        //    if (A.Max() < 0) { result = 1; }
        //    else {result = A.Max() + 1; }
        //    for (int i=1; i <= A.Max(); i++)
        //    {
        //        if (!A.Contains(i)) { result = i; }
        //    }
        //    return result ;

        //}

        public int solution(int[] A)
        {
            int flag = 1;

            A = A.OrderBy(x => x).ToArray();

            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] <= 0)
                    continue;
                else if (A[i] == flag)
                {
                    flag++;
                }

            }

            return flag;
        }
    }

    class Hello
    {
        public int testfunc()
        {
            return 3;
        }

        public Single testfunc(bool? bt = false)
        {
            return 1.4f;
        }
    }

    abstract class abs
    {
        public abstract void subtract();
        //public void Sum() {
        //    int i = 2;
        //}
        //public int prop { get; set; }
        //public abstract void test();
    }

    interface ITest1 : ITest2
    {
        void Sum();
        void subtract();
        int prop { get; set; }
        void test();
    }

    interface ITest2
    {
        void test();
    }

    interface I1
    {
        void Method1();
    }

    interface I2
    {
        void Method1();
    }

    class C1 : I1, I2
    {
        public void Method1()
        {
            Console.WriteLine("Method1 called");
        }

        void I1.Method1()
        {
            Console.WriteLine("I1 Method1 called");
        }

        void I2.Method1()
        {
            Console.WriteLine("I2 Method1 called");
        }
        //checking keywords
        //int var { get; set; }
        //public static int @this {get;set;}
    }

   

    public class Student
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        List<Student> students;

        public List<Student> GetAllStudents()
        {
            List<Student> students = new List<Student>
            {
                new Student { ID = 201, Name = "James", Gender = "M" },
                new Student { ID = 202, Name = "Stacy", Gender = "F" }
            };

            //List<Student> students = new List<Student>();
            students.Add(new Student { ID = 101, Name = "Mark", Gender = "M" });
            students.Add(new Student { ID = 102, Name = "Lucy", Gender = "F" });
            //
            //};

            Student student = new Student()
            {
                ID = 1,
                Name = "Jigar",
                Gender = "M"
            };
            students.Add(student);

            student = new Student()
            {
                ID = 2,
                Name = "Bhumi",
                Gender = "F"
            };
            students.Add(student);



            return students;
        }

        public Student GetStudent()
        {
            students = GetAllStudents();
            Predicate<Student> studPredicate = new Predicate<Student>(FindStudent);
            //return students.Find(stud => studPredicate(stud));
            // return students.Find(delegate(Student stud) { return stud.ID == 102; });
            int count = students.Count(stud => stud.Name.StartsWith("M"));
            return students.Find(Emp => Emp.ID == 102);


        }

        public bool FindStudent(Student stud)
        {
            return stud.ID == 102;
        }
    }

    public class Account
    {
        public string status { get; set; }
        public int balance { get; set; }
    }

    abstract class Book
    {

        protected String title;
        protected String author;

        public Book(String t, String a)
        {
            title = t;
            author = a;
        }
        public abstract void display();
    }

    class MyBook : Book
    {
        int Price;
        public MyBook(String title, String author, int price) : base(title, author)
        {
            Price = price;
        }

        public override void display()
        {

            Console.WriteLine("Title:" + title);
            Console.WriteLine("Author:" + author);
            Console.WriteLine("Price:" + Price);
        }
    }

    //class Node
    //{
    //    public int data;
    //    public Node next;
    //    public Node(int d)
    //    {
    //        data = d;
    //        next = null;
    //    }

    //}

    class BSTNode
    {
        public BSTNode left, right;
        public int data;
        public BSTNode(int data)
        {
            this.data = data;
            left = right = null;
        }
    }

    class BSTSolution
    {
        private static bool isRealRoot=false;
        static public int getHeight(BSTNode root)
        {
            if (root ==null)
            {
                return -1;
            }
            int leftDepth = getHeight(root.left);
            int rightDepth = getHeight(root.right);

            return (leftDepth > rightDepth ? leftDepth : rightDepth) + 1;

        }

        static public void levelOrder(BSTNode root)
        {
            //if (root != null)
            //{
            //    if (!isRealRoot) Console.Write(root.data + " ");

            //    BSTNode curLeft = root.left;
            //    if (curLeft != null) Console.Write(curLeft.data + " ");
            //    BSTNode curRight = root.right;
            //    if (curRight != null) Console.Write(curRight.data + " ");
            //    isRealRoot = true;

            //    levelOrder(curLeft);
            //    levelOrder(curRight);
            //}

            Queue<BSTNode> q = new Queue<BSTNode>();
            BSTNode n;
            if (root != null) q.Enqueue(root);

            while (q.Count > 0)
            {
                n = q.Dequeue();
                Console.Write(n.data + " ");

                if (n.left != null) q.Enqueue(n.left);
                if (n.right != null) q.Enqueue(n.right);
            }


        }


        static public BSTNode insert(BSTNode root, int data)
        {
            if (root == null)
            {
                return new BSTNode(data);
            }
            else
            {
                BSTNode cur;
                if (data <= root.data)
                {
                    cur = insert(root.left, data);
                    root.left = cur;
                }
                else
                {
                    cur = insert(root.right, data);
                    root.right = cur;
                }
                return root;
            }
        }

    }
}
