﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.LeetCode
{
    /*
     Given two strings ransomNote and magazine, return true if ransomNote can be constructed by 
    using the letters from magazine and false otherwise.

    Each letter in magazine can only be used once in ransomNote.

     Example 1:

     Input: ransomNote = "a", magazine = "b"
     Output: false
     Example 2:

     Input: ransomNote = "aa", magazine = "ab"
     Output: false
     Example 3:

     Input: ransomNote = "aa", magazine = "aab"
     Output: true


     Constraints:

     1 <= ransomNote.length, magazine.length <= 105
     ransomNote and magazine consist of lowercase English letters.

     */
    internal class RansomNote
    {
        public bool CanConstruct(string ransomNote, string magazine)
        {
            Dictionary<char, int> count = new Dictionary<char, int>();           

            foreach (char c in magazine)
            {                
                if (count.ContainsKey(c)) count[c]++;
                else count.Add(c, 1);
            }

            foreach (char c in ransomNote)
            {
                if (!count.ContainsKey(c) || count[c]<=0) return false;
                else count[c]--;
            }
            return true;
            
            //for (int i = 0; i < ransomNote.Length; i++)
            //{
            //    var idx = magazine.IndexOf(ransomNote[i]);
            //    if (idx >= 0)
            //    {
            //        magazine = magazine.Remove(idx, 1);
            //    }
            //    else return false;
            //}
            //return true;
        }
    }
}
