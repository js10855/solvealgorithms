﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.LeetCode
{
    /*Given two 0-indexed integer arrays nums1 and nums2, return a list answer of size 2 where:

    answer[0] is a list of all distinct integers in nums1 which are not present in nums2.
    answer[1] is a list of all distinct integers in nums2 which are not present in nums1.
    Note that the integers in the lists may be returned in any order.
    Input: nums1 = [1,2,3], nums2 = [2,4,6]
Output: [[1,3],[4,6]]
Explanation:
For nums1, nums1[1] = 2 is present at index 0 of nums2, whereas nums1[0] = 1 and nums1[2] = 3 are not present in nums2. Therefore, answer[0] = [1,3].
For nums2, nums2[0] = 2 is present at index 1 of nums1, whereas nums2[1] = 4 and nums2[2] = 6 are not present in nums2. Therefore, answer[1] = [4,6].

        Input: nums1 = [1,2,3,3], nums2 = [1,1,2,2]
Output: [[3],[]]
Explanation:
For nums1, nums1[2] and nums1[3] are not present in nums2. Since nums1[2] == nums1[3], their value is only included once and answer[0] = [3].
Every integer in nums2 is present in nums1. Therefore, answer[1] = [].
         */

    class FindDifference
    {
        public static IList<IList<int>> solution(int[] nums1, int[] nums2)
        {
            List<IList<int>> result = new List<IList<int>>();
            IList<int> subResult1 = new List<int>();
            IList<int> subResult2 = new List<int>();

            for (int i = 0; i < nums1.Length; i++)
            {
                if (!nums2.Contains(nums1[i]))
                {
                    if (!subResult1.Contains(nums1[i])) subResult1.Add(nums1[i]);
                }
                    
            }

            for (int i = 0; i < nums2.Length; i++)
            {
                if (!nums1.Contains(nums2[i]))
                {
                    if (!subResult2.Contains(nums2[i])) subResult2.Add(nums2[i]);
                }                    
            }

            result.Add(subResult1);
            result.Add(subResult2);            
            
            return result;

        }
    }
}
