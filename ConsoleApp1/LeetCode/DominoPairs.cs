﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.LeetCode
{
    /*
     Given a list of dominoes, dominoes[i] = [a, b] is equivalent to dominoes[j] = [c, d] if and 
     only if either (a == c and b == d), or (a == d and b == c) - that is, one domino can be rotated to be equal to another domino.
     Return the number of pairs (i, j) for which 0 <= i < j < dominoes.length, and dominoes[i] is equivalent to dominoes[j].
 
    Example 1:

    Input: dominoes = [[1,2],[2,1],[3,4],[5,6]]
    Output: 1
    Example 2:

    Input: dominoes = [[1,2],[1,2],[1,1],[1,2],[2,2]]
    Output: 3
 
    Constraints:

    1 <= dominoes.length <= 4 * 104
    dominoes[i].length == 2
    1 <= dominoes[i][j] <= 9


     */
    class DominoPairs
    {
        public int NumEquivDominoPairs(int[][] dominoes)
        {
            List<string> temp = new List<string>();

            for (int i = 0; i < dominoes.Length; i++)
            {
                Array.Sort(dominoes[i]);
                temp.Add(string.Join("", dominoes[i]));
            }           


            return temp.GroupBy(x => x).Where(x => x.Count() != 1).Select(x => x.Count() * (x.Count() - 1) / 2).Sum();

            //int result = 0;
            //for (int i = 0; i < dominoes.Length - 1; i++)
            //{
            //    for (int j = 0; j < dominoes[i].Length - 1; j++)
            //    {
            //        if (dominoes[i][j] == dominoes[i + 1][j] || dominoes[i][j] == dominoes[i + 1][j + 1])
            //            result++;
            //    }
            //}
            //return result;
        }
    }
}
