﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.LeetCode
{
    /*
    You are given an m x n integer grid accounts where accounts[i][j] is the amount of money 
    the i​​​​​​​​​​​th​​​​ customer has in the j​​​​​​​​​​​th​​​​ bank. Return the wealth that the richest customer has.

    A customer's wealth is the amount of money they have in all their bank accounts. 
    The richest customer is the customer that has the maximum wealth.

     Example 1:

    Input: accounts = [[1,2,3],[3,2,1]]
    Output: 6
    Explanation:
    1st customer has wealth = 1 + 2 + 3 = 6
    2nd customer has wealth = 3 + 2 + 1 = 6
    Both customers are considered the richest with a wealth of 6 each, so return 6.
    Example 2:

    Input: accounts = [[1,5],[7,3],[3,5]]
    Output: 10
    Explanation: 
    1st customer has wealth = 6
    2nd customer has wealth = 10 
    3rd customer has wealth = 8
    The 2nd customer is the richest with a wealth of 10.
    Example 3:

    Input: accounts = [[2,8,7],[7,1,3],[1,9,5]]
    Output: 17
  

     */
    internal class RichestCustomerWealth
    {
        public int MaximumWealth(int[][] accounts)
        {
            int maxWealth = 0;

            foreach (var customer in accounts)
            {
                int totalWealth = 0;
                foreach (var account in customer)
                {
                    totalWealth += account;
                    //Console.WriteLine(totalWealth);
                }
                maxWealth = Math.Max(maxWealth, totalWealth);
            }
            return maxWealth;
        }

    //    int sum = 0;
    //    for(int i=0;i<accounts.Length;i++)
    //    {
    //        for(int j=1;j<accounts[i].Length;j++)
    //        {
    //            accounts[i][j]+= accounts[i][j - 1];
    //            //Console.WriteLine("updated:" + accounts[i][j]);
    //        }
    //sum = Math.Max(accounts[i][accounts[i].Length - 1], sum);
        //}
//return sum;

    }
}
