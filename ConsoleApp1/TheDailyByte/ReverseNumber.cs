﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    internal class ReverseNumber
    {
        public int Reverse(int x)
        {
            bool negative = false;
            if (x < 0)
            {
                negative = true;
                x *= -1;
            }

            long reversed = 0;
            var str = x.ToString();
            StringBuilder sb = new StringBuilder();
           

            for (int i = str.Length - 1; i >=0; i--)
            {
                if (str[i]!='-') sb.Append(str[i]);
            }
            reversed = Convert.ToInt64(sb.ToString());


            //while (x>0)
            //{
            //        reversed = (reversed * 10) + (x % 10);
            //    x /= 10;
            //}

            if (reversed > int.MaxValue) return 0;
            return negative ? (int)reversed * -1 : (int)reversed;
        }
    }
}
