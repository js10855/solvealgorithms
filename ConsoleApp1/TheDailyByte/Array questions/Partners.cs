﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
     Given an integer array, nums, return the total number of “partners” in the array.
     Note: Two numbers in our array are partners if they reside at different 
    indices and both contain the same value.

     Ex: Given the following array nums…

     nums = [5, 5, 3, 1, 1, 3, 3], return 5.
     5 (index 0) and 5 (index 1) are partners.
     1 (index 3) and 1 (index 4) are partners.
     3 (index 2) and 3 (index 5) are partners.
     3 (index 2) and 3 (index 6) are partners.
     3 (index 5) and 3 (index 6) are partners.

     */
    internal class Partners
    {
        public int findPartnerCounts(int[] arr)
        {
            int ans = 0;
            int n = arr.Length; 

            // for each index i and j
            for (int i = 0; i < n; i++)
                for (int j = i + 1; j < n; j++)

                    // finding the index with same
                    // value but different index.
                    if (arr[i] == arr[j])
                        ans++;
            return ans;
        }
    }
}
