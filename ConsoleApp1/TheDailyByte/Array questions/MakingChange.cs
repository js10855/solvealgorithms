﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
    Given an array that represents different coin denominations and an amount of change you need to make, 
    return the fewest number of coins it takes to make the given amount of change.
    Note: If it is not possible to create the amount of change with the coins you’re given, return -1.

    Ex: Given the following denominations and amount…

    coins = [1,5, 10, 25], amount = 78, return 6
    Take three 25 coins and three 1 coins for a total of 6 coins.

    */
    public static class ExtensionMethods
    {
        public static void Fill<T>(this T[] array, T value)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = value;
            }
        }
    }

    class MakingChange
    {
        private int[] _memo;
        private int[] _coins;
        public int CoinChange(int[] coins, int amount)
        {
            _memo = new int[amount + 1];
            _memo.Fill(-999);            
            _coins = coins;
            return DP(amount);
        }

        

        private int DP(int amount)
        {
            if (amount == 0)
            {
                return 0;
            }

            if (amount < 0)
            {
                return -1;
            }

            if (_memo[amount] != -999)
            {
                return _memo[amount];
            }

            int result = int.MaxValue;
            for (var i = 0; i < _coins.Length; i++)
            {
                var diff = amount - _coins[i];
                var subProblem = DP(diff);
                if (subProblem == -1)
                {
                    continue;
                }

                result = Math.Min(result, 1 + subProblem);
            }

            _memo[amount] = result == int.MaxValue ? -1 : result;
            return _memo[amount];
        }

        // Array.Sort(coins);
        //if (amount == 0) return 0;
        //coins = coins.OrderByDescending(x => x).ToArray();
        //int counter = 0;

        //for (int i = 0; i < coins.Length; i++)
        //{
        //    if (amount>=coins[i])
        //    {
        //        while (amount>=coins[i])
        //        {
        //            amount -= coins[i];
        //            counter++;
        //        }                    
        //    }
        //}
        //if (amount > 0) counter = -1;
        //return counter;
        // }
    }
}
