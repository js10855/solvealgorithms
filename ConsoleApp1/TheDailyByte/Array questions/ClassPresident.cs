﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
     High school students are voting for their class president and 
    you’re tasked with counting the votes. 
    Each presidential candidates is represented by a unique integer and 
    the candidate that should win the election is the candidate that 
    has received more than half the votes. 
    Given a list of integers, return the candidate that should become 
    the class president.
    Note: You may assume there is always a candidate that 
    has received more than half the votes.

    Ex: Given the following votes…

    votes = [1, 1, 2, 2, 1], return 1.
    Ex: Given the following votes…

    votes = [1, 3, 2, 3, 1, 2, 3, 3, 3], return 3.
     */
    internal class ClassPresident
    {
        public int findPresident(int[] votes)
        {
            Dictionary<int, int> myDic = new Dictionary<int, int>();
            Array.Sort(votes);
            myDic.Add(votes[0],1);
            int largestVal = 0;
            int result = 0;

            for (int i = 1; i < votes.Length-1; i++)
            {
                if (votes[i] == votes[i + 1])
                {
                    myDic[votes[i]]++;
                }
                else myDic.Add(votes[i + 1], 1);                
            }

            foreach (KeyValuePair<int, int> kvp in myDic)
            {
                if (kvp.Value > largestVal)
                {
                    largestVal = kvp.Value;
                    result = kvp.Key;
                }
            }

            return result;
        }
    }
}
