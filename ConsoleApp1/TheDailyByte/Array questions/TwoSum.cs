﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.Array_questions
{
    /*
    Given an array of integers, return whether or not two numbers sum to a given target, k.
    Note: you may not sum a number with itself.

    Ex: Given the following...

    [1, 3, 8, 2], k = 10, return true (8 + 2)
    [3, 9, 13, 7], k = 8, return false
    [4, 2, 6, 5, 2], k = 4, return true (2 + 2)

    */
    class TwoSum
    {
        public static bool solution()
        {
            int[] A = { 4, 2, 6, 5, 2 };
            int k = 4;
            for (int i = 0; i < A.Length; i++)
            {
                for (int j = i + 1; j < A.Length; j++)
                {
                    if (A[i] + A[j] == k) return true;
                }
            }
            return false;
        }
    }
}
