﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.Array_questions
{
    /*
     Given a list of strings, return a list of strings containing all anagrams grouped together.

     Ex: Given the following list of strings strs…

     strs = ["car", "arc", "bee", "eeb", "tea"], return
     [
       ["car","arc"],
       ["tea"],
       ["bee","eeb"]
     ]
     */

    internal class GroupWords
    {
        public IList<IList<string>> GroupAnagrams(string[] strs)
        {
            if (strs == null || strs.Length == 0)
                return new List<IList<string>>();

            List<IList<string>> res = new List<IList<string>>();
            Dictionary<string, List<string>> dict = new Dictionary<string, List<string>>();

            foreach (var str in strs)
            {
                string cur = new string(str.OrderBy(x => x).ToArray());

                if (!dict.ContainsKey(cur))
                    dict.Add(cur, new List<string>());

                dict[cur].Add(str);
            }

            foreach (var item in dict.Values)
                res.Add(item);

            return res;
        }

        private  bool isValidAnagram(string s, string t)
        {
            bool result = true;
            if (s.Length != t.Length) return false;
            for (int i = 0; i < s.Length; i++)
            {
                int sCount = s.Where(c => c == s[i]).Count();
                int tCount = t.Where(c => c == s[i]).Count();

                if (sCount != tCount) return false;
                t = t.Replace(s[i].ToString(), string.Empty);
                s = s.Replace(s[i].ToString(), string.Empty);
            }

            return result;

        }

    }
}
