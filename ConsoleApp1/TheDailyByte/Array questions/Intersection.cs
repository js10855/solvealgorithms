﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    class Intersection
    {
        /*Given two integer arrays, return their intersection.
        Note: the intersection is the set of elements that are common to both arrays.

        Ex: Given the following arrays...

        nums1 = [2, 4, 4, 2], nums2 = [2, 4], return [2, 4]
        nums1 = [1, 2, 3, 3], nums2 = [3, 3], return [3]
        nums1 = [2, 4, 6, 8], nums2 = [1, 3, 5, 7], return []*/

        public int[] solution(int[] nums1, int[] nums2)
        {
            List<int> result = new List<int>();
            for (int i = 0; i < nums1.Length; i++)
            {
                if (nums2.Contains(nums1[i]))
                {
                   if (!result.Contains(nums1[i])) result.Add(nums1[i]);
                }
            }
            return result.ToArray();
               
            //}
        }
    }
}
