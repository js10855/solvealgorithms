﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{ 
    /*
     Given an array of numbers, move all zeroes in the array to the end while 
    maintaining the relative order of the other numbers.
    Note: You must modify the array you’re given (i.e. you cannot create a new array).

    Ex: Given the following array nums…

    nums = [3, 7, 0, 5, 0, 2], rearrange nums to look like the following [3,7,5,2,0,0]
     */
    class RearrangeElements
    {
        public void MoveZeroes(int[] nums)
        {            
            int left = 0;
            int right = 0;
            while (right < nums.Length)
            {
                if (nums[right] == 0) right++;
                else
                {
                    int temp = nums[left];
                    nums[left] = nums[right];
                    nums[right] = temp;
                    left++;
                    right++;
                }
            }
        }
    }
}
