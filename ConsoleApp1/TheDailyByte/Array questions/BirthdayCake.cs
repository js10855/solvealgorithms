﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.Array_questions
{
    /*
    You are at a birthday party and are asked to distribute cake to your guests. 
    Each guess is only satisfied if the size of the piece of cake they’re given, 
    matches their appetite (i.e. is greater than or equal to their appetite). 
    Given two arrays, appetite and cake where the ithelement of appetite represents the ith guest’s appetite, 
    and the elements of cake represents the sizes of cake you have to distribute, 
    return the maximum number of guests that you can satisfy.

    Ex: Given the following arrays appetite and cake…

    appetite = [1, 2, 3], cake = [1, 2, 3], return 3.
    Ex: Given the following arrays appetite and cake…

    appetite = [3, 4, 5], cake = [2], return 0.
     */
    internal class BirthdayCake
    {
        public int maxGuest(int[] appetite, int[] cakes)
        {
            int guestSatisfied = 0;
            for (int i = 0; i < cakes.Length; i++)
            {
                if (cakes[i] >= appetite[i]) guestSatisfied++;
            }
            return guestSatisfied;
        }
    }
}
