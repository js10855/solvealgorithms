﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
      Given a staircase where the ith step has a non-negative cost associated with it given by cost[i], 
      return the minimum cost of climbing to the top of the staircase. 
      You may climb one or two steps at a time and you may start climbing from either the first or second step.

        Ex: Given the following cost array…

        cost = [5, 10, 20], return 10.

        Ex: Given the following cost array…

        cost = [1, 5, 10, 3, 7, 2], return 10.

     */
    class Stairmaster
    {
        public int MinCostClimbingStairs(int[] cost)
        {
            //List<int> costList = cost.ToList();
            //costList.Add(0);
            //for (int i = costList.Count-3; i >-1; i--)
            //{
            //    costList[i] = Math.Min(costList[i + 1], costList[i + 2]);
            //}

            //return Math.Min(costList[0], costList[1]);


            int p1 = cost[0];
            int p2 = cost[1];
            int temp;
            for (int i = 2; i < cost.Length; i++)
            {
                temp = p2;
                p2 = cost[i] + Math.Min(p1, p2);
                p1 = temp;
            }
            return Math.Min(p1, p2);
        }
    }
}
