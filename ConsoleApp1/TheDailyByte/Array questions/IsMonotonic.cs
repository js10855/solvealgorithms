﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.Array_questions
{
    /*
     Given an array nums, return whether or not its values are monotonically increasing or 
    monotonically decreasing.
    Note: An array is monotonically increasing if for all values i <= j, nums[i] <= nums[j]. 
    Similarly an array is monotonically decreasing if for all values i <= j, nums[i] >= nums[j].

    Ex: Given the following array nums…

    nums = [1, 2, 3, 4, 4, 5], return true.
    Ex: Given the following array nums…

    nums = [7, 6, 3], return true.
    Ex: Given the following array nums…

    nums = [8, 4, 6], return false.
     */
    internal class IsMonotonicClass
    {
        public bool IsMonotonic(int[] nums)
        {
            bool increasing = true;
            bool decreasing = true;

            for (int i = 0; i < nums.Length-1; i++)
            {
                if (nums[i] > nums[i + 1]) increasing = false;
                if (nums[i] < nums[i + 1]) decreasing = false;
            }

            return increasing || decreasing;
            
            #region below solution works but lengthy...
            //int inc = 0;
            //if (nums.Length == 1) return true;

            //if (nums[1] > nums[0]) inc = 1;
            //else if (nums[1] < nums[0]) inc = 2;
            //else inc = 3; //equal


            //for (int i = 2; i < nums.Length; i++)
            //{
            //    if (inc == 1)
            //    {
            //        if (nums[i] < nums[i - 1]) return false;
            //    }
            //    else if (inc == 2)
            //    {
            //        if (nums[i] > nums[i - 1]) return false;
            //    }
            //    else
            //    {
            //        if (nums[i] > nums[i - 1]) inc = 1;
            //        else if (nums[i] < nums[i - 1]) inc = 2;
            //        else inc = 3;
            //    }
            //}
            //return true;
            #endregion
        }
    }
}
