﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    internal class Products
    {
        /*
         Given an integer array nums, return an array where each element i represents the 
        product of all values in nums excluding nums[i].
        Note: You may assume the product of all numbers within nums can safely fit within an integer range.

        Ex: Given the following array nums…

        nums = [1, 2, 3], return [6,3,2].
        6 = 3 * 2 (we exclude 1)
        3 = 3 * 1 (we exclude 2)
        2 = 2 * 1 (we exclude 3)
         */
        public int[] ProductExceptSelf(int[] nums)
        {
            int[] result = new int[nums.Length]; //Enumerable.Repeat(1, nums.Length).ToArray();
            int prefix = 1, postfix=1;

            for (int i = 0; i < nums.Length; i++)
            {
                result[i] = prefix;
                prefix *= nums[i];
            }

            for (int i = nums.Length-1; i >= 0; i--)
            {
                result[i] *= postfix;
                postfix *= nums[i];
            }
            return result;


            //below solution was exceeding time limit as it is O(n *n)
            //List<int> result = new List<int>();
            //for (int i = 0; i < nums.Length; i++)
            //{
            //    for (int j = 0; j < nums.Length; j++)
            //    {
            //        if (i!=j) result.Add(nums[i] * nums[j]);
            //    }
            //}


            //below does not work when having one or more zeroes
            //var count = nums.Where(x => x == 0).Count();
            //int product = 0;
            //product = nums.Aggregate((acc, val) => acc * val);

            //foreach (var num in nums)
            //{
            //    if (count == 1)
            //    {

            //    }
            //    else if (count <= 0)
            //    {

            //    }
            //    else //count>1
            //    {

            //    }
            //}




            //var count = nums.Where(x => x != 0).Count();
            //int product = 0;

            //if (count > 0)
            //{
            //    product = nums.Where(x => x != 0).Aggregate((acc, val) => acc * val);
            //}
            //else product = 0;


            //bool hasZero = false;
            //if (nums.Contains(0)) hasZero = true;

            //foreach (var num in nums)
            //{
            //    if (hasZero)
            //    {
            //        if (num == 0) result.Add(product);
            //        else result.Add(0);
            //    }
            //    else result.Add(product / num);

            //}
            return result.ToArray();
        }
    }
}
