﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
     Given an array, nums, and an integer k, return whether or not two unique indices exist 
    such that nums[i] = nums[j] and the two indices i and jj are at most k elements apart. 
    Ex: Given the following array nums and value k…

    nums = [1, 2, 1], k = 1, return false.
    Ex: Given the following array nums and value k…

    nums = [2, 3, 2], k = 2, return true.
         */
    internal class IdenticalElements
    {
        public bool ContainsNearbyDuplicate(int[] nums, int k)
        {
            var dict = new Dictionary<int, int>();

            for (var i = 0; i < nums.Length; i++)
            {
                if (dict.ContainsKey(nums[i]) && i - dict[nums[i]] <= k) return true;

                dict[nums[i]] = i;
            }

            return false;

            //Dictionary<int, int> dict = new Dictionary<int, int>();

            //for (int i = 0; i < nums.Length; i++)
            //{
            //    // for first occurance
            //    if (!dict.ContainsKey(nums[i]))
            //        dict.Add(nums[i], i);
            //    else
            //    {
            //        // already present in dict
            //        // check for at the most k
            //        if ((i - dict[nums[i]]) <= k)
            //            return true;
            //        // update with new position
            //        dict[nums[i]] = i;
            //    }
            //}

            //return false;
        }
    }
}
