﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
     Given an integer array, two players take turns picking the largest number from the ends of the array. 
     First, player one picks a number (either the left end or right end of the array) followed by player two. 
     Each time a player picks a particular numbers, it is no longer available to the other player. 
     This picking continues until all numbers in the array have been chosen. Once all numbers have been picked, 
     the player with the larger score wins. Return whether or not player one will win.
Note: You may assume that each player is playing to win (i.e. both players will always choose the maximum of the two numbers each turn) 
and that there will always be a winner.

Ex: Given the following integer array...

nums = [1, 2, 3], return true
Player one takes 3
Player two takes 2
Player one takes 1
3 + 1 > 2 and therefore player one wins

     */
    class WhoWins
    {
        public bool PredictTheWinner(int[] nums)
        {
            return winner(nums, 0, nums.Length - 1) >= 0;
        }

            public int winner(int[] nums, int start, int end)
            {
                if (start == end)
                    return nums[start];

                int a = nums[start] - winner(nums, start + 1, end);
                int b = nums[end] - winner(nums, start, end - 1);

                return Math.Max(a, b);
            }

            //int p1Score = 0;
            //int p2Score = 0;
            //int pCounter = 1;

            //for (int i = 0; i < nums.Length; i++)
            //{
            //    for (int j = nums.Length-1; j >=0 ; j--)
            //    {
            //        if (pCounter ==1)
            //        {
            //            if (nums[i] >= nums[j])
            //            {
            //                p1Score += nums[i];
            //                nums = nums.Where((val, idx) => idx != i).ToArray();
            //            }
            //            else
            //            {
            //                p1Score += nums[j];
            //                nums = nums.Where((val, idx) => idx != j).ToArray();
            //            }
            //            pCounter++;
            //        }
            //        else
            //        {
            //            if (nums[i] >= nums[j])
            //            {
            //                p2Score += nums[i];
            //                nums = nums.Where((val, idx) => idx != i).ToArray();
            //            }
            //            else
            //            {
            //                p2Score += nums[j];
            //                nums = nums.Where((val, idx) => idx != j).ToArray();
            //            }
            //            pCounter--;
            //        }

            //    }
            //}
            //if (p1Score > p2Score) return true;
            //return false;
        }
    }


