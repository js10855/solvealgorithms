﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.Array_questions
{
    internal class FindtheNumber
    {
        /*
        Given an array of integers, nums, and a value k, return the kth largest element.

         Ex: Given the following array nums…


         [1, 2, 3], k = 1, return 3.
        Ex: Given the following array nums…


         [9, 2, 1, 7, 3, 2], k = 5, return 2.
        */
        public int FindKthLargest(int[] nums, int k)
        {
            nums = nums.OrderByDescending(x => x).ToArray();
            return nums[k-1];

            //Second solution
            //Array.Sort(nums);
            //return nums[nums.Length - k];
        }
    }
}
