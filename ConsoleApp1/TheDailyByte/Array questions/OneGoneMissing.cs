﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
        Given an array that contains all distinct values from zero through N except one number, return the number that is missing from the array.

        Ex: Given the following array nums…

        nums = [1, 4, 2, 0], return 3.
        3 is the only number missing in the array between 0 and 4.
        Ex: Given the following array nums…

        nums = [6, 3, 1, 2, 0, 5], return 4.
        4 is the only number missing in the array between 0 and 6.

    */
     
    class OneGoneMissing
    {
        public int MissingNumber(int[] nums)
        {
            Array.Sort(nums);
            int counter = 0;
            int result = 0;
            if (nums[0] != 0) return 0;

            for (int i = 1; i < nums.Length; i++)
            {
                if (nums[i] != counter + 1)
                {
                    result = counter +1;
                    return result;
                }
                counter++;
            }
            if (result == 0) result = nums.Length;
            return result;
        }
    }
}
