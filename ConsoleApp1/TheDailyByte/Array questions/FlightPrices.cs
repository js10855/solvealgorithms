﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{

    /*
     A company is booking flights to send its employees to its two satellite offices A and B. 
     The cost of sending the ith employee to office A and office B is given by prices[i][0] and prices[i][1] respectively. 
     Given that half the employees must be sent to office A and half the employees must be sent to office B, 
     return the minimum cost the company must pay for all their employees’ flights.

    Ex: Give the following prices…

    prices = [[40,30],[300,200],[50,50],[30,60]], return 310
    Fly the first personn to office B.
    Fly the second person to office B.
    Fly the third person to office A.
    Fly the fourth person to office A.
     */
    class FlightPrices
    {
        public int TwoCitySchedCost(int[][] costs)
        {
            if (costs == null || costs.Length == 0 || costs[0].Length == 0 || costs[0].Length % 2 == 1)
                return 0;

            var costDiff = new List<Tuple<int, int>>();
            var rows = costs.Length;
            for (int row = 0; row < rows; row++)
            {
                costDiff.Add(new Tuple<int, int>(costs[row][0] - costs[row][1], row));
            }

            costDiff.Sort();

            var totalCost = 0;
            for (int row = 0; row < rows; row++)
            {
                var origIndex = costDiff[row].Item2;
                if (row < rows / 2)
                {
                    totalCost += costs[origIndex][0];
                }
                else
                {
                    totalCost += costs[origIndex][1];
                }
            }

            return totalCost;

            //int minCost = 0;
            //int offA = 0;
            //int offB = 0;
            //// costs = costs.OrderBy((x,y) => x[1]-x[0]>y[1]-y[0]).ToArray();
            ////Array.Sort((x, y) => x[1] - x[0] > y[1] - y[0]);


            //for (int i = 0; i < costs.Length; i++)
            //{               
            //    if (costs[i][0] >= costs[i][1] && (offB < costs.Length / 2))
            //    {
            //        offB++;
            //        minCost += costs[i][1];
            //    }
            //    else if ((costs[i][0] <= costs[i][1] && offA < costs.Length / 2))
            //    {
            //        minCost += costs[i][0];
            //        offA++;
            //    }             
            //}
            //return minCost;
        }
    }
}
