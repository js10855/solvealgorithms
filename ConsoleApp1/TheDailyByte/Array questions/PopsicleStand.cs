﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
    You’re running a popsicle stand where each popsicle costs $5. 
    Each customer you encountered pays with either a $5 bill, a $10 bill, or a $20 bill and only buys a single popsicle. 
    the customers that come to your stand come in the ordered given by the customers array 
    where customers[i] represents the bill the ith customer pays with. Starting with $0, 
    return whether or not you can serve all the given customers while also giving the correct amount of change.

    Ex: Given the following customers…

    customers = [5, 10], return true
    collect $5 from the first customer, pay no change.
    collet $10 from the second customer and give back $5 change.

    Ex: Given the following customers…

    customers = [10], return false
    collect $10 from the first customer and we cannot give back change.

    Ex: Given the following customers…

    customers = [5,5,5,10,20], return true
    collect $5 from the first 3 customers.
    collet $10 from the fourth customer and give back $5 change.
    collect $20 from the fifth customer and give back $10 change ($10 bill and $5 bill).

    */
    class PopsicleStand
    {
        public bool LemonadeChange(int[] bills)
        {
            //[5,5,5,5,20,20,5,5,5,5]
            int fives = 0;
            int tens = 0;
            for (int i = 0; i < bills.Length; i++)
            {
                if (bills[i] == 5)
                    fives++;
                else if (bills[i] == 10)
                {
                    if (fives == 0) return false;
                    fives--;
                    tens++;
                }
                else if (tens > 0 & fives > 0)
                {
                    tens--;
                    fives--;
                }
                else if (fives >= 3) fives -= 3;
                else return false;
            }

                //if (fives < 0) return false;
                return true;


            //    switch (bills[i])
            //    {
            //        case 5:
            //            {
            //                fives += 1;
            //                break;
            //            }
            //        case 10:
            //            {
            //                if (fives >= 1)
            //                {
            //                    fives -= 1;
            //                    tens += 1;
            //                }
            //                else return false;
            //                break;
            //            }
            //        case 20:
            //            {
            //                if (tens >= 1)
            //                {
            //                    fives -= 1;
            //                    tens -= 1;
            //                }
            //                else fives -= 3;
            //                //else return false;
            //                break;
            //            }                        
            //    }
            //}
            //if (fives<0) return false;
            //return true;
        }
    }
}
