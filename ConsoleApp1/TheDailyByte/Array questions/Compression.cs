﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
     Given a character array, compress it in place and return the new length of the array.
    Note: You should only compress the array if its compressed form will be at least as short as the length of its original form.

    Ex: Given the following character array chars…

    chars = ['a', 'a', 'a', 'a', 'a', 'a'], return 2.
    chars should be compressed to look like the following:
    chars = ['a', '6']

    Ex: Given the following character array chars…

    chars = ['a', 'a', 'b', 'b', 'c', 'c'], return 6.
    chars should be compressed to look like the following:
    chars = ['a', '2', 'b', '2', 'c', '2']

    Ex: Given the following character array chars…

    chars = ['a', 'b', 'c'], return 3.
    In this case we chose not to compress chars.

     */
    class Compression
    {
        public int Compress(char[] chars)
        {
            int cnt = 0;
            var result = chars.GroupBy(x => x).Select(x=>x.ToList().Count);

            foreach (var item in result)
            {
                if (item > 1) cnt = cnt + 2;
                else cnt++;
            }            

            Dictionary<char, int> myDictionary = chars.GroupBy(o => o).ToDictionary(g => g.Key, g => g.ToList().Count);

            int counter = 0;
            foreach (var item in myDictionary)
            {
                chars[counter++] = item.Key;
                chars[counter++] = Convert.ToChar(item.Value);
            }

            return cnt;

            //return charsCount;

            //int result = 0;
            //foreach (var item in charEnu)
            //{
            //    result += Convert.ToInt32(item);
            //}
            //return result;

            //if (chars.Length == 1) return 1;
            //int i = 0, CountIndex = 0;
            //while (i < chars.Length)
            //{
            //    int j = i;
            //    while (j < chars.Length && chars[j] == chars[i])
            //    {
            //        j++;
            //    }

            //    chars[CountIndex++] = chars[i];
            //    if (j - i > 1)
            //    {
            //        string count = j - i + "";
            //        foreach (char ch in count.ToCharArray())
            //        {
            //            chars[CountIndex++] = ch;
            //        }
            //    }

            //    i = j;

            //}
            //return CountIndex;



            //int result = 0;         
            //Dictionary<char, int> charDict = new Dictionary<char, int>();
            //if (chars.Length == 1) return 1;

            //foreach (char c in chars)
            //{                
            //    charDict.Add(c,c - 'a');
            //}

            //List<string> charList = new List<string>();

            //foreach (var item in charDict)
            //{
            //    if (item.Value > 0)
            //    {
            //        result += 2;
            //        charList.Add(item.Key.ToString());
            //        charList.Add(item.Value.ToString());
            //    }
            //}

            //chars = string.Join(string.Empty, charList).ToCharArray();

            //return result;
        }
    }
}
