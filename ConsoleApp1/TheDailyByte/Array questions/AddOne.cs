﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
     Given an array digits that represents a non-negative integer, 
    add one to the number and return the result as an array.

    Ex: Given the following digits...

    digits = [1, 2], return [1, 3].
    Ex: Given the following digits...

    digits = [9, 9], return [1, 0, 0].
    https://leetcode.com/problems/plus-one/
     */
    internal class AddOne
    {
        public int[] PlusOne(int[] digits)
        {
            //var value = string.Join("", digits);
            //Int64 num = Int64.Parse(value);
            //num++;           
            //return num.ToString().Select(o => Convert.ToInt32(o) - 48).ToArray();
            int size = digits.Length - 1;

            for (int i = size; i > -1; i--)
            {
                if (digits[i] < 9)
                {
                    digits[i]++;

                    return digits;
                }
                else
                {

                    digits[i] = 0;
                }

            }

            int[] result = new int[size + 2];
            result[0] = 1;
            return result;

        }
    }
}
