﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
    You are transporting bricks on a construction site and want to work as efficiently as possible.
    The weight of each brick is given by bricks[i]. Given a wheelbarrow that can carry up to(not including) 5000 pounds,
    return then maximum number of bricks you can place in your wheelbarrow to transport.

    Ex: Given the following bricks…

    bricks = [1000, 1000, 1000, 2000], return 3.

    Ex: Given the following bricks…

    bricks = [1000, 200, 150, 200], return 4.
    */
    class MovingBricks
    {
        public int findNumberOfBricks(int[] bricks)
        {
            Array.Sort(bricks);
            int brickCounter = 0;
            int totalWeight = 0;
            int maxCapacity = 5000;
            for (int i = bricks.Length-1; i >=0 ; i--)
            {
                if (bricks[i] + totalWeight < maxCapacity)
                {
                    totalWeight += bricks[i];
                    brickCounter++;
                }
                else break;
            }
            return brickCounter;
        }
    }
}
