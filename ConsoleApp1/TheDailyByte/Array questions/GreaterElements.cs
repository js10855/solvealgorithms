﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    public class GreaterElements
    {
        /*
        Given two arrays of numbers, where the first array is a subset of the second array, 
        return an array containing all the next greater elements for each element in the first array, in the second array.
        If there is no greater element for any element, output -1 for that number.

        Ex: Given the following arrays…

        nums1 = [4, 1, 2], nums2 = [1, 3, 4, 2], return [-1, 3, -1] because no element in nums2 is greater than 4, 3 is the first number in nums2 greater than 1, and no element in nums2 is greater than 2.
        nums1 = [2, 4], nums2 = [1, 2, 3, 4], return [3, -1] because 3 is the first greater element that occurs in nums2 after 2 and no element is greater than 4.
*/

        public static int[] NextGreaterElement(int[] nums1, int[] nums2)
        {
            int[] result = new int[nums1.Length];
            for (int i = 0; i < nums1.Length; i++)
            {
                int index = Array.IndexOf(nums2, nums1[i]);
                for (int j = index+1; j < nums2.Length; j++)
                {
                    if (nums2[j] > nums1[i])
                    {
                        result[i] = nums2[j];
                        break;
                    }                    
                }
                if (result[i]==0) result[i] = -1;               
            }
            return result;
        }
    }
}
