﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.Array_questions
{
    class BestTimeToBuyAndSell
    {
        /*
        Given an array of integers where the ith integer represents the price of the stock on day i, return the largest possible profit from completing one transaction (i.e. buying 1 share and selling 1 share).
        Examples: Given the following prices...

        // Buy on day 1 and sell on day 5 for a profit of 5 - 1 = 4. 
        prices = [1, 2, 3, 4, 5], return 4. 
        // Buy on day 4 and sell on day 9 for a profit of 11 - 1 = 10. 
        prices = [4, 5, 2, 1, 6, 10, 4, 9, 11], return 10. 
        // Buying and selling the stock at any point would yield a negative profit. 
        prices = [33, 18, 8, 2], return 0 

        */

        public static int solution(int[] s)
        {
            int[] prices = { 33, 18, 8, 2 };
            int min = int.MaxValue;
            int profit = 0;

            for (int i = 0; i < prices.Length; i++)
            {
                min = Math.Min(min, prices[i]);
                if (prices[i] > min)
                {
                    profit = Math.Max(profit, prices[i] - min);
                }
                else { min = prices[i]; }
            }
            return profit;
        }
    }
}
