﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.Array_questions
{
    class MaximumSubarray
    {
        /*
         Given an integer array, return the sum of its contiguous subarray that produces the largest value.
Note: Your subarray must contain at least one value.

Ex: Given the following integer arrays…

nums = [-3,8,-8,2], return 8 (8)
nums = [2, 3,-4, 2], return 5 (2 + 3)
nums = [1, 5,-2, -3, 7], return 8 (1 + 5 + (-2) + (-3) + 7)

         */
        public int MaxSubArray(int[] nums)
        {
            //int maxSubTotal = nums[0];
            //int currentSum = maxSubTotal;
            //for (int i = 1; i < nums.Length; i++)
            //{
            //    if (currentSum < 0) currentSum = 0;
            //    currentSum = Math.Max(nums[i], nums[i] + currentSum);
            //    maxSubTotal = Math.Max(maxSubTotal, currentSum);
            //}

            //return maxSubTotal;

            int maxSubTotal = nums[0];
            int currentSum = 0;

            foreach (var num in nums)
            {

                if (currentSum < 0) currentSum = 0;
                currentSum += num;
                maxSubTotal = Math.Max(maxSubTotal, currentSum);
            }            

            return maxSubTotal;
        }
    }
}
