﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
     Given a staircase with N steps and the ability to climb either one or two steps at a time, 
     return the total number of ways to arrive at the top of the staircase.

    Ex: Given the following value of N…
    N = 2, return 2
    1 step + 1 step
    2 steps

    Ex: Given the following value of N…
    N = 3, return 3
    1 step + 1 step + 1 step
    1 step + 2 steps
    2 steps + 1 step */
    class Stairs
    {
        public int ClimbStairs(int n)
        {
            int[] dp = new int[n + 1];
            dp[0] = 1;
            dp[1] = 1;
            for (int i = 2; i <= n; i++)
            {
                dp[i] = dp[i - 1] + dp[i - 2];
            }
            return dp[n];
        }
    }
}
