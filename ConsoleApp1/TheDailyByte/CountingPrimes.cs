﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    class CountingPrimes
    {
        /*
         Given a positive integer N, return the number of prime numbers less than N.

        Ex: Given the following N…

        N = 3, return 1.
        2 is the only prime number less than 3.
        Ex: Given the following N…

        N = 7, return 3.
        2, 3, and 5 are the only prime numbers less than 7.

        */

        public int CountPrimes(int n)
        {
            bool[] primes = new bool[n];
            for (int i = 2; i * i < primes.Length; i++)
            {
                if (!primes[i])
                {
                    for (int j = i; j * i < primes.Length; j++)
                    {
                        primes[i * j] = true;
                    }
                }
            }

            int primeCount = 0;
            for (int i = 2; i < primes.Length; i++)
            {
                if (!primes[i]) primeCount++;
            }
            return primeCount;
        }

        #region mysolution //- works but getting time out    
        //public int CountPrimes(int n)
        //{
        //    int counter = 0;
        //    for (int i = 0; i < n; i++)
        //    {
        //        if (isPrime(i)) counter++;
        //    }
        //    return counter;
        //}

        //private bool isPrime(int n)
        //{
        //    bool result = true;
        //    if (n == 1 || n == 0) return false;
        //    else if (n == 2) return true;
        //    else
        //    {
        //        var limit = Math.Ceiling(Math.Sqrt(n));
        //        for (int i = 2; i <= limit; i++)
        //        {
        //            if (n % i == 0) { result = false; break; }
        //        }               
        //    }
        //    return result;
        //}
        #endregion

    }
}
