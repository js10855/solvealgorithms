﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    class ReapingChildren
    {
        /*
        You are given two lists of integers and an integer representing a process id to kill. 
        One of the lists represents a list of process ids and the other represents a list of each of the processes’ corresponding (by index) parent ids. 
        When a process is killed, all of its children should also be killed. Return a list of all the process ids that are killed as a result of killing the requested process.

        Ex: Given the following…

        pid =  [2, 4, 3, 7]
        ppid = [0, 2, 2, 3]
        kill = 3
        the tree of processes can be represented as follows:
                   2
                 /   \
                4     3
                     /
                    7
        return [3, 7] as killing process 3 will also require killing its child (i.e. process 7). 
         */
        public int[] GetProcessIds(List<int> pid, List<int> ppid, int kill)
        {      
            List<int> result = new List<int>();
            return dfs(pid, ppid, kill, result);
        }

        public int[] dfs(List<int> pid, List<int> ppid, int id,List<int> result)
        {
            result.Add(id);
            for (int i = 0; i < ppid.Count; i++)
            {
                if (ppid[i] == id)
                {
                    if (!result.Contains(ppid[i])) result.Add(ppid[i]);
                    result.Add(pid[i]);
                    dfs(pid, ppid, pid[i],result);
                }
            }
            return result.ToArray();
        }
     }
}
