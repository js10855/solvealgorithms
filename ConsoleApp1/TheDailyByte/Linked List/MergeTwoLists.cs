﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.MergeTwoLists
{
    class MergeTwoLists
    {
        /*
         Given two sorted linked lists, merge them together in ascending order and return a reference to the merged list

        Ex: Given the following lists...

        list1 = 1->2->3, list2 = 4->5->6->null, return 1->2->3->4->5->6->null
        list1 = 1->3->5, list2 = 2->4->6->null, return 1->2->3->4->5->6->null
        list1 = 4->4->7, list2 = 1->5->6->null, return 1->4->4->5->6->7->null          
          */


        public ListNode Solution(ListNode list1, ListNode list2)
        {
            ListNode temp = new ListNode(-1);
            ListNode current = temp;

            while (list1 != null && list2 != null)
            {
                if (list1.val < list2.val)
                {
                    temp.next = list1;
                    list1 = list1.next;
                }
                else
                {
                    temp.next = list2;
                    list2 = list2.next;
                }
                temp = temp.next;
            }

            if (list1 != null) temp.next = list1;
            else temp.next = list2;

            return current.next;
        }
    }

    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }

    //public class SingleLinkedList
    //{
    //    internal ListNode head;
    //}
}
