﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.Linked_List
{
    class ContainsCycle
    {
        /*
        Given a linked list, containing unique numbers, return whether or not it has a cycle.
        Note: a cycle is a circular arrangement (i.e. one node points back to a previous node)

        Ex: Given the following linked lists...

        1->2->3->1 -> true (3 points back to 1)
        1->2->3 -> false
        1->1 true (1 points to itself)

        */
        public bool HasCycle(ListNode head)
        {
            if (head == null) return false;
            ListNode slow = head;
            ListNode fast = head.next;

            while (slow!= fast)
            {
                if (fast == null || fast.next == null) return false;
                slow = slow.next;
                fast = fast.next.next;
            }
            return true;
        }
    }
}
