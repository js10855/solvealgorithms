﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.Linked_List
{
    /* Given a potentially cyclical linked list where each value is unique, return the node at which the cycle starts.If the list does not contain a cycle, return null.

 Ex: Given the following linked lists...

 1->2->3, return null
 1->2->3->4->5->2 (5 points back to 2), return a reference to the node containing 2
 1->9->3->7->7 (7 points to itself), return a reference to the node containing 7
 */
    class ReturnStartofCycle
    {
        public ListNode Intersection(ListNode head)
        {

            ListNode slow = head;
            ListNode fast = head;

            while (fast != null && fast.next != null)
            {
                slow = slow.next;
                fast = fast.next.next;
                if (slow == fast) return slow;
            }
            return null;
        }
        public ListNode DetectCycle(ListNode head)
        {
            if (head == null || head.next == null) return null;
            ListNode intersect = Intersection(head);
            if (intersect == null) return null;
            ListNode start = head;
            while (intersect != start)
            {
                start = start.next;
                intersect = intersect.next;
            }
            return start;
        }

       
    }
}
