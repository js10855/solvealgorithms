﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    class RemoveNthToLastNode
    {
        /*
         Given a linked list and a value n, remove the nth to last node and return the resulting list.

        Ex: Given the following linked lists...

        1->2->3->null, n = 1, return 1->2->null
        1->2->3->null, n = 2, return 1->3->null
        1->2->3->null, n = 3, return 2->3->null

        */

        public static ListNode RemoveNthFromEnd(ListNode head, int n)
        {
            ListNode dummy = new ListNode(0,head);
            var left = dummy;
            var right = head;

            while (n>0 && right!=null)
            {
                right = right.next;
                n -= 1;
            }

            while (right != null)
            {
                right = right.next;
                left = left.next;
            }

            //deleting the node
            left.next = left.next.next;

            return dummy.next;
        }


    }


    public class ListNode
    {
      public int val;
      public ListNode next;
      public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }

        public void AddToEnd(int data)
        {
            if (next == null)
            {
                next = new ListNode(data);
            }
            else
            {
                next.AddToEnd(data);
            }
        }

        public void Print()
        {
            Console.Write("|" + val + "|=>");
            if (next != null)
            {
                next.Print();
            }
        }
    }


}
