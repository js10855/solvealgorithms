﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    class FindMiddleElement
    {
        /*
        Given a non-empty linked list, return the middle node of the list.If the linked list contains an even number of elements, return the node closer to the end.
        Ex: Given the following linked lists...

        1->2->3->null, return 2
        1->2->3->4->null, return 3
        1->null, return 1
      */
        public ListNode MiddleNode(ListNode head)
        {
            ListNode middle = new ListNode();
            ListNode dummy = head;
            int middleCounter = 1;

            while (dummy != null)
            {
                dummy = dummy.next;
                middleCounter++;
            }

            middleCounter = (int) Math.Ceiling((double) middleCounter / 2);
            int counter = 1;

            while (head != null)
            {
                if (middleCounter == counter)
                {
                    middle = head;
                    break;
                }
                else
                {
                    head = head.next;
                    counter++;
                }
                
            }

            return middle;
        }
    }
}
