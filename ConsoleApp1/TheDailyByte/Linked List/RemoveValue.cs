﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    class RemoveValue
    {
        /*
        Given a linked list and a value, remove all nodes containing the provided value, and return the resulting list.

        Ex: Given the following linked lists and values...

        1->2->3->null, value = 3, return 1->2->null
        8->1->1->4->12->null, value = 1, return 8->4->12->null
        7->12->2->9->null, value = 7, return 12->2->9->null
         
        */
        public ListNode RemoveElements(ListNode head, int val)
        {           
            ListNode dummy = new ListNode(0, head);
            var left = dummy;
            var right = head;

            if (head == null) return head;

            while (right!=null)
            {
                if (right.val == val)
                {
                    left.next = left.next.next;
                }
                else
                {
                    left = left.next;
                }
                right = right.next;                
            }

            return dummy.next;
        }
    }
}
