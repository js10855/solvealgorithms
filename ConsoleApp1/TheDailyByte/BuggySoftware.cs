﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
     Releasing software can be tricky and sometimes we release new versions of our software with bugs. When we release a version with a bug it’s referred to as a bad release. Your product manager has just informed you that a bug you created was released in one of the past versions and has caused all versions that have been released since to also be bad. Given that your past releases are numbered from zero to N and you have a helper function isBadRelease(int releaseNumber) that takes a version number and returns a boolean as to whether or not the given release number is bad, return the release number that your bug was initially shipped in.
     Note: You should minimize your number of calls made to isBadRelease().

     Ex: Given the following value N…

     N = 5 and release number four is the release your bug was shipped in...
     isBadRelease(3) // returns false.
     isBadRelease(5) // returns true.
     isBadRelease(4) // returns true.

      return 4.

    */
    class BuggySoftware: VersionControl
    {
        public int FirstBadVersion(int n)
        {
        //    int result = n;
        //    while (n>0)
        //    {
        //        if (IsBadVersion(n)) result = n;
        //        else n--;
        //    }
        //    return result;
            int low = 0;
            int high = n - 1;
            int mid;

            while (low<=high)
            {
                mid = low + (high-low)/2;
                if(IsBadVersion(low+1)){
                    return (low+1);
                }
                if(IsBadVersion(mid+1)){
                    high = mid;
                }
                else{
                    low = mid + 1;
                }
            }

            return 0;
        }
    }

    public class VersionControl
    {
        public bool IsBadVersion(int version)
        {
            if (version > 3) return true;
            else return false;
        }
    }
}
