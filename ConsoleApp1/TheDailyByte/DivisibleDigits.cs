﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
        Given an integer N, return the total number self divisible numbers 
       that are strictly less than N (starting from one).
       Note: A self divisible number if a number that is divisible by all of its digits.

       Ex: Given the following value of N…

       N = 17, return 12 because 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 15 are all self divisible numbers.
     */
    internal class DivisibleDigits
    {
        public int CheckDigits(int N)
        {
            if (N == 0) return 0;
            if (N == 1) return 1;
            int result = 0;
            for (int i = 1; i < N; i++)
            {
                if (isSelfDivisiable(i)) result++ ;
            }
            return result;
            
        }

        private bool isSelfDivisiable(int num)
        {
            foreach (var val in num.ToString())
            {
                if (num % val-'0' !=0) return false;
            }
            return true;
        }
    }

    /*
     A self-dividing number is a number that is divisible by every digit it contains.

    For example, 128 is a self-dividing number because 128 % 1 == 0, 128 % 2 == 0, and 128 % 8 == 0.
    A self-dividing number is not allowed to contain the digit zero.

    Given two integers left and right, return a list of all the self-dividing numbers 
    in the range [left, right].
     */
    public class Solution
    {
        public IList<int> SelfDividingNumbers(int left, int right)
        {
            List<int> result = new List<int>();
            if (left == 0 || right == 0)
            {
                result.Add(0);
                return result;
            }

            if (right == 1)
            {
                result.Add(1);
                return result;
            }

            for (int i = left; i <= right; i++)
            {
                if (isSelfDivisiable(i)) result.Add(i);
            }
            return result;
        }

        private bool isSelfDivisiable(int num)
        {            
            foreach (var val in num.ToString())
            {                
                int value = val - '0';
                if (value > 0)
                {
                    if (Convert.ToInt32(num) % Convert.ToInt32(value) != 0) return false;
                }
                else return false;


            }            
            return true;
        }
    }
}
