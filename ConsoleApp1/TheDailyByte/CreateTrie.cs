﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
    Implement a trie class that supports insertion and search functionalities.
Note: You may assume only lowercase alphabetical characters will added to your trie.

Ex: Given the following operations on your trie…

Trie trie = new Trie()
trie.insert("programming");
trie.search("computer") // returns false.
trie.search("programming") // returns true.

    */
    class Trie
    {
        List<string> strText;
        public Trie()
        {
            strText = new List<string>();
        }

        public void Insert(string word)
        {
            strText.Add(word);
        }

        public bool Search(string word)
        {
            if (strText.Exists(x=> x == word)) return true;
            else return false;
        }

        public bool StartsWith(string prefix)
        {
            // bool has = strText.Any(cus => strText.Contains(cus));
            return strText.Where(x => x.StartsWith(prefix)).Count()>0;
           
        }
    }
}
