﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{ 
   public class GoldRush
    {
        /*
        Given a 2D matrix that represents a gold mine, where each cell’s value represents an amount of gold, return the maximum amount of gold you can collect given the following rules:

        You may start and stop collecting gold from any position
        You can never visit a cell that contains 0 gold
        You cannot visit the same cell more than once
        From the current cell, you may walk one cell to the left, right, up, or down
        Ex: Given the following gold mine…

        goldMine = [
            [0,2,0],
            [8,6,3],
            [0,9,0]
        ],
        return 23 (start at 9 and then move to 6 and 8 respectively) 
         */
        public int GetMaximumGold(int[][] grid)
        {
            int max = 0;
            for (int i = 0; i < grid.Length; i++)
            {
                for (int j = 0; j < grid[0].Length; j++)
                {
                    if (grid[i][j] >0)
                    { 
                       int temp= dfs(i, j, grid);
                       if (temp > max) max = temp;
                    }
                }
            }
            return max;
        }

        private int dfs(int i, int j, int[][] grid)
        {            
            if (i < 0 || i >= grid.Length || j < 0 || j >= grid[i].Length || grid[i][j]==0 ) return 0;

            int temp= grid[i][j];
            grid[i][j] = 0;

            int upSum = dfs(i + 1, j, grid);
            int downSum = dfs(i - 1, j, grid);
            int leftSum = dfs(i, j-1, grid);
            int rightSum = dfs(i, j+1, grid);

            grid[i][j] = temp;

           return temp + Math.Max(upSum, Math.Max(downSum, Math.Max(leftSum,rightSum)));
           
        }
    }
}
