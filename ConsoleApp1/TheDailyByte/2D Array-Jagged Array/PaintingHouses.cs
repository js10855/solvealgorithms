﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
     You are tasked with painting a row of houses in your neighborhood such that each house is painted either red, blue, or green. 
     The cost of painting the ith house red, blue or green, is given by costs[i][0], costs[i][1], and costs[i][2] respectively. 
     Given that you are required to paint each house and no two adjacent houses may be the same color, return the minimum cost to paint all the houses.

    Ex: Given the following costs array…

    costs = [[1, 3, 5],[2, 4, 6],[5, 4, 3]], return 8.
    Paint the first house red, paint the second house blue, and paint the third house green.

     */
    class PaintingHouses
    {
        public int minCost(int[][] costs)
        {
            if (costs == null || costs.Length == 0) return 0;
            for (int i = 1; i < costs.Length; i++)
            {
                costs[i][0] += Math.Min(costs[i - 1][1], costs[i - 1][2]);
                costs[i][1] += Math.Min(costs[i - 1][0], costs[i - 1][2]);
                costs[i][2] += Math.Min(costs[i - 1][0], costs[i - 1][1]);
            }

            return Math.Min(costs[costs.Length-1][0], Math.Min(costs[costs.Length - 1][1], costs[costs.Length - 1][2]));
        }
        
    }
}
