﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
     Given a list of positive numbers without duplicates and a target number, 
     find all unique combinations of the numbers that sum to the target. 
     Note: You may use the same number more than once.

Ex: Given the following numbers and target…

numbers = [2,4,6,3], target = 6,
return [
    [2,2,2],
    [2,4],
    [3,3],
    [6]
]
     */
    class UniqueCombinations
    {
        public IList<IList<int>> CombinationSum(int[] candidates, int target)
        {
            //List<IList<int>> result = new List<IList<int>>();
            ////backtrack(candidates,0, target,new List<int>(),result);
            //sum(0, candidates, target, new List<int>(), result);
            //return result;

            // The optimization relies on an array being sorted.
            Array.Sort(candidates);

            // current solution, current sum, and least number that can be used
            List<IList<int>> result = new List<IList<int>>();

            List<(List<int>, int, int)> myCandidates = new List<(List<int>, int, int)>();

            // initial values to start the search
            myCandidates.Add((new List<int>(), 0, Int32.MaxValue));

            while (myCandidates.Count > 0)
            {
                List<(List<int>, int, int)> newCandidates = new List<(List<int>, int, int)>();

                foreach (var (numbers, sum, smallest) in myCandidates)
                {
                    if (sum == target)
                    {
                        result.Add(new List<int>(numbers));
                    }
                    if (sum < target)
                    {
                        foreach (var element in candidates)
                        {
                            if (element + sum <= target)
                            {
                                if (smallest >= element)
                                {
                                    var temp = new List<int>(numbers);
                                    temp.Add(element);
                                    newCandidates.Add((temp, sum + element, element));
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }

                    myCandidates = newCandidates;
                }
            }

            return result;
        }

        //private void backtrack(int[] candidates, int start, int target, List<int> list,List<IList<int>> result)
        //{
        //    if (target < 0) return;
        //    if (target == 0)
        //    {
        //        result.Add(new List<int>(list));
        //        return;
        //    }
        //    for (int i = start; i < candidates.Length; i++)
        //    {
        //        list.Add(candidates[i]);
        //        backtrack(candidates,i, target-candidates[i],list, result);
        //        list.Remove(list.Count - 1);
        //    }
        //}

        //public void sum(int start, int[] candidates, int target, List<int> list, List<IList<int>> result)
        //{
        //    if (start == candidates.Length)
        //    {
        //        if (target == 0)
        //        {
        //            result.Add(new ArrayList<>(list));
        //        }
        //        return;
        //    }
        //    if (candidates[start] <= target)
        //    {
        //        list.Add(candidates[start]);
        //        sum(start, candidates, target - candidates[start], list, result);
        //        list.Remove(list.Count - 1);
        //    }
        //    sum(start + 1, candidates, target, list, result);
        //}

        public void sum(int indx, int[] arr, int target, List<int> list, List<IList<int>> ans)
        {
            if (indx == arr.Length)
            {
                if (target == 0)
                {
                    ans.Add(new List<int>(list));
                }
                return;
            }
            if (arr[indx] <= target)
            {
                list.Add(arr[indx]);
                sum(indx, arr, target - arr[indx], list, ans);
                list.Remove(list.Count - 1);
            }
            sum(indx + 1, arr, target, list, ans);
        }
    }
}
