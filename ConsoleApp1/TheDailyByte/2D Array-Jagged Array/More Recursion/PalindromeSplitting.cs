﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    class PalindromeSplitting
    {
        /*
        Given a string s, return all possible partitions of s such that each substring is a palindrome.

Ex: Given the following string s…

s = "abcba",
return [
    ["a","b","c","b","a"],
    ["a","bcb","a"],
    ["abcba"]
]
*/



        public IList<IList<string>> Partition(string s)
        {
            List<string> part = new List<string>();
            List<IList<string>> result = new List<IList<string>>();
            dfs(s, 0, result, part);
            return result;
        }

        private void dfs(string s, int start, List<IList<string>> result, List<string> list)
        {
            if (s.Length==start)
            {
                result.Add(new List<string>(list));
                return;
            }

            for (int i = start; i < s.Length; i++)
            {
                if (isPalindrome(s,start,i))
                {
                    list.Add(s.Substring(start, (i + 1)-start));
                    dfs(s, i + 1, result, list);
                    list.RemoveAt(list.Count - 1);
                }
            }
        }

        bool isPalindrome(string s,int start,int end)
        {
            while(start < end)
            {
                if (s[start] != s[end]) return false;
                start++;
                end--;
            }
            return true;
        }

    }
}
