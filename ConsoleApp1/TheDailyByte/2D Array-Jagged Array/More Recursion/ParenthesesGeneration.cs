﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
    Given an integer N, where N represents the number of pairs of parentheses (i.e. ”(“ and ”)”) you are given, return a list containing all possible well-formed parentheses you can create.

    Ex: Given the following value of N…

        N = 3, 
        return [  
            "((()))",
          "(()())",
          "(())()",
          "()(())",
          "()()()"
        ]

    */
    class ParenthesesGeneration
    {
        public IList<string> GenerateParenthesis(int n)
        {
            List<string> result = new List<string>();
            backtrack(result,"",0,0,n);
            return result;
        }

        private void backtrack(List<string> result, string currentString, int open, int close, int n)
        {
            if (currentString.Length == n * 2)
            {
                result.Add(currentString);
                return;
            }

            if (open < n) backtrack(result, currentString + "(", open + 1, close, n);
            if (close < open) backtrack(result, currentString +  ")", open, close + 1, n);
        }

    }
    
}

