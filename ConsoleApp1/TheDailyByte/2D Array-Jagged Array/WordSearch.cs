﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
        Given a 2D board that represents a word search puzzle and a string word, return whether or the given word can be formed in the puzzle by only connecting cells horizontally and vertically.

        Ex: Given the following board and words…

        board =
        [
          ['C','A','T','F'],
          ['B','G','E','S'],
          ['I','T','A','E']
        ]
        word = "CAT", return true
        word = "TEA", return true
        word = "SEAT", return true
        word = "BAT", return false

    */
    public class WordSearch
    {
        public static bool Exist(char[][] board, string word)
        {

            //In below two loops, I am just printing all the number in jagged array
            for (int n = 0; n < board.Length; n++)
            {

                // Print the row number
                System.Console.Write("Row({0}): ", n);

                for (int k = 0; k < board[n].Length; k++)
                {
                    // Print the elements in the row
                    System.Console.Write("{0} ", board[n][k]);                    
                }
                System.Console.WriteLine();
            }

            System.Console.WriteLine("Word is:{0}", word);

            //Actual code starts from here
            for (int n = 0; n < board.Length; n++)
            {
                for (int k = 0; k < board[n].Length; k++)
                {
                    if (board[n][k] == word[0] && dfs(board, n, k, 0, word)) return true;
                }             
            }

            return false;
        }

        private static bool dfs(char[][] board, int n, int k, int count, string word)
        {
            if (count == word.Length) return true;

            if (n < 0 || n >= board.Length || k < 0 || k >= board[n].Length || board[n][k] != word[count]) return false;

            char temp = board[n][k];
            board[n][k] = ' ';

            bool found = dfs(board, n + 1, k, count+1, word) ||
                         dfs(board, n - 1, k, count+1, word) ||
                         dfs(board, n, k+1, count+1, word) ||
                         dfs(board, n, k-1, count+1, word);

            board[n][k] = temp;
            return found;
        }
    }
}
