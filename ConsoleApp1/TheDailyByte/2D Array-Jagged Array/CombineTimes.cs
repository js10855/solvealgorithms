﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
    Given a list of interval object, merge all overlapping intervals and return the result.
    Note: an interval object is a simple object that contains a start time and end time 
    and can be constructed by passing a starting and ending time to the constructor.

    Ex: Given the following intervals...

    intervals = [[1, 3], [1, 8]], return a list of interval objects containing [[1, 8]].
    Ex: Given the following intervals...

    intervals = [[1, 2], [2, 6], [7 ,10]], return a list of interval objects containing [[1, 6], [7, 10]]. 

     */
    internal class CombineTimesfor
    {
        public int[][] Merge(int[][] intervals)
        {
            if (intervals == null || intervals.Length == 0 || intervals.Length == 1)
                return intervals;

            List<int[]> res = new List<int[]>();

            intervals = intervals.OrderBy(x => x[0]).ToArray();

            int s = intervals[0][0],
                e = intervals[0][1];

            for (int i = 1; i < intervals.Length; i++)
                if (intervals[i][0] > e)
                {
                    res.Add(new int[] { s, e });
                    s = intervals[i][0];
                    e = intervals[i][1];
                }
                else
                    e = Math.Max(e, intervals[i][1]);

            res.Add(new int[] { s, e });

            return res.ToArray();
        }
    }
}
