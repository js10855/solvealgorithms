﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
     Given an NxM matrix, grid, where each cell in the matrix represents the cost of stepping on the current cell, 
     return the minimum cost to traverse from the top-left hand corner of the matrix to the bottom-right hand corner.
     Note: You may only move down or right while traversing the grid.

     Ex: Given the following grid…

     grid = [
            [1,1,3],
            [2,3,1],
            [4,6,1]
        ], return 7.
        The path that minimizes our cost is 1->1->3->1->1 which sums to 7.
     */
    class MinimizePath
    {
        public int MinPathSum(int[][] grid)
        {
            if (grid.Length <= 0 || grid == null) return 0;
            int[][] result = new int[grid.Length][];

            for (int i = 0; i < grid.Length; i++)
            {                
                result[i] = grid[i].ToArray()  ;                
            }

            for (int i = 0; i < result.Length; i++)
            {
                for (int j = 0; j < result[i].Length; j++)
                {                    
                    if (i>0 && j>0) result[i][j] += Math.Min(result[i][j-1], result[i - 1][j]);
                    else if (i>0) result[i][j] += result[i-1][j];
                    else if (j>0) result[i][j] += result[i][j-1];                    
                }
            }
            return result[grid.Length - 1][grid[0].Length - 1];
        }

        
    }
}
