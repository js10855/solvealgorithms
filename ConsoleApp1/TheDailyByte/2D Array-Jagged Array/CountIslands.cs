﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte._2D_Array_Jagged_Array
{
    /*
     Given a 2D array of integers with ones representing land and zeroes representing water, 
     return the number of islands in the grid. Note: an island is one or more ones surrounded by water connected either vertically or horizontally. 
     Ex: Given the following grid…

    11000
    11010
    11001
    return 3.
    Ex: Given the following grid…

    00100
    00010
    00001
    00001
    00010
    return 4.

     */
    class CountIslands
    {
        public int NumIslands(char[][] grid)
        {
            return 0;
        }
    }
}
