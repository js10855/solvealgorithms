﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
     Given a reference to the root of a binary tree, 
    return a list containing the average value in each level of the tree.

Ex: Given the following binary tree…

       1
      / \
    6    8
   / \ 
  1   5 
return [1.0, 7.0, 3.0]
     */
    internal class Averages
    {
        public IList<double> AverageOfLevels(TreeNode root)
        {
            Queue<TreeNode> q = new Queue<TreeNode>();
            q.Enqueue(root);
            int sum = 0, count = 0;
            List<double> result = new List<double>();

            while ((q.Count != 0))
            {
                // Compute sum of nodes and
                // count of nodes in current
                // level.
                sum = 0;
                count = 0;
                Queue<TreeNode> temp = new Queue<TreeNode>();
                while (q.Count != 0)
                {
                    TreeNode n = q.Peek();
                    q.Dequeue();
                    sum += n.val;
                    count++;
                    if (n.left != null)
                        temp.Enqueue(n.left);
                    if (n.right != null)
                        temp.Enqueue(n.right);
                }
                q = temp;
                
                result.Add(sum * 1.0 / count);
                //Console.Write((sum * 1.0 / count) + " ");
                
            }
            return result;
        }

       

       

    }
}
