﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.Tree
{
    internal class TreesWithinTrees
    {
        /*
         Given two trees s and t return whether or not t is a subtree of s.
         Note: For t to be a subtree of s not only must each node’s value in t match
         its corresponding node’s value in s, but t must also exhibit the exact 
         same structure as s. You may assume both trees s and t exist.

        Ex: Given the following trees s and t…

        s = 1
           / \
          3   8
        t = 1
             \
              8
        return false

        Ex: Given the following trees s and t…

        s = 7
           / \
          8   3
        t = 7
           / \
          8   3
        return true

        Ex: Given the following trees s and t…

        s = 7
           / \
          8   3
        t = 7
           / \
          8   3
             /
            1
        return false
         */
        public bool IsSubtree(TreeNode root, TreeNode subRoot)
        {
            if (root == null) return false;
            else if (isSameTree(root, subRoot)) return true;
            else return IsSubtree(root.left, subRoot) || IsSubtree(root.right, subRoot);
        }

        private bool isSameTree(TreeNode s, TreeNode t)
        {
            if ((s == null) || (t == null)) return s == null && t == null;
            else if (s.val == t.val) return isSameTree(s.left, t.left) && isSameTree(s.right, t.right);
            else return false;
        }
    }
}
