﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
        Given a binary tree, return a list containing its inorder traversal without using recursion.

Ex: Given the following tree…

     2     
    / \   
   1   3
return [1, 2, 3]
Ex: Given the following tree…

       2
      / \
     1   7
    / \
   4   8
return [4, 1, 8, 2, 7]

        */
    class IterativeInorderTraversal
    {

        public IList<int> InorderTraversal(TreeNode root)
        {
            Stack<TreeNode> myStack = new Stack<TreeNode>();
            IList<int> result = new List<int>();
            TreeNode cur = root;
            while (cur !=null || myStack.Count>0 )
            {
                while (cur != null)
                {
                    myStack.Push(cur);
                    cur = cur.left;
                }
                if (myStack.Count>0)
                {
                    cur = myStack.Pop();
                    result.Add(cur.val);
                    cur = cur.right;
                }
               
            }
            return result;

            //IList<int> result = new List<int>();
            //treeSearch(root, result);
            //return result;
        }
        
        private void treeSearch(TreeNode root,IList<int> result)
        {
            if (root == null) return;
            treeSearch(root.left,result);          
            result.Add(root.val);
            treeSearch(root.right, result);
        
        }

    }

    public class TreeNode
    {
      public int val;
      public TreeNode left;
      public TreeNode right;
      public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        public TreeNode(int[] values) : this(values, 0) { }

        TreeNode(int[] values, int index)
        {
            Load(this, values, index);
        }

        void Load(TreeNode tree, int[] values, int index)
        {
            this.val = values[index];
            if (index * 2 + 1 < values.Length)
            {
                this.left = new TreeNode(values, index * 2 + 1);
            }
            if (index * 2 + 2 < values.Length)
            {
                this.right = new TreeNode(values, index * 2 + 2);
            }
        }

        //public void Insert(int value)
        //{
        //    //if the value passed in is greater or equal to the data then insert to right node
        //    if (value >= val)
        //    {   //if right child node is null create one
        //        if (right == null)
        //        {
        //            right = new TreeNode(value);
        //        }
        //        else
        //        {//if right node is not null recursivly call insert on the right node
        //            right.Insert(value);
        //        }
        //    }
        //    else
        //    {//if the value passed in is less than the data then insert to left node
        //        if (left == null)
        //        {//if the leftnode is null then create a new node
        //            left = new TreeNode(value);
        //        }
        //        else
        //        {//if the left node is not null then recursively call insert on the left node
        //            left.Insert(value);
        //        }
        //    }
        //}
    }

    //public class BinaryTree
    //{
    //    int value;
    //    TreeNode left;
    //    TreeNode right;

    //    public BinaryTree(int[] values) : this(values, 0) { }

    //    BinaryTree(int[] values, int index)
    //    {
    //        Load(this, values, index);
    //    }

    //    void Load(TreeNode tree, int[] values, int index)
    //    {
    //        this.value = values[index];
    //        if (index * 2 + 1 < values.Length)
    //        {
    //            this.left = new BinaryTree(values, index * 2 + 1);
    //        }
    //        if (index * 2 + 2 < values.Length)
    //        {
    //            this.right = new BinaryTree(values, index * 2 + 2);
    //        }
    //    }
    //}

    //public static class BinaryTree
    //{
    //    static int idx = -1;
    //   // public TreeNode Root { get; set; }
    //    public static TreeNode buildTree(int[] nodes)
    //    {
    //        idx++;
    //        if (nodes[idx] == -1) return null;
    //        TreeNode node = new TreeNode(nodes[idx]);
    //        node.left = buildTree(nodes);`
    //        node.right= buildTree(nodes);
    //        return node;
    //    }




    //    //public bool Add(int value)
    //    //{
    //    //    TreeNode before = null, after = this.Root;

    //    //    while (after != null)
    //    //    {
    //    //        before = after;
    //    //        if (value < after.val) //Is new node in left tree? 
    //    //            after = after.left;
    //    //        else if (value > after.val) //Is new node in right tree?
    //    //            after = after.right;
    //    //        else
    //    //        {
    //    //            //Exist same value
    //    //            return false;

    //    //        }
    //    //    }

    //    //    TreeNode newNode = new TreeNode();
    //    //    newNode.val = value;

    //    //    if (this.Root == null)//Tree is empty
    //    //        this.Root = newNode;
    //    //    else
    //    //    {
    //    //        if (value < before.val)
    //    //            before.left = newNode;
    //    //        else
    //    //            before.right = newNode;
    //    //    }

    //    //    return true;
    //    //}

    //    //public void Insert(int data)
    //    //{
    //    //    //if the root is not null then we call the Insert method on the root node
    //    //    if (Root != null)
    //    //    {
    //    //        Root.Insert(data);
    //    //    }
    //    //    else
    //    //    {//if the root is null then we set the root to be a new node based on the data passed in
    //    //        Root = new TreeNode(data);
    //    //    }
    //    //}
    //}

}
