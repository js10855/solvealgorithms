﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
    Given the root of a binary tree and two values low and high 
    return the sum of all values in the tree that are within low and high.

    Ex: Given the following tree where low = 3 and high = 5…

            1
           / \
          7   5
        /    / \
       4    3   9
    return 12 (3, 4, and 5 are the only values within low and high and they sum to 12) 

     */
    internal class SumWithinBounds
    {
        int sum = 0;
        public int RangeSumBST(TreeNode root, int low, int high)
        {
            if (root == null) return 0;

            if (root.val >= low) RangeSumBST(root.left, low, high);
            if (root.val <= high) RangeSumBST(root.right, low, high);
            if (root.val >= low && root.val <= high) sum += root.val;

            return sum;
        }

       

    }
}
