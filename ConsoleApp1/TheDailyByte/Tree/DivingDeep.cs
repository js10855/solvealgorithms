﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
        Given an N-ary tree, return its maximum depth.
        Note: An N-ary tree is a tree in which any node may have at most N children.

        Ex: Given the following tree…

                   4
                 / | \
                3  9  2
               /       \
              7         2
        return 3.
     ḍ*/
    internal class DivingDeep
    {
        public int MaxDepth(TreeNode root)
        {
            if (root == null) return 0;
            int left = MaxDepth(root.left);
            int right = MaxDepth(root.right);
            return Math.Max(left, right) + 1;
        }
    }
}
