﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
    Given an array of strings, return the longest common prefix that is shared amongst all strings.
    Note: you may assume all strings only contain lowercase alphabetical characters.

    Ex: Given the following arrays...

    ["colorado", "color", "cold"], return "col"
    ["a", "b", "c"], return ""
    ["spot", "spotty", "spotted"], return "spot"

    */
    class LongestCommonPrefix
    {
        public static string solution(string[] strs)
        {
            if (strs.Length == 0) return "";
            string prefix = strs[0]; //flower

            for (int i = 1; i < strs.Length; i++)
            {
                while (strs[i].IndexOf(prefix) != 0)
                {
                    prefix = prefix.Substring(0, prefix.Length - 1);
                }
            }
            return prefix;
        }
    }
}
