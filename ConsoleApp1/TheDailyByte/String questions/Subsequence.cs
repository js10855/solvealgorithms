﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.String_questions
{
    /*
     Given two strings s and t return whether or not s is a subsequence of t.
    Note: You may assume both s and t only consist of lowercase characters and 
    both have a length of at least one.

    Ex: Given the following strings s and t…

    s = "abc", t = "aabbcc", return true.
    Ex: Given the following strings s and t…

    s = "cpu", t = "computer", return true.
    Ex: Given the following strings s and t…

    s = "xyz", t = "axbyc", return false.
      
     */
    class Subsequence
    {
        public bool IsSubsequence(string s, string t)
        {            
            int index = 0;

            if (s.Equals("")) return true;

            for (int i = 0; i < t.Length; i++)
            {
                if (s[index] == t[i]) index++;
                if (index >= s.Length) return true;
            }
            
            return false;
        }
    }
}
