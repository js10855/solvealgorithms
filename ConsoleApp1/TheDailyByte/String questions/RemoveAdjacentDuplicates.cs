﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
   public class RemoveAdjacentDuplicates
    {
        /*
            Given a string s containing only lowercase letters, continuously remove adjacent characters that are the same and return the result.

            Ex: Given the following strings...

            s = "abccba", return ""
            s = "foobar", return "fbar"
            s = "abccbefggfe", return "a"
    
        */
        public static string RemoveDuplicates(string s)
        {
            //for (int i = 1; i < s.Length; i++)
            //{
            //    if (i > 0 && s[i] == s[i - 1])
            //    {
            //        s = s.Remove(i - 1, 2);
            //        i -= 2;
            //    }
            //}
            //return s;

            int n = 0;
            while (s.Length > n+1)
            {
                if (s[n].Equals(s[n + 1]))
                {
                    //var index = s.IndexOf(s[n]);
                    s = s.Remove(n, 2);
                    n = 0;
                }
                else n++;
            }

            return s;
        }
    }
}
