﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    public class GenerateTextMessages
    {
        /*         
        Given a string of digits, return all possible text messages those digits could send. Note: The mapping of digits to letters is as follows…

        0 -> null
        1 -> null
        2 -> "abc"
        3 -> "def"
        4 -> "ghi"
        5 -> "jkl"
        6 -> "mno"
        7 -> "pqrs"
        8 -> "tuv"
        9 -> "wxyz"
        Ex: digits = "23" return ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"]
          
         */
        public static IList<string> LetterCombinations(string digits)
        {
            List<string> result = new List<string>();
            if (digits == null || digits.Length == 0) return result;

            string[] mapping = { "", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };

            LetterCombinationsRecursive(result, digits, "", 0, mapping);
            return result;

            //List<string> result = new List<string>();
            //string[] mapping =  { "", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };
            //foreach (var item in digits)
            //{
            //    var num = Int32.Parse(item.ToString());
            //    string str = mapping[num];
            //    result.Add(str);
            //}

            //if (result.Count <=1) return result;

            //var temp = result.ToList();                
            //result.Clear();


            //for (int i = 0; i < temp.Count; i++)
            //{
            //    for (int j = 0; j < temp[i].Length; j++)
            //    {
            //        string str1 = temp[i][i].ToString();                        
            //        string str2= temp[i + 1][j].ToString();   
            //        result.Add(str1+str2);
            //    }
            //}
            //return result;
        }

        private static void LetterCombinationsRecursive(List<string> result, string digits, string current, int index,string[] mapping)
        {
            if (index == digits.Length)
            {
                result.Add(current);
                return;
            }

            string letters = mapping[digits[index]-'0'];
            for (int i = 0; i < letters.Length; i++)
            {
                LetterCombinationsRecursive(result, digits, current + letters[i], index + 1, mapping);
            }

        }
    }
}
