﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{ 
    /*
        Given a string s, reverse the words.
        Note: You may assume that there are no leading or trailing whitespaces and each word 
        within s is only separated by a single whitespace.

        Ex: Given the following string s…

        s = "The Daily Byte", return "Byte Daily The".
     */
    internal class SwapWords
    {
        public string ReverseWords(string s)
        {
            string[] words = s.Trim().Split(' ');
            StringBuilder sb = new StringBuilder();

            for (int i = words.Length - 1; i >= 0; i--)
            {
                if (i != 0 && words[i]!="") sb.Append(words[i].Trim() + ' ');
                else sb.Append(words[i].Trim());
            }            
            
            return sb.ToString();
        }
    }
}
