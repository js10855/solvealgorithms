﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.String_questions
{
    class FirstUniqueCharacter
    {
        /*
        Given a string, return the index of its first unique character.If a unique character does not exist, return -1.

        Ex: Given the following strings...

        "abcabd", return 2
        "thedailybyte", return 1
        "developer", return 0
        */
        public static int solution(string s)
        {
            // Solution 1
            //int result = -1;
            //for (int i = 0; i < s.Length; i++)
            //{
            //    if (s.Where(x => x == s[i]).Count() == 1) return i; 
            //}R
            //return result;

            //Solution 2            
            int result = -1;
            int[] count = new int[26];
            for (int i = 0; i < s.Length; i++)
            {
                count[s[i] - 'a'] += 1;
            }

            for (int i = 0; i < s.Length; i++)
            {
                if (count[s[i] - 'a'] == 1) return i;
            }
            return result;

        }

    }
}
