﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
    Given a string, return whether or not it uses capitalization correctly.A string correctly uses capitalization if all letters are capitalized, no letters are capitalized, or only the first letter is capitalized.

Ex: Given the following strings...

"USA", return true
"Calvin", return true
"compUter", return false
"coding", return true
    */

    class CorrectCapitalization
    {
        public bool solution (string s)
        {
            int counter = 0;
            foreach (var item in s)
            {
                if (Char.IsUpper(item)) counter++;
            }
            return (counter == 1 && Char.IsUpper(s[0])) || counter == s.Length || counter == 0;
        }
    }
}
