﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
     Given a string representing the sequence of moves a robot vacuum makes, 
    return whether or not it will return to its original position. 
    The string will only contain L, R, U, and D characters, representing left, right, up, and down respectively.
    Ex: Given the following strings...
    "LR", return true
    "URURD", return false
    "RUULLDRD", return true
     */
    class VacuumCleanerRoute
    {
        public bool solution (string s)
        {
            if (s.Length % 2 != 0) return false;
            int LR = 0;
            int UD = 0;
            foreach (var item in s)
            {
                switch(item)
                {
                    case 'R':
                        {
                            LR++;
                            break;
                        }
                    case 'L':
                        {
                            LR--;
                            break;
                        }
                    case 'U':
                        {
                            UD++;
                            break;
                        }
                    case 'D':
                        {
                            UD--;
                            break;
                        }
                }
            }
            return LR == 0 && UD == 0;
        }
    }
}
