﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    internal class KeyboardRow
    {
        public string[] FindWords(string[] words)
        {
            List<char> firstRow = new List<char> { 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p' };
            List<char> secondRow = new List<char> { 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l' };
            List<char> thirdRow = new List<char> { 'z', 'x', 'c', 'v', 'b', 'n', 'm' };
            List<string> result = new List<string>();
            int row=0;

            foreach (string word in words)
            {
                var w = word.ToLower();
                if (firstRow.Contains(w[0])) row = 1;
                else if (secondRow.Contains(w[0])) row = 2;
                else if (thirdRow.Contains(w[0])) row = 3;
                bool allCharFound = true;

                foreach (char c in w)
                {
                    if (row==1)
                    {
                        if (!firstRow.Contains(c)) { allCharFound = false; break; }
                    }
                    if (row == 2)
                    {
                        if (!secondRow.Contains(c)) { allCharFound = false; break; }
                    }
                    if (row == 3)
                    {
                        if (!thirdRow.Contains(c)) { allCharFound = false; break; }
                    }
                }
                if (allCharFound) result.Add(word);
            }
            return result.ToArray();
        }
    }
}
