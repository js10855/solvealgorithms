﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    class ValidePalindrome
    {
        /*
        Given a string, return whether or not it forms a palindrome ignoring case and 
        non-alphabetical characters.
        Note: a palindrome is a sequence of characters that reads the same forwards and backwards.

        Ex: Given the following strings...

        "level", return true
        "algorithm", return false
        "A man, a plan, a canal: Panama.", return true
          
         */

        public bool solution(string s)
        {
            s = Regex.Replace(s, "[^a-zA-Z0-9]", "");
            string strReverse = new string(s.Reverse().ToArray());
            return string.Equals(s, strReverse, StringComparison.OrdinalIgnoreCase);
        }
    }
}
