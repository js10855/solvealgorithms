﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
     Given a string s and a string code, 
     return whether or not s could have been encrypted using the pattern 
     represented in code.

    Ex: Given the following s and code...

    s = “the daily byte”, code = “abc”, return true
    Ex: Given the following s and code...

    s = “the daily byte curriculum”, code = “abcc”, return false 
    because ‘c’ in code already maps to the word “byte”
     */
    internal class CracktheCode
    {
        public bool WordPattern(string pattern, string s)
        {            
            //Solution 1
            //var arr = s.Split(' ').ToList();
            //int n = pattern.Length;
            //if (n != arr.Count) return false;
            //for (int i = 0; i < pattern.Length; i++)
            //{
            //    if (arr.IndexOf(arr[i]) != pattern.IndexOf(pattern.Substring(i,(i+1)-i))) return false;
            //}
            //return true;

            //Solution 2
            string[] words = s.Split(' ');
            int patternLen = pattern.Length;
            int wordLen = words.Length;
            if (patternLen != wordLen) return false;
            Dictionary<Char,String> patternMap= new Dictionary<Char,String>();  

            for (int i = 0; i < patternLen; i++)
            {
                char pChar = pattern[i];
                string word = words[i];
                if (patternMap.ContainsKey(pChar))
                {
                    if (!(patternMap[pChar] == word)) return false;
                }
                else
                {
                    if (patternMap.Values.Contains(word)) return false;
                    patternMap[pChar] = word;
                }

            }

            return true;

        }
    }
}
