﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    class ValidPalindromeWithRemoval
    {
        /*
    Given a string and the ability to delete at most one character, return whether or not it can form
        a palindrome.
    Note: a palindrome is a sequence of characters that reads the same forwards and backwards.

    Ex: Given the following strings...

    "abcba", return true
    "foobof", return true (remove the first 'o', the second 'o', or 'b')
    "abccab", return false        
        */
        public static bool solution(string s)
        {
            int start = 0, end = s.Length - 1;
            // Count1 for tracking no. of deletion performed at start position


            // Count2 for tracking no. of deletion performed at end position
            int count1 = 0;
            int count2 = 0;

            while (start < end)
            {
                // IF start and end char are equal then just move start and end repectively
                if (s[start] == s[end])
                {
                    start++;
                    end--;
                }
                // But if they are not equal let's try skiping character at start and increment count1
                else
                {
                    start++;
                    count1++;
                }
            }
            // Don't forget to reinitialize start and end
            start = 0; end = s.Length - 1;

            while (start < end)
            {
                // IF start and end char are equal then just move start and end repectively
                if (s[start] == s[end])
                {
                    start++;
                    end--;
                }
                // But if they are not equal let's try skiping character at start and increment count2
                else
                {
                    end--;
                    count2++;
                }
            }

            // AT end end if given string is already palindrome then count1 and count2 will be zero
            if (count1 == 0 && count2 == 0) return true;

            // If count1 or count2 is exactly one then it means only one deletion is required and then string will be plaindrome
            if (count1 == 1 || count2 == 1) return true;

            // If count1 or coun2 is greater than 1 then it means that MORE THAN ONE deletion is required then string will be plaindrome
            return false;
        }

    }
}
