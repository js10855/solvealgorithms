﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.String_questions
{    
    public class RecentCounter
    {
        /*
        Create a class CallCounter that tracks the number of calls a client has made within the last 3 seconds.Your class should contain one method, ping(int t) that receives the current timestamp(in milliseconds) of a new call being made and returns the number of calls made within the last 3 seconds.
        Note: you may assume that the time associated with each subsequent call to ping is strictly increasing.

        Ex: Given the following calls to ping…

        ping(1), return 1 (1 call within the last 3 seconds)
        ping(300), return 2 (2 calls within the last 3 seconds)
        ping(3000), return 3 (3 calls within the last 3 seconds)
        ping(3002), return 3 (3 calls within the last 3 seconds)
        ping(7000), return 1 (1 call within the last 3 seconds)
        */
        private List<int> times = new List<int>();
        public RecentCounter()
        {

        }

        public int Ping(int t)
        {
            times.Add(t);
            int counter = 0;
            int lo = t - 3000;
            int hi = t;

            //Count all time(s) that fall in the range t-3000 to t
            foreach (var time in times)
            {
                if (lo <= time && time <= hi) counter++;
            }
            return counter;
        }
    }
}
