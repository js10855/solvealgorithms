﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    public class StringPermutations
    {
        /*
        Given a string s consisting of only letters and digits, where we are allowed to transform any letter to uppercase or lowercase, return a list containing all possible permutations of the string.

Ex: Given the following string…

S = "c7w2", return ["c7w2", "c7W2", "C7w2", "C7W2"]


        */
        public static IList<string> LetterCasePermutation(string s)
        {
            LinkedList<string> result = new LinkedList<string>();
            result.AddFirst(s);
            int n = s.Length;

            for (int i = n-1; i >=0; i--)
            {
                char c = s[i];               
                if (Char.IsLetter(c))
                {
                    int size = result.Count;
                    while (size-- > 0)
                    {
                        string str = result.First();
                        result.RemoveFirst();
                        string left = str.Substring(0, i-0);
                        string right = str.Substring(i+1 , n-(i+1));
                        result.AddLast(left + Char.ToUpper(c) + right);
                        result.AddLast(left + Char.ToLower(c) + right);
                    }
                }
            }         
           
            return result.ToList();
        }
    }
}
