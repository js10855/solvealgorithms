﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    public class CompareKeystrokes
    {
        /*
        Given two strings s and t, which represents a sequence of keystrokes, where # denotes a backspace, return whether or not the sequences produce the same result.

        Ex: Given the following strings...

        s = "ABC#", t = "CD##AB", return true
        s = "como#pur#ter", t = "computer", return true
        s = "cof#dim#ng", t = "code", return false
        */
        public static bool BackspaceCompare(string s, string t)
        {
           
            while (s.Contains('#'))
            {
                int index = s.IndexOf('#');
                s = s.Remove(index, 1);
                if (index>0) s = s.Remove(index - 1, 1);
            }

            while (t.Contains('#'))
            {
                int index = t.IndexOf('#');
                t = t.Remove(index, 1);
                if(index > 0) t = t.Remove(index - 1, 1);
            }           

            return s.Equals(t);
        }



    }
}
