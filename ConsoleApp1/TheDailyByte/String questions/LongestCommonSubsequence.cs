﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
     Given two strings, s and t, return the length of their longest subsequence.
    Ex: Given the following strings s and t…

    s = "xyz", t = "xyz". return 3.
    Ex: Given the following strings s and t…

    s = "abca", t = "acea", return 3.
    Ex: Given the following strings s and t…

    s = "abc", t = "def", return 0.

     */
    class LongestCommonSubsequenceClass
    {
        int[][] dp;
        public int LongestCommonSubsequence(string text1, string text2)
        {
            dp = new int[text1.Length][];
            for (int i = 0; i < text1.Length; i++)
            {
                dp[i] = new int[text2.Length];
                for (int j = 0; j < text2.Length; j++)
                {
                    dp[i][j] = -1;
                }
            }

            return helper(text1, text2, 0, 0);
        }

        private int helper(string text1, string text2, int index1, int index2)
        {
            if (index1 == text1.Length || index2 == text2.Length) return 0;
            if (dp[index1][index2] >= 0) return dp[index1][index2];

            if (text1[index1] == text2[index2]) dp[index1][index2] = 1 + helper(text1, text2, index1 + 1, index2 + 1);
            else dp[index1][index2] = Math.Max(helper(text1, text2, index1 + 1, index2), helper(text1, text2, index1, index2 + 1));
            return dp[index1][index2];
        }
    }
}
