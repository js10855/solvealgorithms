﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
     Given a string s remove all the vowels it contains and return the resulting string.
Note: In this problem y is not considered a vowel.

Ex: Given the following string s…

s = "aeiou", return ""
Ex: Given the following string s…

s = "byte", return "byt"
Ex: Given the following string s…

s = "xyz", return "xyz"
     */
    class RemoveVowelClass
    {
        public string RemoveVowel(string s)
        {
            List<string> vList = new List<String> { "a", "e", "i", "o", "u" };
            foreach (var vowel in vList)
            {
                s = s.Replace(vowel, "");
            }
            return s;
        }

    }
}
