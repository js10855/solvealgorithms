﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.String_questions
{
    class LunchTime
    {
        /*
            You are serving people in a lunch line and need to ensure each person gets a “balanced” meal. 
            A balanced meal is a meal where there exists the same number of food items as drink items. 
            Someone is helping you prepare the meals and hands you food items (i.e. F) or a drink items (D) in the order 
            specified by the items string. Return the maximum number of balanced meals you are able to create without being able to modify items.
            Note: items will only contain F and D characters.

            Ex: Given the following items…

            items = "FDFDFD", return 3
            the first "FD" creates the first balanced meal.
            the second "FD" creates the second balanced meal.
            the third "FD" creates the third balanced meal.
            Ex: Given the following items…

            items = "FDFFDFDD", return 2
            "FD" creates the first balanced meal.
            "FFDFDD" creates the second balanced meal.
*/
            public int BalancedMeal(string s)
        {
            int fMeal = 0;
            int dMeal = 0;
            int balancedMeal = 0;
            foreach (var item in s)
            {
                if (item == 'F') fMeal++;
                else dMeal++;
            }

            if (fMeal == dMeal) balancedMeal++;
            return balancedMeal;
        }
         
    }
}
