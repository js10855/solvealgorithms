﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.String_questions
{
    class JewelsAndStones
    {
        /*
         Given a string representing your stones and another string representing a list of jewels, return the number of stones that you have that are also jewels.

        Ex: Given the following jewels and stones...

        jewels = "abc", stones = "ac", return 2
        jewels = "Af", stones = "AaaddfFf", return 3
        jewels = "AYOPD", stones = "ayopd", return 0        
        */
        public static int solution(string jewels, string stones)
        {
            int count = 0;

            for (int i = 0; i < stones.Length; i++)
            {
                if (jewels.Contains(stones[i]))
                {
                    count++;
                }
            }
            return count;
        }

    }
}
