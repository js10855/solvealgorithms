﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    class CharacterScramble
    {
        /*
         Given two strings, passage and text return whether or not the characters in text can be used to form the given passage.
            Note: Each character in text may only be used once and passage and text will only contain lowercase alphabetical characters.

            Ex: Given the following passage and text…

            passage = "bat", text = "cat", return false.
            Ex: Given the following passage and text…

            passage = "dog" text = "didnotgo", return true.

         */

        public bool IsScramble(string s1, string s2)
        {
            //bool result = false;
            for (int i = 0; i < s1.Length; i++)
            {
               
                int val = s2.IndexOf(s1[i]);
                if (val >= 0) s2 = s2.Remove(val, 1);
                else return false;
            }           

            return true;
        }

        /*
         You are given an array of strings words and a string chars.

        A string is good if it can be formed by characters from chars (each character can only be used once).

        Return the sum of lengths of all good strings in words. 

        Example 1:

        Input: words = ["cat","bt","hat","tree"], chars = "atach"
        Output: 6
        Explanation: The strings that can be formed are "cat" and "hat" so the answer is 3 + 3 = 6.
        Example 2:

        Input: words = ["hello","world","leetcode"], chars = "welldonehoneyr"
        Output: 10
        Explanation: The strings that can be formed are "hello" and "world" so the answer is 5 + 5 = 10.

         */

        public int CountCharacters(string[] words, string chars)
        {
            int[] char_count = new int[26];           
            int matchLength = 0;           
                        
            foreach (var c in chars)
            {
                char_count[c - 'a']++;
            }

            foreach (var word in words)
            {
                int[] temp_char_count= new int[char_count.Length];
                int valid_char_count = 0;
                Array.Copy(char_count, temp_char_count, char_count.Length);

                foreach (var c in word)
                {
                    if (temp_char_count[c-'a']>0)
                    {
                        valid_char_count++;
                        temp_char_count[c - 'a']--;
                    }
                   
                }

                if (valid_char_count == word.Length) matchLength += word.Length;           
             }
               
            return matchLength;

            //int matchLength = 0;
            //foreach (var word in words)
            //{
            //    bool found = true;               
            //    for (int i = 0; i < word.Length; i++)
            //    {
            //        string newChars = chars;
            //        int val = newChars.IndexOf(word[i]);
            //        if (val >= 0)
            //        {
            //            newChars = newChars.Remove(val, 1);
            //        }
            //        else
            //        { 
            //            found = false;
            //            break;
            //        }
            //    }
            //    if (found) matchLength += word.Length;
            //}
            //return matchLength;
        }
    }
}
