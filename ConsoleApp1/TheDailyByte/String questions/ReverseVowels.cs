﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    class ReverseVowelsClass
    {
        /*
        Given a string, reverse the vowels of it.
        Note: In this problem y is not considered a vowel.

        Ex: Given the following strings s…

        s = "computer", return "cemputor"
        Ex: Given the following strings s…

        s = "The Daily Byte", return "The Dialy Byte" 
        The vowels are 'a', 'e', 'i', 'o', and 'u', and they can appear in both cases.

         */

        public string ReverseVowels(string s)
        {            
            List<Char> vowels = new List<Char> { 'a','A','e','E','i','I','o','O','u','U'};           

            char[] characters = s.ToCharArray();
            int first = 0;
            int last = s.Length - 1;

            while (first<last)
            {
                while (first < last && !vowels.Contains(characters[first]))
                    first++;

                while (first < last && !vowels.Contains(characters[last]))
                    last--;

                Char temp = characters[first];
                characters[first++] = characters[last];
                characters[last--] = temp;

            }         
            
            return new string(characters);
        }


    }
}
