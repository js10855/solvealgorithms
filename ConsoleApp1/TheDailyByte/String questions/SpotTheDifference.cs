﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    class SpotTheDifference
    {
        /*This question is asked by Google.You are given two strings, s and t which only consist of lowercase letters.t is generated by shuffling the letters in s as well as potentially adding an additional random character.Return the letter that was randomly added to t if it exists, otherwise, return ’  ‘.
        Note: You may assume that at most one additional character can be added to t.
        Ex: Given the following strings...
        s = "foobar", t = "barfoot", return 't'
        s = "ide", t = "idea", return 'a'
        s = "a", t="aa", return 'a'
        s = "coding", t "ingcod", return '
        '*/

        public static char solution (string s, string t)
        {
            int[] count = new int[26];
            char[] S = s.ToCharArray(), T = t.ToCharArray();
            for (int i = 0; i < S.Length; i++) count[S[i] - 'a']++;
            for (int i = 0; i < T.Length; i++) count[T[i] - 'a']--;
            for (int i = 0; i < 26; i++) if (count[i] != 0) return (char)(i + 'a');
            return '\0';


            //int i = 0;
            //int len = s.Length;
            //int sum = 0;

            //while (i<len)
            //{
            //    sum += s[i] - t[i];
            //    i++;
            //}

            //sum += t[i];
            //return (char)sum;


            //char result ='\0';
            //foreach (var item in t)
            //{
            //    if (!s.Contains(item))
            //    {
            //        result = item;
            //        return result;
            //    }
            //    else
            //    {
            //        s=s.Replace(item.ToString(), string.Empty);
            //    }
            //}
            //return result;
        }
    }
}
