﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
    Given two strings s and t, return the index of the first occurrence of t 
    within s if it exists; otherwise, return -1.

    Ex: Given the following strings s and t…

    s = "abc", t = "a", return 0.
    Ex: Given the following strings s and t…

    s = "abc", t = "abcd", return -1. 

    Given two strings needle and haystack, 
    return the index of the first occurrence of needle in haystack, 
    or -1 if needle is not part of haystack.

     */
    internal class IndexOf
    {
        public int StrStr(string haystack, string needle)
        {
            
            if (needle.Length > haystack.Length) return -1;
            return haystack.IndexOf(needle);

            
        }
    }
}
