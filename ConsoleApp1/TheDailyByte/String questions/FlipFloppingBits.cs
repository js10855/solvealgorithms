﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.String_questions
{
    /*
     Given a positive integer N, return whether or not it has alternating bit values.

    Ex: Given the following value for N…

    N = 5, return true.
    5 in binary is 101 which alternates bit values between 0 and 1.
    Ex: Given the following value for N…

    N = 8, return false
    8 in binary is 1000 which does not alternate bit values between 0 and 1. 
     */
    internal class FlipFloppingBits
    {
        public bool HasAlternatingBits(int n)
        {            
            var str = Convert.ToString(n, 2);
            var num = str[0];

            for (int i = 1; i < str.Length; i++)
            {
                if (num == str[i]) return false;                
                else num=str[i];
            }
            
            return true;
        }
    }
}
