﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
        Given two integers x and y, return the hamming distance between the two numbers.
        Note: The hamming distance between two numbers is the number of bit positions in which the bits differ.

        Ex: Given the following integers x and y…

        x = 2, y = 4, return 2.
        2 in binary is 0 0 1 0
        4 in binary is 0 1 0 0
        therefore the number of positions in which the bits differ is two.

     */
    class HammingDistanceClass
    {
        public int HammingDistance(int x, int y)
        {
            //int cnt = 0;

            //while (x > 0 || y > 0)
            //{
            //    if (x % 2 != y % 2) ++cnt;
            //    x /= 2; y /= 2;
            //}
            //return cnt;


            int result = 0;

            string s1 = Convert.ToString(x, 2);
            string s2 = Convert.ToString(y, 2);

            s1= s1.Length > s2.Length ? s1.PadLeft(s2.Length, '0') : s1;
            s2 = s2.Length > s2.Length ? s1.PadLeft(s1.Length, '0') : s2;

            for (int i = 0; i < s1.Length; i++)
            {
                if (s1[i] != s2[i]) result++;
            }

            return result;
        }
    }
}
