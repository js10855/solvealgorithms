﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*This question is asked by Google.Given a string, reverse all of its characters and return the resulting string.
        Ex: Given the following strings...
        “Cat”, return “taC”
        “The Daily Byte”, return "etyB yliaD ehT”
        “civic”, return “civic”*/

    class ReverseString
    {
        public static void solution()
        {
            string s = "cat";
            string strReverse = string.Empty;

            //Solution 1
            //strReverse = new string(s.Reverse().ToArray());


            //Solution 2;
            //int start = 0;
            //int end = s.Length-1;
            //char[] charRev = new char[s.Length];
            //foreach (var item in s)
            //{
            //    charRev[start] = s[end];
            //    start++;
            //    end--;
            //}
            //strReverse = new string(charRev);

            //Solution 3
            char[] newArray = new char[s.Length];
            for (int i = 0, j = s.Length - 1; i <= j; i++, j--)
            {
                newArray[i] = s[j];
                newArray[j] = s[i];
            }
            strReverse = new string(newArray);


            Console.WriteLine(strReverse);
            Console.ReadLine();


        }
    }
}
