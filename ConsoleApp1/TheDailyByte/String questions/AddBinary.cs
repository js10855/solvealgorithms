﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    /*
     Given two binary strings (strings containing only 1s and 0s) return their sum (also as a binary string).
Note: neither binary string will contain leading 0s unless the string itself is 0

Ex: Given the following binary strings...

"100" + "1", return "101"
"11" + "1", return "100"
"1" + "0", return  "1"
      
     */
    class AddBinary
    {
        public static string solution (string s1, string s2)
        {
            Int64 number_one = Convert.ToInt64(s1, 2);
            Int64 number_two = Convert.ToInt64(s2, 2);

            return Convert.ToString(number_one + number_two, 2);
        }
    }
}
