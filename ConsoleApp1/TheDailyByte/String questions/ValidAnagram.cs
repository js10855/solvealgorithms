﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte.String_questions
{
    /*
    Given two strings s and t return whether or not s is an anagram of t.
    Note: An anagram is a word formed by reordering the letters of another word.

    Ex: Given the following strings...

    s = "cat", t = "tac", return true
    s = "listen", t = "silent", return true
    s = "program", t = "function", return false

    */
    class ValidAnagram
    {
        public static bool solution(string s, string t)
        {
            bool result = true;
            if (s.Length != t.Length) return false;
            for (int i = 0; i < s.Length; i++)
            {
                int sCount = s.Where(c => c == s[i]).Count();
                int tCount = t.Where(c => c == s[i]).Count();

                if (sCount != tCount) return false;
                t = t.Replace(s[i].ToString(), string.Empty);
                s = s.Replace(s[i].ToString(), string.Empty);
            }

            return result;

        }
    }
}
