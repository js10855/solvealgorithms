﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    class ComplementaryNumbers
    {
        /*
         Given a positive number, return its complementary number.
        Note: The complement of a number is the number that results from flipping every bit in the original number. 
        (i.e. zero bits become one bits and one bits become zero bits).

        Ex: Given the following number…

        number = 27, return 4.
        27 in binary (not zero extended) is 11011.
        Therefore, the complementary binary is 00100 which is 4.
         */
        public int FindComplement(int num)
        {
            StringBuilder sbResult = new StringBuilder();
            string binaryString = Convert.ToString(num, 2);
            foreach (var item in binaryString)
            {
                if (item == '1') sbResult.Append('0');
                else sbResult.Append('1');
            }
            Console.WriteLine(Convert.ToInt32(sbResult.ToString(), 2));
            return Convert.ToInt32(sbResult.ToString(), 10);
            
        }
    }
}
