﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.TheDailyByte
{
    class UncommonFromSentences
    {
        /*Given two strings representing sentences, return the words that are not common to both strings
         * (i.e. the words that only appear in one of the sentences). 
         * You may assume that each sentence is a sequence of words (without punctuation) correctly 
         * separated using space characters.

        Ex: given the following strings...

        sentence1 = "the quick", sentence2 = "brown fox", return ["the", "quick", "brown", "fox"]
        sentence1 = "the tortoise beat the haire", sentence2 = "the tortoise lost to the haire", return ["beat", "to", "lost"]
        sentence1 = "copper coffee pot", sentence2 = "hot coffee pot", return ["copper", "hot"]*/

        public static string[] solution(string s1, string s2)
        {
            List<string> result = new List<string>();
            string[] s1Arr = s1.Split(' ');
            string[] s2Arr = s2.Split(' ');
            for (int i = 0; i < s1Arr.Length; i++)
            {
                if (!s2Arr.Contains(s1Arr[i]))
                {
                    if (!(s1Arr.Where(x=>x==s1Arr[i]).Count()>1))  result.Add(s1Arr[i]);                    
                }
            }




            for (int i = 0; i < s2Arr.Length; i++)
            {
                if (!s1Arr.Contains(s2Arr[i]))
                {
                    if (!(s2Arr.Where(x => x == s2Arr[i]).Count() > 1)) result.Add(s2Arr[i]);
                }
            }


            return result.ToArray();
        }
    }
}
