﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;


namespace FindnthFibonacciNumber
{
    class Program
    {
        //This variable will hold all sum results up to nth number
        static List<int> resultSum = new List<int>();
        static int[] arrResult;

        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();           
            Console.Write("Enter the nth number of the Fibonacci Series: ");
            int length = Convert.ToInt32(Console.ReadLine());
            /////First Method
            sw.Start();
            int result=0;

            //Loop through all numbers till nth number to get nth number's value
            arrResult = new int[length+1];
            for (int i = 0; i <= length; i++)
            {
                result = getFibonacci(i);
            }
            Console.WriteLine(result);
            sw.Stop();
            Console.WriteLine("Time taken using List:" + sw.ElapsedMilliseconds);

            //Second Method
            sw.Start();
            result = 0;

            //Loop through all numbers till nth number to get nth number's value           
            for (int i = 0; i <= length; i++)
            {
                result = getFibonacci1(i);
            }
            Console.WriteLine(result);
            sw.Stop();
            Console.WriteLine("Time taken using for loop:" + sw.ElapsedMilliseconds);

            //Third Method
            sw.Start();
            result = 0;

            //Loop through all numbers till nth number to get nth number's value           
            for (int i = 0; i <= length; i++)
            {
                result = getFibonacci2(i);
            }
            Console.WriteLine(result);
            sw.Stop();
            Console.WriteLine("Time taken using array:" + sw.ElapsedMilliseconds);

            //Fourth Method
            sw.Start();
            result = 0;

            //Loop through all numbers till nth number to get nth number's value           
            for (int i = 0; i <= length; i++)
            {
                result = getFibonacci3(i);
            }
            Console.WriteLine(result);
            sw.Stop();
            Console.WriteLine("Time taken using recursive method:" + sw.ElapsedMilliseconds);

            Console.ReadKey();
        }

        static int getFibonacci(int N)
        {
            //As per task given, both zero and one has result 1.
            if ((N == 0) || (N == 1))
            {
                resultSum.Add(1);
                return 1;
            }
            else
            {
                resultSum.Add(resultSum[N - 1] + resultSum[N - 2]);
                return Convert.ToInt32(resultSum[N - 1] + resultSum[N - 2]);
            }
        }

        static int getFibonacci1(int n)
        {
            int firstnumber = 1, secondnumber = 1, result = 0;

            if (n == 0) return 1; //To return the first Fibonacci number   
            if (n == 1) return 1; //To return the second Fibonacci number   

            for (int i = 2; i <= n; i++)
            {
                result = firstnumber + secondnumber;
                firstnumber = secondnumber;
                secondnumber = result;
            }

            return result;

        }

        static int getFibonacci2(int N)
        {
            //As per task given, both zero and one has result 1.
            if ((N == 0) || (N == 1))
            {
                arrResult[N] = 1;
                return 1;
            }
            else
            {
                arrResult[N] = arrResult[N - 1] + arrResult[N - 2];
                //resultSum.Add(resultSum[N - 1] + resultSum[N - 2]);
                return Convert.ToInt32(arrResult[N - 1] + arrResult[N - 2]);
            }
        }

        static int getFibonacci3(int n)
        {
            if (n == 0) return 1; //To return the first Fibonacci number   
            if (n == 1) return 1; //To return the second Fibonacci number   
            return getFibonacci3(n - 1) + getFibonacci3(n - 2);
        }

        

        
       
    }
}
